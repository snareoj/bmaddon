/* 
 * Copyright 2017 snareoj.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */


import Client from 'content_scripts/Client.js';
import TagsMenu from 'content_scripts/TagsMenu.js';
import * as Constants from 'content_scripts/Constants.js';
import Utils from 'lib/Utils.js';
import ModalHandler from 'lib/ModalHandler.js'

const clsPref = 'snares-io-';
        
export default class Menu {

    /**
     * @param {Client} client
     */
    constructor(client) {        
        this.client = client;
        this.inited = false;
        this.buttons = [];
        var prefix = Constants.prefix;
        var idMenu = Constants.IdMenu;
        var clIcon = Constants.clIcon;
        var style = clsPref + "squares";// | clsPref + 'circles'
        this.menuTemplate = `    
            <div class="${clsPref}menu ${clsPref}outside ${style}" id="${idMenu}">
                <div class="${clsPref}minimal">
                    <div class="${clsPref}fadeInButton" id="${clsPref}fadeInButton"><i class="material-icons ${clsPref}fade-icon" id="${clsPref}fade-icon">chevron_right</i></div>
                </div>
                <div class="${clsPref}maximal ${clsPref}row-container">
                    <div class="${clsPref}header" id="${clsPref}menuTitle">Menu item name</div>
                    <div class="${clsPref}content">
                            <div class="${clsPref}btn-container ${clsPref}btn1">Button 1</div>
                            <div class="${clsPref}btn-container ${clsPref}btn2">Button 2</div>
                            <div class="${clsPref}btn-container ${clsPref}btn3">Button 3</div>
                            <div class="${clsPref}btn-container ${clsPref}btn4">Button 4</div>
                            <div class="${clsPref}btn-container ${clsPref}btn5">Button 5</div>
                            <div class="${clsPref}btn-container ${clsPref}btn6">Button 6</div>
                            <div class="${clsPref}btn-container ${clsPref}btn7">Button 7</div>
                            <div class="${clsPref}btn-container ${clsPref}btn8">Button 8</div>
                            <div class="${clsPref}btn-container ${clsPref}btn9">Button 9</div>
                            <div class="${clsPref}btn-container ${clsPref}btn10">Button 10</div>
                            <div class="${clsPref}btn-container ${clsPref}btn11">Button 11</div>
                            <div class="${clsPref}btn-container ${clsPref}btn12">Button 12</div> 
                    </div>
                </div>
        </div>`;
    }
       
    addButtonObj(  obj ) {
        this.buttons.push( obj );
    }
       
        
    addButton( name, title, func) {
        this.addButtonObj( {
            name : name,
            title : title,
            listener : func
        });
    }
       
    init() {
        if (this.inited)
            return;
        this.inited = true;
        // Finmally create Menu
        if(!this.createMenu()) {
            this.inited = false;
            return;
        }
        function showMenu(){
            if(menu.classList.contains(clsPref + 'outside')) {
                menu.classList.remove(clsPref + 'outside');
                menu.classList.add(clsPref + 'inside');
                document.getElementById(clsPref + "fade-icon").textContent = 'chevron_right';    
            }
        }
        function hideMenu(){                
            let menu = document.getElementById(Constants.IdMenu);    
            if(menu.classList.contains(clsPref + 'inside')) {
                menu.classList.remove(clsPref + 'inside');
                menu.classList.add(clsPref + 'outside');
                document.getElementById(clsPref + "fade-icon").textContent = 'chevron_left';    
            }
        }
        function toggleMenu(){
              if(menu.classList.contains(clsPref + 'outside')) {
                  showMenu();
              } else  if(menu.classList.contains(clsPref + 'inside')) {
                  hideMenu();
              }
        }
        
        let activateButton = function( ev ) {
            // THIS IS BUTTON
            if(!this.classList.contains(clsPref + 'hovered')) {
                this.classList.add(clsPref + 'hovered');
            }  
        };
        let deActivateButton = function( ev ) {
            // THIS IS BUTTON
            if(this.classList.contains(clsPref + 'hovered')) {
                this.classList.remove(clsPref + 'hovered');
            }   
        };  
        let releaseAction = (btnObj, button, ev )=> {
            console.log("released");
            hideMenu();
            let disabled = button.getAttribute('data-disabled');
            try {
                if(btnObj && btnObj.listener && disabled !== false) {                    
                    btnObj.listener( ev);
                }
            } catch ( e ) {
                console.error(e);
                ModalHandler.showEx(e);
            }
         };
        
        let menu = document.getElementById(Constants.IdMenu);
        menu.addEventListener('mouseleave', ( ev )=> {
        //    hideMenu();
        });
        menu.addEventListener('animationend', ( ev ) => {
            console.log("Animation END");
        });

        let btns = document.querySelectorAll('#'+ Constants.IdMenu + ' .'+clsPref +'btn-container');
        let counter = 0;
        for (let button of btns) {                   
            let btnObj = this.buttons[counter];
            
            button.addEventListener('mouseenter', activateButton);
            button.addEventListener('mouseover', activateButton);   
            button.addEventListener('mouseout', deActivateButton);              
        //    button.addEventListener('touchend', ( ev) => releaseAction(btnObj, button, ev));
         //   button.addEventListener('mouseup', ( ev) => releaseAction(btnObj,  button, ev));  
            button.addEventListener('click', ( ev) => releaseAction(btnObj,  button, ev));
            
            if(!btnObj) {
                button.classList.add(clsPref + "invisible");
            } else {
                btnObj.index = counter;
                button.setAttribute('data-name', btnObj.name);
                if(btnObj.build ) {
                    btnObj.build(button);
                }
            }       
            counter++;
        }

        let fadeInButton = document.getElementById(clsPref + 'fadeInButton');
        //fadeInButton.addEventListener('mousedown', ( ev ) => showMenu());
       // fadeInButton.addEventListener('touchstart', ( ev ) => showMenu());
        fadeInButton.addEventListener('click', ( ev ) => toggleMenu());
       // fadeInButton.addEventListener('mouseup', ( ev )=> {
        //    hideMenu();
        //});
        fadeInButton.addEventListener('mouseenter', activateButton);
        fadeInButton.addEventListener('mouseover', activateButton);   
        fadeInButton.addEventListener('mouseout', deActivateButton);  
    }
    
    disable ( dataName , disabled ) {
        let objs = document.querySelector(`[data-name="${dataName}"]`);
        objs.setAttribute('data-disabled', disabled);
        if(disabled) {
            if(!objs.classList.contains(clsPref + 'disabled')) {
                objs.classList.add(clsPref + 'disabled');
            }   
        } else {
            objs.classList.remove(clsPref + 'disabled');            
        }
    }
    
    toggleMenuClass ( menuClass, on ) {
        let objs = document.getElementById(Constants.IdMenu);
        if(!on && objs.classList.contains(menuClass)) {
            objs.classList.remove(menuClass);
        } else if(on && !objs.classList.contains(menuClass)) {
             objs.classList.add(menuClass);
        }
    }
    
    createMenu() {
     // can access and modify the DOM
        var bodyNode = document.body;
        var menuRoot = document.getElementById(Constants.IdMenu);
        if (!menuRoot && bodyNode) {
            menuRoot = document.createElement("div");
            bodyNode.appendChild(menuRoot);
            menuRoot.innerHTML = this.menuTemplate;
            menuRoot.data = {};
            this.menuRoot = menuRoot;
            this.setSprite();
            return true;
        }
        return false;
    }
    
    getFadeButton(){
        return document.getElementById('#'+clsPref +'fadeInButton');
    }
         
    getButton( name ){
        if(!name) {
            return;
        }
        return document.querySelector('#'+ Constants.IdMenu + ' .'+clsPref +'btn-container[data-name="'+name+'"');
    }     
    
    setSprite() {
       try {
           var bUndef = true;
           try {
               bUndef = browser === undefined;
           } catch (e) {};
           //Code for displaying <extensionDir>/images/myimage.png: 
           var itsMe = chrome && bUndef ? "chrome" : "browser";
           this.menuRoot.className += Constants.prefix + itsMe;
       } catch (e) {
           console.log("ERROR", e);
       }
       /// background-image:url('chrome-extension://__MSG_@@extension_id__/icons/icon_sprite.png');
   }
}