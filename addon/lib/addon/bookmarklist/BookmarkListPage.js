/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global ModalHandler */
/* global Utils */
import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import $ from 'jquery'
import jquery from 'jquery'
import naturalSort from 'naturalSort'
import BookmarkList from 'lib/addon/bookmarklist/BookmarkList.js'
import BackgroundSessionClient from 'lib/addon/common/BackgroundSessionClient.js'

export default class BookmarkListPage {
    
    constructor(){
        this.tagsMarked = [];
        this.bookmarkList = null;
        this.sess = new BackgroundSessionClient();
    }
    
    toogleTagSelected(tagName){
        var idx = this.tagsMarked.indexOf(tagName);
        if(idx >= 0){
            this.tagsMarked.splice(idx, 1);
            $('#bookmarks .tagList').find('[data-tag="'+tagName+'"]').removeClass('marked');
        } else {
            this.tagsMarked.push(tagName);
        }           
    }   
    
    replaceTagSelection ( res ) {
        this.tagsMarked.splice(0, this.tagsMarked.length);  
        if(res && res.length) {
            for (let i = 0, max = res.length; i < max; i++) {
               if(res[i]) {
                   this.tagsMarked.push(res[i]);
               }
            } 
        }  
    }
    
    highlightTagForSelection(tagName){
        var idx = this.tagsMarked.indexOf(tagName);
        if(idx >= 0){
            $('#bookmarks .tagList').find('[data-tag="'+tagName+'"]').addClass('marked');
        } else {
            $('#bookmarks .tagList').find('[data-tag="'+tagName+'"]').removeClass('marked');
        }           
    }    
    
    highlightTagsForSelection () {
        let res = this.tagsMarked;
        if(res && res.length) {
            for (let i = 0, max = res.length; i < max; i++) {
               if(res[i]) {
                    this.highlightTagForSelection(res[i]);
               }
            } 
        }  
    }
    
    searchAndLoadByTags( callback ){
        // Set the current selection to the lastTags  (SLICE==Copy the array)
        this.sess.set( 'lastTags', this.tagsMarked.slice(), ( res, error) => {               
            if(error) {
                ModalHandler.showErr(error);
                return;
            }
            try {            
                this.bookmarkList.reload( callback );                   
            } catch ( e ) {
                console.error(e);
                ModalHandler.showEx(error);
            }
        });
    }
    
    searchAndLoadByLastTags(){        
        this.sess.get( 'lastTags', (res, error) => {            
            console.log("LASTTAG", res, error);
            if(error) {
                ModalHandler.showErr(error);
                return;
            }             
            try {           
                this.replaceTagSelection(res);
                this.bookmarkList.reload(
                    ( bookmarks)=>{         
                        this.setAvailbilityClassToTags(bookmarks);
                        this.highlightTagsForSelection();
                    }
                );                   
            } catch ( e ) {
                console.error(e);
                ModalHandler.showEx(error);
            }
        });
    }
    
    // Set classes to the spans where the tag is available in the bookmark list
    setAvailbilityClassToTags ( bookmarks ) {
        let availTags = [];
        for(let bm of bookmarks) {
            if(bm.tags) {
                for(let k of bm.tags) {
                    if(availTags.indexOf(k)< 0) {
                        availTags.push(k);
                    }
                }
            }
        }        
        if(availTags.length > 0) {
            $('#bookmarks .tagMenu.tagList .tag')
                    .removeClass('avail')
                    .removeClass('notAvail')
                    .addClass('notAvail'); 
            for(let tag of availTags) {
                $('#bookmarks .tagMenu.tagList').find('[data-tag="'+tag+'"]').addClass('avail').removeClass('notAvail');
            }
        } else {            
            $('#bookmarks .tagMenu.tagList .tag')
                    .removeClass('avail')
                    .removeClass('notAvail'); 
        }
        
    }
    
    appendAndSortTags(res) {
        $('#bookmarks .tagList').empty();
        naturalSort.insensitive = true;
        let keys = Object.keys(res).sort(naturalSort);
        let lastChar;
        for(let key of keys) {
            let txt = key;
            if(key.length > 0) {
                let fChar = key.toLowerCase().charAt(0);
                if(fChar !== lastChar) {
                    lastChar = fChar;
                    $('#bookmarks .tagList').append ('<span class="label alphabet">' + key.toUpperCase().charAt(0) +"</span>");
                }
            }
            $('#bookmarks .tagList').append(`<span class="tag label label-success " data-tag="${key}">${txt} (${res[key]})</span>`);
        } 
    }
    
    loadTags( callback ){
        Utils.exec({ cmd : "tags.load" }, ( res , error) => {
            if(error) {
                ModalHandler.showErr(error);
                return;
            }
            this.appendAndSortTags(res);
            callback(res, error);
        });
    }
    
    
    /**
     * Init the page
     * 
     * @public
     * 
     */
    start() {        
        this.bookmarkList = new BookmarkList( ( bmListCallback ) => {        
            Utils.exec({ cmd : "db.searchByTags", tags : this.tagsMarked }, ( res , error) => {
                bmListCallback(res, error);
            }); 
        }, "bookmarkList");
        // And append it to main html
        this.bookmarkList.appendTo( $('#bookmarks #tagSearchResult') );

        $('#bookmarks .tagList').on('click', ( ev ) => {
            var tagName = ev.target.getAttribute('data-tag');
            if(!tagName){
                return;
            }
            this.toogleTagSelected(tagName);
            this.searchAndLoadByTags(( bookmarks)=> {                
                this.setAvailbilityClassToTags(bookmarks);
                this.highlightTagsForSelection();
            });
        });

        this.bookmarkList.getLastUsedSortFunction( ( sortName ) =>  {            
            $('#sortType').text(sortName);
            this.loadTags(( res, error) => {                
                this.searchAndLoadByLastTags(); 
            });           
        })
    }
    
}