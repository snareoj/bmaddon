/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Utils from 'lib/Utils.js'
import $ from 'jquery'
import * as bootstrap from 'bootstrap'
import Humane from 'humane'

function Msg(cl, id, title, msg){
    this.cl = cl;
    this.id = id;
    this.title = title;
    this.defMsg =  msg;
}

var ModalHandler =  {
  
    templateModalMessage : `
        <div id="modalWindow" class="modal fade modal-window" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">       
                  </div>
                  <div class="modal-footer" id="modalButtons">
                  </div>
            </div>
          </div>
        </div>
    `,
    
    _i18n : function ( id ) {
        return Utils._i18n(id);
    },
    
    showQuestion : function(id, defMsg){
       return this._showgeneric(new Msg('warning', id, this._i18n ('question_title') ,defMsg), [
           { id : 'ok',  text : this._i18n ('button_ok')},
           { id : 'cancel', text :  this._i18n ('button_cancel') }
       ]);
    }, 
    
    showWarn : function(id, defMsg){
        this._showgeneric(new Msg('alert-warning', id, this._i18n ('warning_title'), defMsg));
    },    
    
    showMesg : function(id, defMsg){
        this._showgeneric(new Msg('', id, 'Info' ,defMsg));
    },
    
    showErr : function(error) {
        if(!error) {
            // Ignored
            return;
        }
        var dfMsg = "<p>An error occured, action can not be executed.</p>";
        for (var key in error) {
            if (error.hasOwnProperty(key)) {
              dfMsg += key + ": " + error[key]+"<br/>";
            }
          }
        this._showgeneric(new Msg('danger', 'msg.error', 'Error', dfMsg));
    },
    
    showEx : function(error) {
        this._showgeneric(error);
    },
    
    _showgeneric : function( obj, btnArray ) {        
        let modal = $('#modalWindow');
        if(modal.length === 0) {
            modal = $(this.templateModalMessage);
            $('body').prepend(modal);
        }
        modal.find("#text").text("An error occured.");
        let txt = '';
        modal.find(".modal-content").removeClass().addClass('modal-content');
        if( obj  instanceof Error) {
            modal.find(".modal-title").text("Exception");
            modal.find(".modal-content").addClass("danger ");
            txt = this._errorText( obj );
        } else if( obj  instanceof Msg)  {
            modal.find(".modal-title").text(obj.title);
            modal.find(".modal-content").addClass(obj.cl);
            txt =  obj.defMsg ;
        } else if( typeof obj === 'object' && obj.message && obj.stack) {            
            modal.find(".modal-title").text(obj.title || 'Error');
            modal.find(".modal-content").addClass("danger ");
            txt = this._errorText( obj );
        } else if( typeof obj === 'object' && obj.message) {            
            modal.find(".modal-title").text(obj.title || 'Hint');
            txt =  obj.message ;
        } else {
            txt =  obj ;
        }
        modal.find(".modal-body").html(txt);
        
        
        return new Promise((resolve, reject) => {        
            let btns =  modal.find('#modalButtons');
            btns.empty();
            if(!btnArray || btnArray.length === 0) {
                let closeBtn = $('<button type="button" class="btn btn-default">Close</button>');
                closeBtn.on("click", ( ev ) => {
                    modal.modal('hide');
                    resolve();
                });
            } else {
                for( let b of btnArray) {
                    let closeBtn = $(`<button type="button" class="btn btn-default" >${b.text}</button>`);
                    btns.append(closeBtn);
                    closeBtn.on("click", ( ev ) => {
                          resolve( { 'btn' : b, 'modal' : modal } );
                    });
                }
            }
            modal.modal('show');
        });
    },
    
    
    _errorText : function(error){
        var stack = error.stack;
        stack = stack.replace("\n", "<br/>");
        return `   <p>${error.message}</p>    
                   <p>${error.lineNumber},${error.columnNumber} in ${error.fileName}</p>      
                   <p class="small">${stack}</p>`;                               
    },
    
    notify : function ( msgId, cl ) {
        cl = 'humane-flatty-' +  ( cl || 'info' );     
        let msg = this._i18n(msgId);
        Humane.log(msg, { timeout: 4000, clickToClose: true, addnCls:  cl });
    }
    
};

export default ModalHandler;