/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * Handles the persistence cmds like updateBookmark, createBookmark etc.
 */
import Utils from 'lib/Utils.js';
import JSONImpExporter from 'lib/JSONImporter.js';

export default function BookmarkPersistenceCmdHandler(settings, persistence, eventBus) {

    this.settings = settings;
    this.eventBus = eventBus;
    this.persistence = persistence;
}
/**
 * Handles navigation events
 * @param {cmd} cmdMessage 
 * @param {object} sender
 * @param {Function} callback
 * @return {boolean} true if cmd was handled
 */
BookmarkPersistenceCmdHandler.prototype.handle = function (cmdMessage, sender, callback) {
    if (cmdMessage.cmd === "db.createAndPersistBookmark" && cmdMessage.obj) {
        this.createAndPersistBookmark(cmdMessage.obj, callback);
        return true;
    } else if (cmdMessage.cmd === "db.createBookmark" && cmdMessage.obj) {
        this.createBookmark(cmdMessage.obj, callback);
        return true;
    } else if (cmdMessage.cmd === "db.removeBookmark" && cmdMessage.obj) {
        this.removeBookmark(cmdMessage.obj, callback);
        return true;
    } else if (cmdMessage.cmd === "db.listBookmarks") {
        this.listBookmarks(callback);
        return true;
    } else if (cmdMessage.cmd === "db.getBookmarkByURL" && cmdMessage.url) {
        this.getBookmarkByURL(cmdMessage.url, callback);
        return true;
    } else if (cmdMessage.cmd === "db.getBookmarkById" && cmdMessage.id) {
        this.getBookmarkById(cmdMessage.id, callback);
        return true;
    } else if (cmdMessage.cmd === "db.updateBookmark" && cmdMessage.obj) {
        this.updateBookmark(cmdMessage.obj, callback);
        return true;
    } else if (cmdMessage.cmd === "db.searchByTags" && cmdMessage.tags) {
        this.searchByTags(cmdMessage.tags, callback);
        return true;
    } else if (cmdMessage.cmd === "db.rebuildBookmarkIndex") {
        this.persistence.rebuildIndex( callback );
        return true;
    } else if ( cmdMessage.cmd === 'main.clearStorage') {
        this.persistence.clearBookmarks( (res, error ) => {
           callback ( 'cleared bookmarks', error);  
        });
        return true;
    } 
    return false;
};


/**
 * If a listener is around, wrap the callback into the listener.
 * @param {string} cmdName
 * @param {type} callback
 * @returns {void}
 */
BookmarkPersistenceCmdHandler.prototype._wrapCallback = function (cmdName, callback) {
    if(this.eventBus) {
        return ( result, error ) => {
            try {
                callback(result, error);
            } catch( e) {
                console.error("Error while calling callback in persistnceCmdHandler ", e);
            } finally {
                this.eventBus.handleCmd({
                    cmdName : cmdName,
                    result : result,
                    error : error
                }, null, ( r, e ) => {
                    if(e) {
                        //console.error("Error while handling persistence result ", e);
                    } else {
                        console.log("Raised " + cmdName);
                        //  NOthing
                    }
                },
                true);
            }
        };
    } else {
        return callback;
    }
};

/**
 * Update a bookmark
 * @param {bookmark} bookmark
 * @param {type} callback
 * @returns {void}
 */
BookmarkPersistenceCmdHandler.prototype.updateBookmark = function (bookmark, callback) {
    this.persistence.updateBookmark(
            bookmark, 
            this._wrapCallback("res.db.updateBookmark", callback));
};

/**    
 * Save a bookmark
 * @param {type} bookmark
 * @param {Function} callback
 * @returns {undefined}
 * 
 */
BookmarkPersistenceCmdHandler.prototype.createAndPersistBookmark = function (bookmark, callback) {
    this.persistence.createAndPersistBookmark(
            bookmark, this._wrapCallback("res.db.createAndPersistBookmark", callback));
};

/**
 * Create a bookmark from a pageInfo Object, see pageinfo.js
 * @param {PageInfo} pageinfo
 * @param {Function} callback
 * @returns {void}
 */
BookmarkPersistenceCmdHandler.prototype.createBookmark = function (pageinfo, callback) {
    this.persistence.createBookmark(
            pageinfo, this._wrapCallback("res.db.createBookmark", callback));
};

/**
 * Removes a bookmark from persistence
 * @param {String} id
 * @param {type} callback
 * @returns {void}
 */
BookmarkPersistenceCmdHandler.prototype.removeBookmark = function (id, callback) {
    this.persistence.removeBookmark(
            id, this._wrapCallback("res.db.removeBookmark", callback));
};

/**
 * List all available Bookmarks
 * @param {type} callback
 * @returns {void}
 */
BookmarkPersistenceCmdHandler.prototype.listBookmarks = function (callback) {
    this.persistence.listBookmarks(
            this._wrapCallback("res.db.listBookmarks", callback));
};

/**
 * Get a bookmark from stroage
 * @param {String} url
 * @param {function} callback
 * @returns {void}
 */
BookmarkPersistenceCmdHandler.prototype.getBookmarkByURL = function (url, callback) {
    this.persistence.getBookmarkByURL(url, 
            this._wrapCallback("res.db.getBookmarkByURL", callback));
};

/**
 * Get a bookmark from stroage
 * @param {String} id
 * @param {function} callback
 * @returns {void}
 */
BookmarkPersistenceCmdHandler.prototype.getBookmarkById = function (id, callback) {
    this.persistence.getBookmarkById(id, 
            this._wrapCallback("res.db.getBookmarkById", callback));
};
    /**
 * Get a bookmark from stroage
 * @param {array} tagList
 * @param {function} callback
 * @returns {void}
 */
BookmarkPersistenceCmdHandler.prototype.searchByTags = function (tagList, callback) {
    this.persistence.searchByTags(tagList, 
            this._wrapCallback("res.db.searchByTags", callback));
};


