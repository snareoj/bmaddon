/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * Bookmark Service, listens to default bookmark API
 * 
 * Tries to stay in "sync" of the "normal" bookmarks:
 * If a normal BOokmark is added / updated, add a bookmark in this addon.
 * If a normal BOokmark is deleted, try to delete bookmark in this addon.
 * If a bookmark is added / updated in this addon, add it to the default bookmarks.
 * If a bookmark is removed in this addon, remove it from the default bookmarks.
 * 
 * Bookmark TreeNode:
 * https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/bookmarks/BookmarkTreeNode
 * 
 */

import Utils from 'lib/Utils.js';
import NativeBookmarkScanner from 'lib/NativeBookmarkScanner.js';

export default function BookmarkAPIService( settings, eventBus ) {    
    
    this.settings = settings;
    this.eventBus = eventBus;
    
    this.isImporting = false;
    this.importCreated = [];
    
}

/**
 * Attach to bookmark API if available. Does nothing if no bookmark API is detected.
 * 
 * @returns {undefined}
 */
BookmarkAPIService.prototype.attach = function () {
    
        if(!Utils.get().bookmarks) {
            console.log("No browser bookmark API, not attaching");
            return;
        } 
        
        console.log("Browser bookmark API detected, attaching");
        
        // listen for bookmarks being created
        Utils.get().bookmarks.onCreated.addListener( (id, e) => this._nat_onCreated(e));
        Utils.get().bookmarks.onRemoved.addListener( (id, e) => this._nat_onRemoved(e));
        Utils.get().bookmarks.onChanged.addListener( (id, e) => this._nat_onChanged(e));
        
        // NOt supported by firefox
        if(Utils.get().bookmarks.onImportBegan) {
            Utils.get().bookmarks.onImportBegan.addListener( e => this._nat_onImportBegan(e));
            Utils.get().bookmarks.onImportEnded.addListener( e => this._nat_onImportEnded(e));
        }
    
        this.eventBus.addCmdHandler(this);
};

/**
 * Handles native onCreated event
 * @param {object} bookmarkTreeNode 
 */
BookmarkAPIService.prototype._nat_onCreated = function ( bookmarkTreeNode ) {
   if(this.isImporting) {
        this.importCreated.push(bookmarkTreeNode);
    } else {
        this._saveSingle(bookmarkTreeNode);
    }
};


/**
 * Handles native onRemoved event
 * @param {object} bookmarkTreeNode 
 */
BookmarkAPIService.prototype._nat_onRemoved = function ( bookmarkTreeNode ) {
    if(!bookmarkTreeNode.node || !bookmarkTreeNode.node.url) {        
        return;
    }
    this.eventBus.handleCmd( {
       cmd : 'db.removeBookmark',           
       obj : Utils.urlToBookmarkId(bookmarkTreeNode.node.url)
    },
    null,
    ( res, error) => {
        if(error) {
            console.error("Removing bookmark failed ", error);
        } else {
            console.log("Bookmark removing", res);
        }
    });
};

/**
 * Handles native onChanged event
 * @param {object} bookmarkTreeNode 
 */
BookmarkAPIService.prototype._nat_onChanged = function ( bookmarkTreeNode ) {
    this._saveSingle( bookmarkTreeNode );
};

/**
 * Handles native onImportBegan event
 * @param {object} bookmarkTreeNode 
 */
BookmarkAPIService.prototype._nat_onImportBegan = function () {
    this.isImporting = true;
};

/**
 * Handles native onImportEnd event
 */
BookmarkAPIService.prototype._nat_onImportEnd = function () {
    this.isImporting = false;
    this._bulkSave(0, ( res, error) => {
        console.log("Bulk inserted count:"+ res.length);
    });
};

/**
 * Handles native onImportEnd event
 * @param {object} natBookmark 
 */
BookmarkAPIService.prototype._bulkSave = function (curIndex, callback) {
    if(curIndex < this.importCreated.length){
        this._saveSingle(this.importCreated[curIndex], ( res, error)=> {
            this._bulkSave( curIndex + 1, callback);
        });
    } else {
        callback(this.importCreated);
        this.importCreated = [];
    }
};

/**
 * Saves created / delted bookmarks.
 * @param {object} bookmarkTreeNode 
 * @param {function} callback 
 */
BookmarkAPIService.prototype._saveSingle = function ( bookmarkTreeNode, callback ) {
    let bookmark = this.treeNodeToBookmark(bookmarkTreeNode);
    if(!bookmark) {        
        return;
    }
    this.eventBus.handleCmd( {
       cmd : 'db.createAndPersistBookmark',           
       obj : bookmark
    },
    null,
    ( res, error) => {       
        // Autosync if avail
        this.eventBus.handleCmd({
            cmd : "sync.autoSyncTrigger",
            reason : 'user_added_bookmark_inbrowser'
        }, null, ( res, error) => { 
            if(callback) {
                callback(res, error);
            }        
            if(error) {
                console.error("Saving bookmark failed ", error);
            } else {
                console.log("Bookmark saved", res);
            }
    });
        
        
    });
};


/**
 * Handles native onImportEnd event
 * @param {object} bookmarkTreeNode 
 */
BookmarkAPIService.prototype.treeNodeToBookmark = function ( bookmarkTreeNode ) {
    return NativeBookmarkScanner.prototype.createBookmark(bookmarkTreeNode);
}; 
    

/**
 * Handles Sync Events
 * @param {cmd} cmdMessage 
 * @param {sender} sender
 * @param {Function} callback 
 */
BookmarkAPIService.prototype.handle = function (cmdMessage, sender, callback) {
    
     if(cmdMessage.cmd  === 'res.db.createBookmark' && cmdMessage.result) {
        //this.addTags(cmdMessage.result.tags);
        return true;
    } else if(cmdMessage.cmd  === 'res.db.createAndPersistBookmark' && cmdMessage.result) {
        //this.addTags(cmdMessage.result.tags);
        return true;
    } else if(cmdMessage.cmd  === 'res.db.updateBookmark' && cmdMessage.result) {
       // this.addTags(result.tags);
        return true;
    } else if(cmdMessage.cmd  === 'res.db.removeBookmark' && cmdMessage.result) {
        //this.removeTags(cmdMessage.result.tags);
        return true;
    } 
    
};
