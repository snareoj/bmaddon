/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global ModalHandler */
/* global Utils */
import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import BookmarkTemplate from 'lib/Template_Bookmark.js'
import BookmarkListTemplate from 'lib/Template_BookmarkList.js'
import $ from 'jquery'
import naturalSort from 'naturalSort'

// bookmark_list.js
"use strict";

export default class BookmarkList {
    
    constructor( findFunc, name) {
        this.findFunc = findFunc;
        this.name = name;
        this._prevItem = null;
        this.createBookmarkList();
    }
    
    highlightItem(itemId){
         // "Highlight" selected item
        if(this._prevItem) {
            $('#' + this._prevItem+'[data-role="bookmark"]').removeClass("well");
        }
        this._prevItem = itemId;
        $('#'+ itemId+'[data-role="bookmark"]').addClass("well");
    }
          
    editItem(itemId) {
        // Open ID
       window.location.hash=itemId;
       Utils.exec( {
           "cmd" : "nav.editBookmark",
           "id" : itemId
       }, (res, error) => {                                        
           if(error) {
               ModalHandler.showErr(error);
               return;
           }
       });
    }
       

    openBookmarkById(itemId) {
        // Open ID
        Utils.exec( { 
               "cmd" : "db.getBookmarkById", 
               "id" : itemId 
           }, (res1, error1) => {                        

               if(error1) {
                   ModalHandler.showErr(error1);
                   return;
               }

               Utils.exec( { 
                   "cmd" : "nav.openBookmarkURL", 
                   "obj" : res1 }, (res2, error2) => {
                       if(error2) {
                           ModalHandler.showErr(error2);
                           return;
                       }
               });
       });
    }

    deleteBookmark(bookmark){

        if(!bookmark){
            throw Error("No bookmark supplied");
        }        

        var deleteQuestion = Utils.get().i18n.getMessage(
            "bookmark_delete_confirm",  // string
            bookmark.info ? bookmark.info.url : "No URL"// optional any
        );
        let q = ModalHandler.showQuestion(
                'bookmark_delete_confirm',
                 deleteQuestion
                );
        q.then( ( res ) => {      
            res.modal.modal('hide');        
            if(res.btn.id === "ok") {
                        //delete them.
                  Utils.exec( { 
                      "cmd" : "db.removeBookmark", 
                      "obj" : 
                              bookmark.id },
                  ( res, error) => {
                    if(res) {  
                        // Deleted! Reload the list (TODO Exactly remove the item from the list
                        //listTemplate.reload();
                        $('#' +bookmark.id+'[data-role="bookmark"]').remove();

                        Utils.pExec({
                            cmd : "sync.autoSyncTrigger",
                            reason : 'user_deleted_bookmark_in_bookmarklist'
                        });
                        
                    } else {                        
                        ModalHandler.showErr(error);
                    }
                  });
            } else {
            }          
        });
    }
    
    createBookmarkList() {
        // 'renderer' for each Item inside the list.
        var  _renderer = ( listTemplate, bookmark) => {      
            var bmTmpl = new BookmarkTemplate(bookmark);             
            var openURL = $('<button class="btn btn-default" id="url"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> open</button>');
            openURL.on('click', ( e ) => {
                this.openBookmarkById(bookmark.id);
            });        
            bmTmpl.addAction( openURL );
            var edit =  $('<button class="btn btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> edit</button>');
            edit.on('click', ( e ) => {
                this.editItem(bookmark.id);
            });
            bmTmpl.addAction( edit );
            var deleteBM =  $('<button class="btn btn-default"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> delete</button>');
            deleteBM.on('click', ( e ) => {
                this.deleteBookmark(bookmark);
            });
            bmTmpl.addAction( deleteBM );      
            return bmTmpl;
        };


        let listTemplate = new BookmarkListTemplate( this.findFunc, _renderer , this);
        // THis key is used for persistence in Settings
        listTemplate.viewClassKey = this.name+ ".taglistOfStartPage";        

        listTemplate._prepare();

        //<!-- # Reload List -->
        var btnReload = $('<button class="btn btn-default" id="btnReload" ><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> </button>');
       // btnReload.html(Utils.get().i18n.getMessage("btn_reload_text"));
        btnReload.on('click', ( ev ) => {
            try {
                listTemplate.reload();
            } catch( e ) {
                console.error("Cant reload", e);
                ModalHandler.showEx(e);
            }
        });
        listTemplate.addAction(btnReload);
                
        let sort = $(`<div class="dropdown" id="sortDropdown">
                    <button class="btn btn-default btn-sm dropdown-toggle" type="button" id="sortDropdownBtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      Sort <span id="sortType"></span>
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="#" data-value="titleAZ">Title A-Z</a></li>
                      <li><a href="#" data-value="titleZA">Title Z-A</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#" data-value="hits0N">Hits 0-N</a></li> 
                      <li><a href="#" data-value="hitsN0">Hits N-0</a></li>
                    </ul>
                </div>`);        
                
        sort.find('a[data-value]').on('click', ( ev ) => {
            var sortName=  ev.target.getAttribute('data-value');
            if(!sortName){
                return;
            }
            
            $('#sortType').text(sortName);
            this.setLastUsedSortFunction( sortName );
        });

        listTemplate.addAction(sort);
        this.listTemplate = listTemplate;
    }
    
    onItem (type, itemId) { 
        if(type === 'doubletap') {
            // Open in Background
        } else if(type === 'tap') {
            this.highlightItem(itemId);
        } else if(type === 'swipeleft') {
            this.editItem(itemId);
        } else if(type === 'swiperight') {
            this.openBookmarkById(itemId);
        } 
    }
    
    onRendered(){
        if(window.location.hash && window.location.hash.length > 2)  {
            var  el = document.getElementById(window.location.hash.substring(1));
            if(el) {
                setTimeout(() => {
                    el.scrollIntoView();
                }, 1000);
            }
        }
    }
    
    appendTo ( parent ) {
        this.listTemplate.appendTo( parent );
    }
    
    
    reload ( callback ) {
        this.listTemplate.reload( callback );
    }
        
    getLastUsedSortFunction( callback ){
        Utils.exec({ 
                cmd : "settings.get", 
                key : this.name + '.lastUsedSortFunction'
              }, (res, error) => {
                if(error) {
                   ModalHandler.showEx(error);
                   return;
                }
                this.selectSortFunction ( res );
                if(callback) {
                    callback(res, error);
                }
             });
    }
            
    setLastUsedSortFunction( funcName ){
        Utils.exec({ 
                cmd : "settings.set", 
                key : this.name + '.lastUsedSortFunction',
                value : funcName
              }, (res, error) => {
                if(error) {
                   ModalHandler.showEx(error);
                   return;
                }
                this.selectSortFunction ( funcName );
                this.listTemplate.renderList();
             });
    }
    
    selectSortFunction ( nameOfSortFunction ) {        
        if(!nameOfSortFunction) {
            nameOfSortFunction = 'titleAZ';
        }
        let func = this.listSortFunctions()[nameOfSortFunction];
        if(!func) {
            func = this.listSortFunctions()['titleAZ'];
        }
        this.listTemplate.setSortFunc(func);
    }
    
    listSortFunctions(){
        function h(bm) {
            if(bm) {
                return bm.count;
            } else {
                return '';
            }
        }
        
        function t(bm) {
            if(bm && bm.info && bm.info.title) {
                return bm.info.title;
            } else {
                return '';
            }
        }
        
        return { 
            'titleAZ' : function(a, b){
                naturalSort.insensitive = true;
                return naturalSort(t(a),t(b));
            },
            'titleZA' : function(a, b){
                naturalSort.insensitive = true;
                return naturalSort(t(b),t(a));                    
            }, 
            'hits0N' : function(a, b){
                naturalSort.insensitive = true;
                return naturalSort(h(b),h(a));     
                    
            }, 
            'hitsN0' : function(a, b){
                naturalSort.insensitive = true;
                return naturalSort(h(a),h(b));                         
            }};
    }
}

