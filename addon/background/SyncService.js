/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * Bookmark SyncService
 */
import Utils from 'lib/Utils.js';
import JSONImpExporter from 'lib/JSONImporter.js'
import * as sync from 'lib/WebDavNextCloudSync.js';


export default function SyncService( settings, eventBus ) {    
    this.AUTO_SYNC_ALARM_NAME = 'sync.autosync';
    this.settings = settings;
    this.autoSyncTriggered = false; 
    this.autoSyncSyncOnNextAlarm = false; 
    this.eventBus = eventBus;    
    
    Utils.get().alarms.onAlarm.addListener( ( alarm ) => {
        if(alarm && alarm.name === this.AUTO_SYNC_ALARM_NAME) {
            this.onAutoSyncAlarm();
        }
    });
}


/**
 * Returns the current used Storage.
 * See https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage/StorageArea/set
 * @returns {unresolved}
 */
SyncService.prototype.createClient = function(){    
    return new sync.WebDavNextCloudSync( 
        this.settings.getProp('webdav_url'),
        this.settings.getProp('webdav_user'),
        this.settings.getProp('webdav_password')               
    );
};
/**
 * Returns the current used Storage.
 * See https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage/StorageArea/set
 * @returns {unresolved}
 */
SyncService.prototype.get = function(){
    var itsMe = Utils.get();
    return itsMe;
};

/**
 * Get date when this was synced the lasttime.
 * @param {function} callback
 * @returns {undefined}
 */
SyncService.prototype.lastSync = function ( callback ) {  
    callback( this.settings.getProp("webdav_lastDownloadDate") );
};

/**
 * onBookmarkAdded
 * @param {bookmark} bookmark
 * @param {type} callback
 * @returns {undefined}
 */
SyncService.prototype.onBookmarkAdded = function ( bookmark , callback) {    
};

/**
 * onBookmarkDeleted
 * @param {bookmark} bookmark
 * @param {type} callback
 * @returns {undefined}
 */
SyncService.prototype.onBookmarkDeleted = function ( bookmark , callback) {    
};

/**
 * onBookmarkUpdated
 * @param {bookmark} bookmark
 * @param {type} callback
 * @returns {undefined}
 */
SyncService.prototype.onBookmarkUpdated = function ( bookmark , callback) {    
};

/**
 * Check if Sync is GENERALLY ON
 * @returns {boolean} true if persistencetype is set to "webdav"
 */
SyncService.prototype.isSyncActive = function () {    
    let sync = this.settings.getProp('persistencetype', false);
   // console.log("Sync Active: ", sync);
    return sync === 'webdav';
};

/**
 * Check if AUTO SYNC IS ON
 * @returns {Boolean}
 */
SyncService.prototype.isAutoSyncActive = function () {    
    let sync = this.settings.getProp('webdav_autosync', false);
   // console.log("Sync Active: ", sync);
    return !!sync;
};

/**
 * 1. called on start (see background script)
 * 2. check if sync is neccessary
 * 2a) if TRUE:
 *      3. Show Notification we get an msg ID
        4. check if autosync is on
        4a) IF TRUE: Start Sync
            5) Update msgID that we are syncing
        4b) IF FALSE: nothing
   2b) if FALSE: done
        3) MSg we dont have to do anything
 * @param {type} callback
 * @returns {undefined}
 */
SyncService.prototype.checkOnStart = function ( callback ) {   
    if(!this.isSyncActive()) {
        return;
    }
    
    this.checkIfSyncNeccessary( ( checkResult, error ) => {
        
        if(error) {
            console.error(error);
            callback(null, error);
            return;
        }
        
        if(checkResult && checkResult.hasChanges) {
            console.log("Update neccessary: ", checkResult);
            
            let autoSync = this.isAutoSyncActive();
            if(autoSync) {
               if(checkResult.conflict) {
                   // Cant do anything              
                    this.addAutoSyncMessage('sync_autosync_checkOnStart_failed_cause_conflict');  
                    this.addAutoSyncMessage(checkResult.conflictReason);  
                    this.eventBus.handleCmd({
                        cmd : "backend.notify",
                        msgID : "sync_checkonstart_conflict",
                        params : ['i18n:' + checkResult.conflictReason],
                        defMsg : "Conflict detected."
                    }, null, ( res, error ) => {    
                        if(error) {
                            console.error("checkOnStart", error);
                            callback(null, error);
                        } else  {
                            callback('syncservice: conflict ');
                        }
                    }); 

               } else if(checkResult.upload) {

                    this.addAutoSyncMessage(checkResult.hint);  
                    this.doAutoSyncUpload(( r, error) => {    

                       if(error) {
                           this.addAutoSyncMessage('sync_autosync_checkOnStart_upload_failed_cause_of_error', error);  
                           return;
                       } else {
                           this.addAutoSyncMessage('sync_autosync_checkOnStart_upload_success');  
                       }

                        this.eventBus.handleCmd({
                            cmd : "backend.notify",
                            msgID : "sync_checkonstart_synced_upload",
                            params : [ ],
                            defMsg : "Syncronized Bookmarks."
                        }, null, ( res, error ) => {    
                            if(error) {
                                console.error("checkOnStart", error);
                                callback(null, error);
                            } else  {
                                callback('syncservice: synced ' + JSON.stringify(r));
                            }
                        });                           
                   });

               } else if(checkResult.download) {

                    this.addAutoSyncMessage(checkResult.hint);  
                    this.doAutoSyncDownload(( r, error) => {    

                       if(error) {
                           this.addAutoSyncMessage('sync_autosync_checkOnStart_download_failed_cause_of_error', error);  
                           return;
                       } else {
                           this.addAutoSyncMessage('sync_autosync_checkOnStart_download_success');  
                       }

                        this.eventBus.handleCmd({
                            cmd : "backend.notify",
                            msgID : "sync_checkonstart_synced_download",
                            params : [ r.length ],
                            defMsg : "Syncronized "+r.length+" Bookmarks."
                        }, null, ( res, error ) => {    
                            if(error) {
                                console.error("checkOnStart", error);
                                callback(null, error);
                            } else  {
                                callback('syncservice: synced ' + JSON.stringify(r));
                            }
                        });                           
                   });
               }                   
            } else {
                // NO AUTOSYNC ACTIVE
                callback('synservice: changes detected, but no autosync');
                this.eventBus.handleCmd({
                    cmd : "backend.notify",
                    msgID : "sync_checkonstart_syncrequired",
                    params : [ autoSync ? 'i18n:on' : 'i18n:off' ],
                    defMsg : "Syncronisation required!"
                }, null, ( res, error ) => {    
                    if(error) {
                        console.error("checlOnStart", error);
                        callback(null, error);
                        return;
                    }
                });
            }
                
        } else {
             this.addAutoSyncMessage('sync_autosync_checkOnStart_no_changes');  
            console.log("No update needed.", checkResult);
            callback('syncservice: ok');
            this.eventBus.handleCmd({
                cmd : "backend.notify",
                msgID : "sync_checkonstart_syncuptodate",
                params : [],
                defMsg : "Bookmarks uptodate."
            }, null,  ( res, error ) => {
                
            });
        }
    });
};

SyncService.prototype.saveAndSetLastSyncDate = function ( key , cb ) {    
    let d = new Date().toISOString();
    this.settings.setProp(key, d);
    this.settings.setProp("webdav_lastSyncDate", d);
    this.settings.saveOptions((r, e)=> {
        if(cb) {
            cb(r,e);
        }
      });
};

SyncService.prototype.getAsDate = function ( key , def) {    
    let lastSyncDateString = this.settings.getProp(key, def );
    let lastSyncDate;
    if(lastSyncDateString) {
        lastSyncDate = new Date(Date.parse(lastSyncDateString));
    }  
    return lastSyncDate;
};


function isBefore(s,  a , b, delta = 0) {
    if(a && !b) {
        // Date is set but b is not
        return true;
    }    
    if(!a && b) {
        // Date is not set but b is
        return true;
    }
    if(!a  && !b) {
        // Both arent set
        return true;
    }
    let before = a.getTime() - b.getTime() < delta;
   // console.log("before: "+ s+ ": " + b.getTime()+" - " + a.getTime() +" = "+(b.getTime() - a.getTime())+" < " + delta);
    console.log("before: "+ s + ": " + a.toLocaleString()+" - " + b.toLocaleString() +" = " + (before));
    return before;
}
function isSame(s,  a , b, delta = 0) {
    if(a && !b) {
        // Date is set but b is not
        return false;
    }    
    if(!a && b) {
        // Date is not set but b is
        return false;
    }
    if(!a  && !b) {
        // Both arent set
        return true;
    }
    let amoutn = Math.abs(a.getTime() - b.getTime());
    let almostTheSame = amoutn < delta;
   // console.log("before: "+ s+ ": " + b.getTime()+" - " + a.getTime() +" = "+(b.getTime() - a.getTime())+" < " + delta);
    console.log("almostTheSame: "+ s + ": " + a.toLocaleString()+" - " + b.toLocaleString() +" = " + amoutn+ " :"+ (almostTheSame));
    return almostTheSame;
}

/**
 * onBookmarkUpdated
 * @param {type} callback
 * @returns {undefined}
 */
SyncService.prototype.checkIfSyncNeccessary = function ( callback) {    
    try {
        let fileName = this.settings.getProp("webdav_filename", "/samplebookmarks.json" );
        var file = new sync.NclFile(fileName);       
        let webdavClient = this.createClient();
        file.propFind( webdavClient, ( res, error  ) => {
            if(error && error.status !== 404) {
                callback(undefined, error);
                return;
            }
            let lastSyncDate = this.getAsDate('webdav_lastSyncDate');     
            let lastUpload = this.getAsDate('webdav_lastUploadDate');   
            let lastDownload = this.getAsDate('webdav_lastDownloadDate');          
            let remoteModification = file.lastModified();
            let localModification = this.getAsDate('sync_lastDBChange');  
            
            let checkResult = {
                download : false,
                upload : false,
                conflict : false,
                conflictReason : undefined,
                hint : undefined
            };
            if(remoteModification) {           
                checkResult.remote = remoteModification.toISOString();
            }
            let remoteExists = typeof remoteModification !== 'undefined';            
            let localExists = typeof localModification !== 'undefined';
            let localOlderThanRemote = isBefore("local<remote", localModification, remoteModification);
            // If remote was created by last Sync, we have a delta like:
            // - time for upload, in test its ~3 sec!
            let remoteOlderThanSync = isBefore("remote<lastSyncDate1", remoteModification, lastSyncDate, 1100);
            let lastSyncOlderThanLocal = isBefore("lastSyncDate1<local", lastSyncDate, localModification, 1100);
            
            // Delta ==== 10 Secs
            let remoteAlmostSameLikeLastSync = isSame("remote==LastSync", remoteModification, lastSyncDate, 3000);
            
            // Delta can be almost same
            let localAlmostSameLikeLastSync = isSame("local==LastSync", localModification, lastSyncDate, 500);
                     
            
            if(!localExists && !remoteExists) {
                // THERE ARE NO CHANGES - nothiung happend at all
            } else if(localExists && !remoteExists) {
                // Remote does not exist => UPLOAD
                checkResult.upload = true;
                checkResult.hint = "sync_noremote_willbecreated";
            }  else if(!localExists && remoteExists) {
                checkResult.download = true;
                checkResult.hint = "sync_nolocals_importremote";
            } else 
                // SYNC is the same
             if(remoteAlmostSameLikeLastSync && localAlmostSameLikeLastSync) {
                // Nothing Happend!
                checkResult.hint = "sync_no_changes1";
            } else if(remoteAlmostSameLikeLastSync && localOlderThanRemote) {
                // Nothing Happend!
                checkResult.hint = "sync_no_changes2";
            } else if(remoteAlmostSameLikeLastSync && !localOlderThanRemote) {
                // Can push 
                checkResult.upload = true;
                checkResult.hint = "sync_update_remote";
            } else 
                if(localAlmostSameLikeLastSync && remoteOlderThanSync) {
                // Nothing Happend!
                checkResult.hint = "sync_shouldnothappen";
            } else if(localAlmostSameLikeLastSync && !remoteOlderThanSync) {
                // Can pull 
                checkResult.download = true;
                checkResult.hint = "sync_madelocalchanges_synced_remotenewest";
            } else 
            //LOCAL	SYNC	REMOTE
            if(localOlderThanRemote && !lastSyncOlderThanLocal) {
                // Local Changes, After that Synced and REMOTE is Newer =>
                // PULL Changes from REMOTE
                // And we didnt download it yet
                checkResult.download = true;
                checkResult.hint = "sync_madelocalchanges_synced_remotenewest";
            } else 
            //SYNC LOCAL REMOTE	
            if(localOlderThanRemote && lastSyncOlderThanLocal) {           
                // CONFLICT => User Changed Something, his changes werent Synced and Remote CHANGED!     
                checkResult.conflict = true;
                checkResult.conflictReason = 'sync_conflict_remote_newer_but_localchanges';
            } else 
            //REMOTE	LOCAL	SYNC
            if(remoteOlderThanSync && !lastSyncOlderThanLocal) {
                // SYNC is newest... SHould not happen
                checkResult.hint = "sync_shouldnothappen2";
            } else 
            //REMOTE	SYNC	LOCAL
            if(remoteOlderThanSync && lastSyncOlderThanLocal) {
                // We synced the Remote stuff and have now local changes
                checkResult.upload = true;
                checkResult.hint = "sync_madelocalchanges_push";
            } else 
            //SYNC REMOTE LOCAL
            if(lastSyncOlderThanLocal && !remoteOlderThanSync) {
                // CONFLICT  => IT WASNT Synced before user edited sth
                checkResult.conflict = true;
                checkResult.conflictReason = 'sync_conflict_remote_older_but_localchanges';
            } else 
            //REMOTE SYNC LOCAL	
            if(lastSyncOlderThanLocal && remoteOlderThanSync) {                
                checkResult.hint = "sync_shouldnothappen1";
            } else {
                console.error("Not HANDLED");
                checkResult.hint = "sync_shouldnothappen3";
            }          
          
            checkResult.hasChanges = ( checkResult.upload || checkResult.download || checkResult.conflict );
            
            let d = new Date().toISOString();
            this.settings.setProp('webdav_lastSyncCheck', d);
            this.settings.saveOptions(()=> {
                callback ( checkResult );
            });
        }); 
    } catch ( e ) {
        console.error("Failed to check if sync neccessarry", e);
        callback(undefined, e);
    }
};


/**
 * Upload all bookmarks
 * @param {type} callback
 * @returns {undefined}
 */
SyncService.prototype.upload = function ( callback ) {    
    this._upload(callback);
    // Dont need to sync again if its avail
    if(this.autoSyncTriggered) {
        this.clearAutoSync();
    }
};

/**
 * Upload all bookmarks
 * @param {type} callback
 * @returns {undefined}
 */
SyncService.prototype._upload = function ( callback ) {    
    try { 
     //   this.settings.restoreOptions (  ()  => {
        this.eventBus.handleCmd({                
            cmd : 'db.listBookmarks'
        },
        null,
         ( bookmarks, error ) => {
             
             if(error){
                 callback(null, error);
                 return;
             }

            var jsonData = JSON.stringify( bookmarks );
            let fileName = this.settings.getProp("webdav_filename", "/samplebookmarks.json" );
            var file = new sync.NclFile( fileName );

            let webdavClient = this.createClient();
            webdavClient.putFile(file, jsonData, ( res , error ) => {                
               this.saveAndSetLastSyncDate("webdav_lastUploadDate", (r, e)=> {
                    callback ( res, error  );        
                });    
            });
        });
  //     });
    } catch ( e ) {
        callback(undefined , e);
        console.error("Cant upload bookmarks", e);
    }
};


/**
 * Fetch all bookmarks from remote
 * @param {function} callback
 * @returns {undefined}
 */
SyncService.prototype.fetch = function ( callback ) {    
  // this.settings.restoreOptions (  () => {
        let fileName = this.settings.getProp("webdav_filename", "/samplebookmarks.json" );
        var file = new sync.NclFile(fileName);
        let webdavClient = this.createClient();
        webdavClient.getFile( file, ( res, error  ) => {
            console.log("Fetch File ", file, res);
            if(res && !error && res.status === 200) {
                this.importJSON(res.data, callback);
            } else {
                callback ( undefined, error);
            }
        });    
   // });
};


/**
 * Fetch all bookmarks from remote
 * @param {object} data
 * @param {function} callback
 * @returns {undefined}
 */
SyncService.prototype.importJSON = function ( data, callback ) {    
    let importer = new JSONImpExporter(
            (obj, callback) => this._import(obj, callback)
            );
    importer.import( data, ( importedBms, error) => {
        if(error) {
            callback( null, error);
        } else {            
             this.saveAndSetLastSyncDate("webdav_lastDownloadDate", (r, e)=> {
                if(e) {
                    callback( null, e);
                } else {
                    callback(importedBms); 
                }  
            });
        }
    });
};
/**
 * IMports a single itmem
 * @param {type} obj
 * @param {type} callback
 * @returns {undefined}
 */
SyncService.prototype._import = function(obj, callback) {
    this.eventBus.handleCmd({
        cmd: "db.updateBookmark",
        obj: obj
    }, null,
    (bms, error) => {
        if (error) {
            console.log("While importing: ", error);
        } else {
            if (bms.info) {
                console.log("Imported: " + bms.info.url);
            } else {
                console.log("Imported unknown: " + JSON.stringify(bms));
            }
        }
        callback(bms, error);
    });
    
};

SyncService.prototype.addAutoSyncMessage = function (  msg, aparms ) {       
    
    let current = this.settings.getProp('sync_autoSync_messages', []);
    if(current) {
        current.push({
            date : Date.now(),
            msg : msg,
            params : aparms
        });
        this.settings.setProp('sync_autoSync_messages', current);
        this.settings.saveOptions(( res, error) => {
            if(error) {
                console.error("Failed to save autosync message" , error);
            }
        });
    }    
};

SyncService.prototype.clearAutoSyncMessages = function ( callback ) {       
    this.settings.setProp('sync_autoSync_messages', []);
    this.settings.saveOptions(( res, error) => {
        if(error) {
            console.error("Failed to save autosync message" , error);
        }
        if(callback) {
            callback(res, error);
        }
    });
    
};

SyncService.prototype.doAutoSyncDownload = function ( callback ) {       
    this.fetch((r,e)=> {
        if(e)  {
            this.addAutoSyncMessage('sync_autosync_download_failed_cause_of_error', e);  
            console.log("AutoSync failed!" , e);
            callback(null, e);
            return;
        } else if(r) {
            this.addAutoSyncMessage('sync_autosync_download_success');  
            console.log("AutoSynced " , r.length);
            callback(r);
        } else {
            //  ?
            console.log("AutoSync failed! No bookmarks were returned." );
        }
    });  
};

SyncService.prototype.doAutoSyncUpload = function (callback) {       
    console.log("SYNC: Autosync start..."); 
    this.upload( (res, error)=> {
        if(res) {
            this.addAutoSyncMessage('sync_autosync_upload_success');
            console.log("SYNC: ... autosync end # UPLOADED"); 
            callback(res);
        } else if(error) {
            this.addAutoSyncMessage('sync_autosync_upload_failed_cause_of_error', error);                    
            console.error("SYNC: ... autosync end # ERROR"); 
            callback(null, error);
        }
    });
};
SyncService.prototype.checkIfUploadPossibleAndDoIt = function () {       
    console.log("SYNC: Autosync start..."); 
    this.checkIfSyncNeccessary( ( res, error) => {    
        if(error) {            
            this.addAutoSyncMessage('sync_autosync_upload_failed_cause_coudnt_check_remote_for_changes');
            return;
        }
        if(res && res.conflict) {
            this.addAutoSyncMessage('sync_autosync_upload_failed_cause_remote_was_changed');
            this.addAutoSyncMessage(res.conflictReason);
            // Someone changed the bookmarks remotely, we cant simply override it!
            console.log("SYNC: ... autosync end # Someone Changed in background!"); 
        } else if(res.upload) {
            this.doAutoSyncUpload(( res, error) => {
                // 
            });
        } else {
            // Nothin to do
        }
    });
};
SyncService.prototype.clearAutoSync = function () {   
    this.autoSyncTriggered = false;
    this.autoSyncSyncOnNextAlarm = false;
    Utils.get().alarms.clear(this.AUTO_SYNC_ALARM_NAME);
};

SyncService.prototype.onAutoSyncAlarm = function (alarm) {   
     if(this.autoSyncTriggered) {
        if(this.autoSyncSyncOnNextAlarm) {
            this.checkIfUploadPossibleAndDoIt();
            this.clearAutoSync();
        } else {
            console.log("SYNC: starting autosync on next tick"); 
            this.autoSyncSyncOnNextAlarm = true;
            Utils.get().alarms.create(
                this.AUTO_SYNC_ALARM_NAME, {delayInMinutes: 1}
            );
        }
     }
};

/**
 * Handles sync trigger.
 * Sync Trigger is DEBOUNCED. Every trigger resets the timer to sync.
 * Callbacks are directly called.
 * @param {cmd} obj 
 * @param {Function} callback 
 */
SyncService.prototype.handleSyncTrigger = function (obj, callback) {   
    
    if(this.autoSyncTriggered) {        
        console.log("SYNC: prolong autosync"); 
        this.autoSyncSyncOnNextAlarm = false;        
    } else {
        console.log("SYNC: Trigger autosync"); 
        this.autoSyncTriggered = true;
        this.autoSyncSyncOnNextAlarm = true;  
        Utils.get().alarms.create(
            this.AUTO_SYNC_ALARM_NAME, {delayInMinutes: 1}
        );
    }    
    // Call the callback.
    callback(true);
};

  /**
 * Collect the infos
 * @param {Function} callback 
 */
SyncService.prototype.collectSyncInfos = function (callback) {        
    let res= {};
    res['persistencetype'] = this.settings.getProp('persistencetype');
    res['sync_Active'] = this.isSyncActive();
    res['webdav_lastUploadDate'] = this.settings.getProp('webdav_lastUploadDate');
    res['webdav_lastDownloadDate'] = this.settings.getProp('webdav_lastDownloadDate');
    res['webdav_lastSyncDate'] = this.settings.getProp('webdav_lastSyncDate');
    res['webdav_lastSyncCheck'] = this.settings.getProp('webdav_lastSyncCheck');    
    res['autoSyncActive'] = this.isAutoSyncActive();
    res['sync_lastDBChange'] = this.settings.getProp('sync_lastDBChange');        
    res['autoSyncTriggered'] = this.autoSyncTriggered;
    res['autoSyncSyncOnNextAlarm'] =  this.autoSyncSyncOnNextAlarm; 
    res['sync_autoSync_messages']= this.settings.getProp('sync_autoSync_messages');
    if(callback){
        callback( res );
    }
    return res;
};

/**
 * Init sync service
 * @returns {undefined}
 */
SyncService.prototype.init = function () {    
    this.eventBus.addCmdHandler(this);
};

SyncService.prototype._cmdMakesDatabaseChanges = function ( cmd ) {    
    return [
        "db.updateBookmark",
        "db.removeBookmark",
        "db.createBookmark"
    ].indexOf(cmd) >= 0;
};

SyncService.prototype._setLastDBChange = function ( ) {    
    this.settings.setProp('sync_lastDBChange', new Date(Date.now()).toISOString());
    this.settings.saveOptions(( res, error) => {});    
};
/**
 * Handles Sync Events
 * @param {cmd} obj 
 * @param {sender} sender
 * @param {Function} callback 
 */
SyncService.prototype.handle = function (obj, sender, callback) {    
    
    if(!this.isSyncActive()) {
        return false;
    }
    
    if(obj.cmd === "sync.autoSyncTrigger") {
        if(!this.isSyncActive()) {
            callback(true);
        } else {
            console.log("Handling autosync, reason: ", obj.reason);
            this.handleSyncTrigger(obj, callback);
        }
        return true;
    } else if(obj.cmd === "sync.collectSyncInfos") {
        this.collectSyncInfos( callback );
        return true;
    } else if(obj.cmd === "sync.clearAutoSyncMessages") {
        this.clearAutoSyncMessages( callback );
        return true;
    } else if(obj.cmd === "sync.fetch") {
        this.fetch( callback );
        return true;
    } else if(obj.cmd === "sync.upload") {
        this.upload( callback );
        return true;
    } else if(obj.cmd === "sync.check") {
        this.checkIfSyncNeccessary(callback);
        return true;
    } else if(obj.cmd === "background.startup") {
           // Check on first load if there are currently changes
        this.checkOnStart(callback);
        return true;
    } else if(this._cmdMakesDatabaseChanges(obj.cmd)) {
        // Check on first load if there are currently changes
        this._setLastDBChange(callback);
    }            
            
             
    return false;
};