var config = {
    "specs": [
        "spec/background.spec.js",
        "spec/utils.spec.js",
        'spec/EventBus.spec.js',
        'spec/bookmarkpersistence.local.spec.js',
        'spec/storage.spec.js',
        "spec/history.service.spec.js",
        'spec/webdav.spec.js'
    ]
};
export default config;