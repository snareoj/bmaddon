/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import $ from 'jquery'

/**
 * Uses Topsites API from WebExtensions to list the most visisted pages inside the browser.
 * Not supported by FF Android...
 */
export default function Topsites() {
};

Topsites.prototype.checkIsDefaultTopSites = function() {
    let obj = Utils.get().topSites;
     return !! obj;
};
Topsites.prototype.fetchDefaultTopsites = function( callback ) {
     Utils.get().topSites.get((sites, error) => {
        callback(sites, error);
    }); 
};

/**
 *Fetch the history for that day
 * @param {Date} date
 * @param {type} callback
 * @returns {undefined}
 */
Topsites.prototype.fetchHistory = function( date, callback ) {    
    Utils.exec({
        cmd : 'fetch.history',
        date : date.toISOString()
    }, callback);    
};

/**
 *Fetch the history for that day
 * @param {Date} date
 * @param {type} callback
 * @returns {undefined}
 */
Topsites.prototype.fetchHistoryBuckets = function( callback ) {    
    Utils.exec({
        cmd : 'fetch.historyBuckets'
    }, callback);    
};
/**
 * Get current topsites
 * @param {type} callback
 * @returns {undefined}
 */
Topsites.prototype.fetchTopsites = function( callback ) {    
    Utils.exec({
        cmd : 'fetch.topsites'
    }, callback);    
};


