/**
 * Global System JS Config
 * @param global Normally the window
 */
(function (global) {

    // map tells the System loader where to look for things
    var map = {
        'jasmine-html': '/node_modules/jasmine-core/lib/jasmine-core/jasmine-html.js',
        'jasmine-boot': '/node_modules/jasmine-core/lib/jasmine-core/boot.js',
        'jasmine-core': '/node_modules/jasmine-core/lib/jasmine-core/jasmine.js',
        'sinon': '/node_modules/sinon/pkg/sinon.js',
        'sinon-chrome': '/node_modules/sinon-chrome/bundle/sinon-chrome.min.js',
        'plugin-babel': '/node_modules/systemjs-plugin-babel/plugin-babel.js',
        'systemjs-babel-build': '/node_modules/systemjs-plugin-babel/systemjs-babel-browser.js'
    };
        // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
    };

    var meta = {
        'jasmine-html': {
            format: 'global',
            deps : [ 'jasmine-core' ]
        },
        'jasmine-core': {
            exports: 'jasmineRequire',
            format: 'global'
        },
        'jasmine-boot': {
            format: 'global',
            exports: 'jasmine',
            deps : [ 'jasmine-html' ]
        },
        'sinon': {
            format: 'global'
        },
        'sinon-chrome': {
            format: 'global',
            deps : [ 'sinon' ]
        }
    };


    var config = {
        map: map,
        //defaultExtension: 'js',
        packages: packages,
        meta : meta,
        transpiler : 'plugin-babel'
    };
    System.config(config);
    console.log("Set System.config EXTRA");
})(this);