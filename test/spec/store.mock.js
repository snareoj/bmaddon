/* 
  MOck up for local storage
 */

let mock = {
    store : {},
    get :function( obj, callback) {
        for(let k of Object.keys(obj)){
            //this.store[k][l] = obj[k][l];     
            if(this.store[k]) {
                obj[k] = this.store[k];
            } 
            callback(  obj, undefined );
        }
    },
    set :function( obj, callback) {
        for(let k of Object.keys(obj)){
            this.store[k] = obj[k];
            callback( obj, undefined );
        }
    },
    remove :function( obj, callback) {
        delete this.store[obj];
        callback(obj, undefined);
    }
};
export default mock;

