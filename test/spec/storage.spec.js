import Storage from 'lib/Storage.js'
import sinonChrome from 'sinon-chrome'; // from 'sinon-chrome'
import sinon from 'sinon'; // from 'sinon-chrome'

import mock from '/spec/store.mock.js'; 

/* 
 * Spec for Background
 */
describe("Storage Spec", function() {   
    
    window.chrome = sinonChrome.chrome;
    let prefix = 'unittest';
    
    function stubBMPers(pers){
        sinon.stub(pers, "_getStorage").callsFake(function () { return mock; });   
    }
    
  it("crud objects in a prefix at storage", function( done ) {
    
        let storage = new Storage( prefix );
        
        stubBMPers(storage);
        
        storage.updateItem({
            id : 'test'
        }, (res, error) => {            
            expect(res).toBeDefined();
         //   expect(res.id).toBe('test'); 
            expect(mock.store[prefix+':index']).toBeDefined();  
            expect(mock.store[prefix+':index'].items).toBeDefined();  
            expect(mock.store[prefix+':index'].items.length).toBe(1);   
            expect(mock.store[prefix+':index'].items[0]).toBe('test');            
            expect(mock.store[prefix+':test']).toBeDefined();
                         
            storage.updateItem({
                id : 'test',
                'udated' : "true"
            }, (res, error) => {

                expect(res).toBeDefined();
              //  expect(res.id).toBe('test');       
                expect(mock.store[prefix+':index']).toBeDefined();  
                expect(mock.store[prefix+':index'].items).toBeDefined();  
                expect(mock.store[prefix+':index'].items.length).toBe(1);   
                expect(mock.store[prefix+':test']).toBeDefined(); 
                expect(mock.store[prefix+':test']['udated']).toBe("true");

                storage.getItemById('test', (res, error) => {

                    expect(res).toBeDefined();
                    expect(res.id).toBe('test');
                    expect(res.udated).toBe("true");

                    storage.removeItem('test', (res, error) => {
                        expect(res).toBeDefined();
                        expect(mock.store[prefix+':index'].items.length).toBe(0);  
                        expect(mock.store[prefix+':test']).not.toBeDefined();            
                        done();
                    });
                });                
            });
        });
  });
});
