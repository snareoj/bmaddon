/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import Topsites from 'lib/topsites.js'
import BMMenu from 'lib/menu.js'
import * as datepicker from 'bootstrap-datepicker'
import $ from 'jquery'

function showHistory() {    
    
    let startDate = new Date();
    
    // Topsites stuff
    var topsites = new Topsites();
    topsites.fetchHistory( startDate, ( historyBucket, error ) => {        
        renderHistory(startDate, historyBucket, error);
    });    
    
    $('#datepicker').datepicker({
        format: "mm/dd/yyyy",
        todayBtn: "linked"
    });
    var changeDateToDatePicker = function(){
        let selDate = $('#datepicker').datepicker('getFormattedDate');
        let d = new Date();
        if(selDate !== ""){
            d = new Date(Date.parse(selDate));
        }
        topsites.fetchHistory( d, ( historyBucket, error ) => {        
            renderHistory(d, historyBucket, error);
        });  
    };
    $('#datepicker').on('changeDate', changeDateToDatePicker);
    $('#reloadHistory').on('click', changeDateToDatePicker);
    $('#clearHistory').on('click', () => {
          var deleteQuestion = Utils.get().i18n.getMessage(
                "history_delete_confirm"
            );
            let q = ModalHandler.showQuestion(
                    'history_delete_confirm',
                     deleteQuestion
                    );
            q.then( ( res ) => {      
                res.modal.modal('hide');        
                if(res.btn.id === "ok") {
                            //delete them.
                    Utils.exec( { 
                        "cmd" : "history.delete"  },
                         ( res, error) => {
                             if(error) {
                                 ModalHandler.showErr(error);
                                 return;
                             } else {                                 
                                 ModalHandler.notify("history_deleted", 'success');
                                 topsites.fetchHistory( new Date(), ( historyBucket, error ) => {        
                                     renderHistory(new Date(), historyBucket, error);
                                 });    
                             }
                     });   
                } else {
                }          
            });

    });
    // And the menu
    var menu = new BMMenu();
    menu.build();
    
}

function renderHistory(date, historyBucket, error){
    let div = document.getElementById('historyList');        
    if(error)  {
        ModalHandler.showErr(error);
        return;
    }
    
    let headerDate = document.getElementById('headerDate');
    if(headerDate) {
        headerDate.textContent = date.toLocaleDateString();
    }
    if(!historyBucket)  {
        div.textContent = "No history for that date";
        return;
    } else if(!historyBucket.items || historyBucket.items.length === 0)  {
        div.textContent = "No items";            
        return;
    } else {
        div.textContent = "";
    }

    let listElement = document.createElement("dl");
    listElement.setAttribute("class",'dl-horizontal');
    div.appendChild(listElement);

    historyBucket.items.sort( ( a, b) => {
        let dateA = new Date();
        if(a.dateAdded){
            dateA = new Date(Date.parse(a.dateAdded));
        }
        let dateB = new Date();
        if(b.dateAdded){
            dateB = new Date(Date.parse(b.dateAdded));
        }
        return dateB.getTime() - dateA.getTime();
    });

    function listener ( e ) {
        /* Utils.exec({
            'cmd' : "open.url",
            'url'  : e.target.getAttribute('data-url')
        }); */
        window.location = e.target.getAttribute('data-url');
    }

    for (var i = 0, max = historyBucket.items.length; i < max; i++) {
            let site =  historyBucket.items[i];

            let dateEl = document.createElement("dt");                       
            let date = new Date();
            if(site.dateAdded){
                date = new Date(Date.parse(site.dateAdded));
            }
            date = date.toLocaleTimeString();
            dateEl.textContent = date;
            listElement.appendChild(dateEl);
            let urlEl = document.createElement("dd");  
            urlEl.innerText = site.url;
            urlEl.className = "historyItem";
            urlEl.setAttribute("data-url", site.url);
            urlEl.setAttribute("tooltiptext", "Open "  + site.url);
            urlEl.onclick = listener;
            listElement.appendChild(urlEl);


    }   
}

function render(parentId, listOf, error, itemR) {
    var div = document.getElementById(parentId);
    if (!listOf.sites.length) {
        div.innerText = 'No sites returned from the topSites API.';
        return;
    } else if(error) {        
        ModalHandler.showErr(error);
        return;
    } else {
        renderList(div, listOf.sites, itemR);
    }
}

function renderList(div, sites, childR) {
    let ul = document.createElement('ul');
    ul.className = 'list-group';
    for (let site of sites) {
        let li = document.createElement('li');
        li.className = 'list-group-item';
        li.innerHTML = childR(site);
        ul.appendChild(li);
    }
    div.appendChild(ul);
}

export default function main(){
    /* document.querySelector("#btnOptions").addEventListener('click', ( ev) => {
        get().runtime.openOptionsPage();
    }); */
    try {
        showHistory();
    } catch ( e ) {
        console.error("Error " , e);        
        ModalHandler.showEx(e);
    }
}; 