/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import * as hammerjs from 'hammerjs'
import BMMenu from 'lib/menu.js'
import BookmarkTemplate from 'lib/Template_Bookmark.js'
import ModalHandler from 'lib/ModalHandler.js'
import naturalSort from 'naturalSort'

/* 
 * Edit Bookmarks
 */
function addMeta(template) {
    let bm = template.bookmark;
    if(bm.info && bm.info.meta) {
        for(var metaName in bm.info.meta){
            var meta = bm.info.meta[metaName];
            var html = "<h4>" + meta["name"]+"</h4>";
                    
           html += `<table  class="table metaProperties ${metaName}">

 <thead>
  <tr>
     <th class="metaPropertyName" >Property Name</th>
     <th class="metaPropertyValue">Property Value</th>
  </tr>
 </thead>

`;

            for(var prop in meta.props) {
                html += '<tr><td>' + prop + '</td><td>' + meta.props[prop] + '</td>';
            }

            html = html + '</table>';
             $('#metaData').append(html);
        }
    }
};

function crudTag(bm, val, what) {
    Utils.pExec({ 
        cmd : "db.updateBookmark", 
        obj : bm
    }).then( () => {
        ModalHandler.notify("bookmark_edited", 'success');
        Utils.pExec({ 
            cmd : what, 
            tag : val
        }).then( () => { 
            
            Utils.pExec({
                cmd : "sync.autoSyncTrigger",
                reason : 'user_edited_bookmark_in_backend'
            });
           
            loadExistingTags();
        }).catch(e  => ModalHandler.showErr(e));            
    }).catch(e  => ModalHandler.showErr(e)); 
}

function addTag(bm, val){
    if(val && val.length > 0) {
        if(!bm.tags) {
            bm.tags = [];
        }
        var indx = bm.tags.indexOf(val);
        if(indx >= 0) {
            // already added
            return;
        }
        bm.tags.push(val);
        crudTag(bm, val, "tags.addTag");   
    }    
}

function removeTag(bm, tagName){
    try{        
        if(tagName && tagName.length > 0) {
            if(!bm.tags) {
               return;
            }
            var indx = bm.tags.indexOf(tagName);
            bm.tags.splice(indx, 1);
           crudTag(bm, tagName, "tags.removeTag");   
        }   
    } catch( e ) {
        console.error(e);
    }
}    
function renderTags(containerPath, list, removeCallback){
    if(!list) {
        return;
    }
    list.sort();
    $(containerPath).empty();
    for (var i = 0, max = list.length; i < max; i++) {       
        var content = 
               $(`<span class="tag label label-success" data-tagName="${list[i]}">
                 ${list[i]}
        <span class="glyphicon glyphicon-remove align-right" aria-hidden="true"></span>
        </span>`);
        $(containerPath).append(content);
    }
}
function loadExistingTags(){        
     Utils.exec({ cmd : "tags.load" }, ( res , error) => {
        $('#existingTags').empty();
        for(let key of _sortedArray(res)) {            
            $('#existingTags').append(`<span class="tag label label-success" data-tag="${key}">${key} (${res[key]})</span>`);
        }
    });
}
function addTagEditPanel(bTemplate){
    var panel = `
            <div id="bookmarkTags" class="tagList"></div>
             <div class="input-group tagInput">
                <input id="addTagName" type="text" class="form-control" placeholder="tagname">
                <span class="input-group-btn">
                  <button id="addTagBtn" class="btn btn-default" type="button">+</button>
                </span>
             </div><!-- /input-group -->    
             <p>Existing Tags:</p>
            <div id="existingTags" class="tagList"></div>
            <div style="clear:both;"></div>
    `;
    bTemplate.tmpl.find('.tags').empty().append(panel);    
    function _removeTag(val){        
        removeTag(bTemplate.bookmark, val);        
        bTemplate.tmpl.find('[data-tagName='+val+']').remove();
    } 
    var _addTag = () => {
        var val = $('#addTagName').val().trim();
        addTag(bTemplate.bookmark, val);
        $('#addTagName').val('');
        renderTags('#bookmarkTags', bTemplate.bookmark.tags, _removeTag);        
    };
    bTemplate.tmpl.find('#bookmarkTags').on('click', ( e )=> {        
       var val = $(e.target).attr('data-tagName');
       _removeTag(val);
    });
    bTemplate.tmpl.find('#addTagName').keypress(function (e) {
        if (e.which === 13) { // ENTER
            _addTag();
        }
    });
    bTemplate.tmpl.find('#existingTags').click(function (e) {
        if(e.target.getAttribute("data-tag")) {
            $('#addTagName').val(e.target.getAttribute("data-tag"));
        }
    });
    bTemplate.tmpl.find('#addTagBtn').on('click', _addTag);
    renderTags('#bookmarkTags', bTemplate.bookmark.tags, _removeTag);
    loadExistingTags();
};

function _sortedArray( tags ){
    naturalSort.insensitive = true;
    let keys = Object.keys(tags).sort(naturalSort);    
    return keys;
}

/**
 * Initiates Hammer Listeners
 * @param {object} editedBookmark Edited Bookmark 
 */
 function _initHammer(editedBookmark) {          
    
    var myElement = document.getElementsByClassName("bookmark-item")[0];
    
    if(!myElement){
        console.log("Didnt find the hammer");
        return;
    }
    // We create a manager object, which is the same as Hammer(), but without the presetted recognizers. 
    var mc = new hammerjs.default.Manager(myElement, {
	/*recognizers: [
		// RecognizerClass, [options], [recognizeWith, ...], [requireFailure, ...]
		[Hammer.Rotate],
                [Hammer.Tap, { event: 'doubletap', taps: 2 }],
                [Hammer.Pinch, { enable: false }, ['rotate']],
		[Hammer.Swipe,{ direction: Hammer.DIRECTION_HORIZONTAL }],
	] */
    });

    // Tap recognizer with minimal 2 taps
    mc.add( new hammerjs.default.Tap({ event: 'doubletap', taps: 2 }) );
    mc.add( new hammerjs.default.Swipe({ event: 'swipe' }) );
    
    mc.on("swiperight", (ev) => {
         // Open ID
        Utils.exec( {
            "cmd" : "nav.listBookmarks",
            'hash' : editedBookmark.id
        }, (res, error) => {                                        
            if(error) {
                ModalHandler.showErr(error);
                return;
            } else {
                console.log("Listed");
            }
        });
    });
    mc.on("swipeleft", (ev) => {
        
    });
};

function main(){        
    Utils.get().runtime.getBackgroundPage(( bgWindow ) => {
        let searchParams = new URLSearchParams(window.location.search);
        if(!searchParams.has("id")){
            console.warn("No bookmark id for editing");
            $('#editBookmark').append(Utils.i18nMsg("edit_nobookmarkid", "No Bookmark id given."));               
            return;
        }
        var id = searchParams.get("id");
        Utils.exec( { "cmd" : "db.getBookmarkById", "id" : id }, ( bm, error) => {        
            var editedBookmark = bm;
            if(!editedBookmark){
                console.warn("No bookmark for editing", error);
                $('#editBookmark').append(Utils.i18nMsg("edit_nobookmark", "No Bookmark found for this id.", [ id ]));
                return;
            }   
            var bTempl = new BookmarkTemplate(editedBookmark);
            $('#editBookmark').append(bTempl.asHTML());

            try {
                addTagEditPanel(bTempl);
                addMeta(bTempl);         
                _initHammer(editedBookmark);   
            } catch ( e ) {
                console.error(e);
            }

        });
        
        
    });
    
    var menu = new BMMenu();
    menu.build();
}

export default main; 