/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * Listens to DB function calls and collects the tags.
 */
/* global Utils */
"use strict";

import Utils from 'lib/Utils.js';
import Storage from 'lib/Storage.js';
import naturalSort from 'naturalSort'

export default function TagsService() {
    this.autoPersist = true;
    this.storage = new Storage('tags');
    this.tags = {};    
    
}

/**
 * Returns the current used Storage.
 * See https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage/StorageArea/set
 * @private
 * @returns {unresolved}
 */
TagsService.prototype.fetch = function( callback ){
    if (callback) {
        callback(this.tags);
    } else {
        console.warn("No callback");
    }
};

// See    PersistenceCmdHandler.prototype._wrapCallback = function (cmdName, callback) {
TagsService.prototype.persist = function(callback) {  
    this.storage.updateItem({
        id : 'tagList',
        tagList : this.tags
    }, (res, error) => {
        console.log("Set into storage", res);
        if (callback) {
            callback(res, error);
        } else {
            console.warn("No callback");
        }
    });
};

// Adds tags
TagsService.prototype.addTags = function(tagList, callback) {  
    if(!tagList || !typeof tagList === "array")  {
        return;
    }
    for (var i = 0, max = tagList.length; i < max; i++) {
        var tag = tagList[i];
        if(typeof this.tags[tag] === 'number' ){
            this.tags[tag] ++;
        } else {
            this.tags[tag] = 1;
        }
    }
            
    if(this.autoPersist) {
        this.persist(callback);
    }
};

// Removes tags
TagsService.prototype.removeTags = function(tagList, callback) {  
    if(!tagList || !typeof tagList === "array")  {
        return;
    }
    for (var i = 0, max = tagList.length; i < max; i++) {
        var tag = tagList[i];
        if(typeof this.tags[tag] === 'number' ){
            this.tags[tag] = --this.tags[tag];
            if(this.tags[tag] <= 0) {
                delete this.tags[tag];  
            }
        } else {
            // Doesnt exist at all...
        }
    }
    if(this.autoPersist) {
        this.persist(callback);
    }
};

TagsService.prototype.load = function( callback ) {   
    this.storage.getItemById('tagList', ( item, error ) => {
        if (callback) {
            if(item) {                        
                console.log("Returning tags", item['tagList']);
                this.tags = item['tagList'];
            } else {
                this.tags = {};
            }
            callback(this.tags, error);
        } else {
            console.warn("No callback");
        }
    });
};

/**
 * Handles Sync Events
 * @param {object} obj The command
 * @param {sender} sender
 * @param {Function} callback 
 */
TagsService.prototype.handle = function (obj, sender, callback) {
    if(obj.cmd === "tags.fetch") {
        this.fetch( callback );
        return true;
    } else if(obj.cmd === "tags.load") {
        this.load( callback );
        return true;
    } else if(obj.cmd === "tags.setAndSave") {
        this.tags = obj.tags;
        this.persist(callback);
        return true;
    } else if(obj.cmd === "tags.addTag") {
        this.addTags([ obj.tag ], callback);
        return true;
    } else if(obj.cmd === "tags.addTags") {
        this.addTags(obj.tags, callback);
        return true;
    } else if(obj.cmd === "tags.removeTag") {
        this.removeTags([ obj.tag ], callback);
        return true;
    } else if(obj.cmd === "tags.removeTags") {
        this.removeTags( obj.tags, callback);
        return true;
    } else if(obj.cmd  === 'res.db.createBookmark' && obj.result) {
        this.addTags(obj.result.tags);
        return true;
    } else if(obj.cmd  === 'res.db.createAndPersistBookmark' && obj.result) {
        this.addTags(obj.result.tags);
        return true;
    } else if(obj.cmd  === 'res.db.updateBookmark' && obj.result) {
       // this.addTags(result.tags);
        return true;
    } else if(obj.cmd  === 'res.db.removeBookmark' && obj.result) {
        this.removeTags(obj.result.tags);
        return true;
    } else if ( obj.cmd === 'main.clearStorage') {        
        this.storage.clearItems((res, error ) => {
           callback ( 'cleared tags', error);  
        });
        return true;
    }
    return false;
};



