/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global Utils */

import Storage from 'lib/Storage.js';
import Utils from 'lib/Utils.js';
/* 
 * Bookmark Persistence
 * 
 * Simply stores all bookmarks in storage.local
 */

export default function BookmarkPersistenceLocalStorage() {
    this.store =  new Storage("bookmarks");
}


/**
 * Create a bookmark out of the pageInfo.
 * @param {type} pageInfo
 * @returns {undefined} bookmark
 */
BookmarkPersistenceLocalStorage.prototype.createBookmark = function (pageInfo) {
    // TODO remove ID from this
    var id = Utils.urlToBookmarkId( pageInfo.url );    
    var bookmark = {
        id : id,
        dateAdded : new Date().toISOString(),
        count : 0,
        info : pageInfo,
        tags : null
    };
    return bookmark;
};


/**
 * Returns the current used Storage.
 * See https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage/StorageArea/set
 * @private
 * @returns {unresolved}
 */
BookmarkPersistenceLocalStorage.prototype._getStorage = function(){
    return this.store;
};

/**
 * Get A bookmark from Storage by URL
 * @param {string} url URL
 * @param {type} callback
 * @returns {undefined}
 */
BookmarkPersistenceLocalStorage.prototype.getBookmarkByURL = function ( url, callback) {
    this.getBookmarkById(Utils.urlToBookmarkId( url ), callback);
};

/**
 * Get A bookmark from Storage by Bookmark ID
 * @param {string} id
 * @param {type} callback
 * @returns {undefined}
 */
BookmarkPersistenceLocalStorage.prototype.getBookmarkById = function ( id , callback) {
    this._getStorage().getItemById(id, (res, error) => {
        console.log("Got from storage", res);
        if (callback) {
            callback(res, error);
        } else {
            console.warn("No callback");
            throw new Error("No callback for getBookmarkById");
        }
    });
};
/**
 *  Set a bookmark, uses "bookmark:URL" as KEY
 * @param {type} pageInfo
 * @param {type} callback
 * @returns {undefined}
 */
BookmarkPersistenceLocalStorage.prototype.createAndPersistBookmark = function (pageInfo, callback) {
    var bookmark = this.createBookmark(pageInfo);
    this.updateBookmark(bookmark, callback);
};

/**
 *  Set a bookmark, uses "bookmark:URL" as KEY
 * @param {type} bookmark
 * @param {type} callback
 * @returns {undefined}
 */
BookmarkPersistenceLocalStorage.prototype.updateBookmark = function (bookmark, callback) {
    if(!bookmark.id) {
        bookmark.id = Utils.urlToBookmarkId(bookmark.url);
        console.log("Generated ID for new bookmark: ", bookmark);
    }     
    this._getStorage().updateItem(bookmark, (res, error) => {
      //  console.log("Set into storage", bookmark, res);
        if (callback) {
            callback(res, error);
        } else {
            console.warn("No callback");
        }
    });
};

/**
 *  Removes a bookmark, uses "bookmark:URL" as KEY
 * @param {String} id
 * @param {Function} callback
 * @returns {undefined}
 */
BookmarkPersistenceLocalStorage.prototype.removeBookmark = function ( id , callback) { 
    console.log("Removing ", id);
    this._getStorage().removeItem( id , ( res, error ) => {
        console.log("Removed from storage", id , res);
        if (callback) {
            callback(id, error);
        } else {
            console.warn("No callback");
        }
    });
};


/** * 
 * Lists all Bookmarks
 * Gives the callback an array of all PageInfos.
 * 
 * @param {type} callback
 * @returns {undefined}
 */
BookmarkPersistenceLocalStorage.prototype.listBookmarks = function (callback) {
    this._getStorage().listItems(( allItems, error ) => {
        if (callback) {            
            var items = [];
            for(var key in allItems ){
                var obj = allItems[key];
                items.push(obj);
            }
            console.log("Read " + items.length+ " Bookmarks");
            callback(items, error);
        } else {
            console.warn("No callback");
        }
    });
};

function hasAllTags(tagList, neededTags) {
    if(!tagList || tagList.length === 0) { // or NO TAG
        let tagsSearched = neededTags && neededTags.length > 0;
        return !tagsSearched;
    }
    if(neededTags && neededTags.length === 0 
                && tagList && tagList.length > 0) {
        return false; // No tags searched but object has tags.
    }
    for (var i = 0; i < neededTags.length; i++) {
        if (tagList.indexOf(neededTags[i]) <= -1) {
            return false;
        }
    }
    return true;
};

/** * 
 * Lists all Bookmarks
 * Gives the callback an array of all PageInfos.
 * @param tagList List of needed Tags
 * @param {type} callback
 * @returns {undefined}
 */
BookmarkPersistenceLocalStorage.prototype.searchByTags = function (tagList, callback) {
     this.listBookmarks(( allItems, error ) => {
        if (callback) {            
            var items = [];
            for(let obj of allItems ){
                if(hasAllTags(obj.tags, tagList)) {
                   items.push(obj);
                }
            }
            console.log("Search By Tags " + items.length+ " Bookmarks");
            callback(items, error);
        } else {
            console.warn("No callback");
        }
    });
};

/** * 
 * Lists all Bookmarks
 * Gives the callback an array of all PageInfos.
 * 
 * @param {type} callback
 * @returns {undefined}
 */
BookmarkPersistenceLocalStorage.prototype.clearBookmarks = function (callback) {
    this._getStorage().clearItems((res, error) => {
        if(error) {
            console.error("Error while removing bookmarks", error);
        } else {
            console.error("Deleted bookmarks");
        }
        if (callback) {                   
            callback(true, error);
        } else {
            console.warn("No callback");
        }
    });
};

/** 
 * Rebuilds index
 * @param {function} callback
 */
BookmarkPersistenceLocalStorage.prototype.rebuildIndex = function (callback) {
    this._getStorage().index.rebuildIndex( callback );
};