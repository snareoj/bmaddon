/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import $ from 'jquery'
import BMMenu from 'lib/menu.js'
import * as sync from 'lib/WebDavNextCloudSync.js';
import TagsService from 'background/TagsService.js';

/**
 * Options panel
 */
var OptionsPage = {
    
    settings : {},
    
    toHumanDate : function( s ){
        try {
            return new Date(Date.parse(s)).toLocaleString();
        } catch ( e ) {
            return '-';
        }
    },
    load : function(){
        Utils.exec({ "cmd" : "settings.load"}, (res, error) => {
           try {
               if(error) {
                   console.log(error);
               }
               $("[data-setting='bookmarkList.listOfStartPage']").val(res['bookmarkList.listOfStartPage']);
               $("[name='persistencetype'][value='" + res['persistencetype'] + "']").prop('checked', true);
               $("[data-setting='webdav_lastSyncDate']").val(res['webdav_lastSyncDate']);
               $("[data-setting='webdav_url']").val(res['webdav_url']);
               $("[data-setting='webdav_user']").val(res['webdav_user']);
               $("[data-setting='webdav_password']").val(res['webdav_password']);    
               $("[data-setting='webdav_filename']").val(res['webdav_filename']);
               $("[data-setting='webdav_lastUploadDate']").text(this.toHumanDate(res['webdav_lastUploadDate']));    
               $("[data-setting='webdav_lastDownloadDate']").text(this.toHumanDate(res['webdav_lastDownloadDate']));
               $("[data-setting='webdav_lastSyncDate']").text(this.toHumanDate(res['webdav_lastSyncDate']));
               $("[data-setting='webdav_lastSyncCheck']").text(this.toHumanDate(res['webdav_lastSyncCheck']));
               let webdav_autosync = !!res['webdav_autosync'];
               if(webdav_autosync){ 
                    $("[data-setting='webdav_autosync']").attr('checked', 'checked');
               }
               this.settings = res;
               
           } catch (e) {
               console.error(e);
           }
       });        
    },
    
    showResult : function(res, error, htmlId, successText) {
            var msg = "";
            if (res && !error) {
                if (res.status === 200) {
                    successText += " (Http-Ok)";
                } else if (res.status === 201) {
                    successText += " (File created)";
                } else if (res.status === 204) {
                    successText += " (File updated)";
                } else if (res.status === 207) {
                    successText += " (Multi OK)";
                }
                msg = '<span class="label label-success">' + successText + '</span>';
            } else {
                let eMsg = error.message;
                if(eMsg && typeof eMsg === 'string') {
                    eMsg = JSON.parse(eMsg);
                    if(eMsg.status === 0) {
                        eMsg.statusText = "Browser denied crossside scripting.";
                    }
                    
                    msg = '<span class="label label-danger">Failed';
                    msg += ": "+ eMsg.status + "/" + eMsg.statusText;
                    msg += "</span>";  
                    ModalHandler.showErr(eMsg);
                } else {
                    msg = '<span class="label label-danger">Failed';
                    if (error.message) {
                        msg += ": " + error.message;
                    }
                    msg += "</span>";  
                    ModalHandler.showErr(error);
                }
            }
            document.querySelector(htmlId).innerHTML = msg;
        },
        
    saveSettings :function() {
        //var datee =  new Date($("[data-setting='lastChangeDate']").val() );
        // settings.set( 'lastChangeDate' , datee);
        var settings = this.settings;
        settings['persistencetype'] = $('input[name="persistencetype"]:checked').val();
        settings['bookmarkList.listOfStartPage'] = $('[data-setting="bookmarkList.listOfStartPage"]').val();
        settings['webdav_url'] = $("[data-setting='webdav_url']").val();
        settings['webdav_user'] = $("[data-setting='webdav_user']").val();
        settings['webdav_password'] = $("[data-setting='webdav_password']").val();
        settings['webdav_filename'] = $("[data-setting='webdav_filename']").val();
        settings['webdav_autosync'] = !!$("[data-setting='webdav_autosync']").prop('checked');
        Utils.exec({ "cmd" : "settings.save", 'obj' : settings }, (res, error) => {
            // TODO
            if(error){            
                ModalHandler.showErr(error);                
            } else {                
                ModalHandler.notify('options_savedSettings', "success" );
                this.setUnDirty();
            }
        });
    },
    
    
    testWebdav : function(){      
        document.querySelector("#testResult").innerHTML = "<p><bold>Test Connection...</bold></p>";  
        let settings = {};
        settings['webdav_url'] = $("[data-setting='webdav_url']").val();
        settings['webdav_user'] = $("[data-setting='webdav_user']").val();
        settings['webdav_password'] = $("[data-setting='webdav_password']").val();
        
        var nxtClient = new sync.WebDavNextCloudSync(
                settings['webdav_url'],
                settings['webdav_user'],
                settings['webdav_password']);

        nxtClient.listRootFolder((data, error) => {
            this.showResult(data, error, "#testResult", "Success");
        });
    },
    
    buildTagList : function( cb ){
        Utils.exec({ cmd : "db.listBookmarks" }, ( bookmarks , error) => {
            if(error) {
                ModalHandler.showErr(error);
                return;
            }
            var tempTagService = new TagsService();
            tempTagService.autoPersist = false;
            for (var i = 0, max = bookmarks.length; i < max; i++) {
                let bm = bookmarks[i];
                tempTagService.addTags(bm.tags);
            }
            Utils.exec({ cmd : "tags.setAndSave", tags :  tempTagService.tags },
             (res , error) => {
                if(error) {
                    ModalHandler.showErr(error);
                    return;
                } else {
                    console.log("Persisted ", tempTagService.tags);
                    if(cb) {
                        cb ( tempTagService.tags );
                    }
                }
             });
        });
    },
    
    setDirty : function ( ev ){        
        if(!this.dirty) {
            this.dirty = [];
        }     
        this.dirty.push(ev);
        $("#checkSync").attr('disabled', 'disabled');
        $("#downloadBookmarks").attr('disabled', 'disabled');
        $("#uploadBookmarks").attr('disabled', 'disabled');    
        $("#saveBtn").removeAttr('disabled');        
    },
    setUnDirty : function ( ){        
        this.dirty = [];
        $("#checkSync").removeAttr('disabled');
        $("#downloadBookmarks").removeAttr('disabled');
        $("#uploadBookmarks").removeAttr('disabled');
        $("#saveBtn").attr('disabled', 'disabled');       
        
    },
    init : function(){     
        this.load();
        this.setUnDirty();
       $("input").on("change", (ev)=> {
            console.log("Someone changed sth, setting dirty: ", ev);
            this.setDirty(ev);
        });
        
        document.querySelector("#optionsForm").addEventListener("submit", (ev)=> {
            ev.preventDefault();
            this.saveSettings();
        });

        document.querySelector("#testWebdavClient").addEventListener("click", (ev) => {
            ev.preventDefault();
            this.testWebdav();
        });
        
        document.querySelector("#buildTagList").addEventListener("click", (ev) => {
            ev.preventDefault();
            this.buildTagList( ( tags ) => {
                ModalHandler.notify('options_buildTagList', 'success');
            });
        });
        document.querySelector("#rebuildHistoryIndex").addEventListener("click", (ev) => {
            ev.preventDefault();
            Utils.exec({ cmd : "history.rebuildHistoryIndex" }, ( bookmarks , error) => {
                if(error) {
                    ModalHandler.showErr(error);
                    return;
                } else {
                    ModalHandler.notify('options_rebuildHistoryIndex', "success" );
                }
            });
        });
        document.querySelector("#rebuildTopsitesIndex").addEventListener("click", (ev) => {
            ev.preventDefault();
            Utils.exec({ cmd : "history.rebuildTopsitesIndex" }, ( bookmarks , error) => {
                if(error) {
                    ModalHandler.showErr(error);
                    return;
                } else {
                    ModalHandler.notify('options_rebuildTopsitesIndex', "success" );
                }
            });
        });
        
        document.querySelector("#rebuildBookmarkIndex").addEventListener("click", (ev) => {
            ev.preventDefault();
             Utils.exec({ cmd : "db.rebuildBookmarkIndex" }, ( bookmarks , error) => {
                if(error) {
                    ModalHandler.showErr(error);
                    return;
                }else {
                    ModalHandler.notify('options_rebuildBookmarkIndex', "success" );
                }
            });
        });
        
        document.querySelector("#clearStorage").addEventListener("click", (ev) => {
            
            ev.preventDefault();
            var deleteQuestion = Utils.get().i18n.getMessage(
                "options_delete_storage_confirm"
            );
            let q = ModalHandler.showQuestion(
                    'options_delete_storage_confirm',
                     deleteQuestion
                    );
            q.then( ( res ) => {      
                res.modal.modal('hide');        
                if(res.btn.id === "ok") {
                            //delete them.
                    Utils.exec({ cmd : "main.clearStorage" }, ( res , error) => {
                        if(error) {
                            ModalHandler.showErr(error);
                            console.error(res, error);
                            return;                       
                        } else {                        
                            console.log('cleared storage', res);
                            ModalHandler.notify('options_clearStorage', "success" );                
                        }
                    });     
                } else {
                }          
            });
        });
    }
};

export default function main() {
    // Load
    try {
        OptionsPage.init();
        var menu = new BMMenu();
        menu.build();
    } catch (e) {
        console.error(e);
        ModalHandler.showEx(e);
    }
};