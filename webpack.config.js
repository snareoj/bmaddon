var webpack = require('webpack');
//var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
//var ChunkManifestPlugin = require("chunk-manifest-webpack-plugin");
//var WebpackChunkHash = require("webpack-chunk-hash");
var ManifestPlugin = require('webpack-manifest-plugin');

// webpack.config.js 
var path = require('path');
var pkg     = require('./package.json');  //loads npm config file
var dist = path.resolve(__dirname, 'build');
var base = path.resolve(__dirname, 'addon'); //'./build';
var vendor = path.resolve(base, 'vendor'); //'./build';
var nodeModules = path.resolve(__dirname, 'node_modules'); //'./build';
var dev = true;
console.log("Base: " + base);
console.log("dist: " + dist);

var addonModules = {
    'background/background': {
        entry: 'background/background.main', 
        filename : 'background/background.html',
        template : 'background/background.ejs'},
    'pages/bookmark_edit': {
        entry: 'pages/bookmark_edit.main', 
        filename : 'pages/bookmark_edit.html',
        template : 'pages/bookmark_edit.ejs'},
    'pages/home': {
        entry: 'pages/home.main', 
        filename : 'pages/home.html',
        template : 'pages/home.ejs'},
    'pages/sync': {
        entry: 'pages/sync.main', 
        filename : 'pages/sync.html',
        template : 'pages/sync.ejs'},
    'pages/bookmark_list': {
        entry: 'pages/bookmark_list.main', 
        filename : 'pages/bookmark_list.html',
        template : 'pages/bookmark_list.ejs'},
    'pages/file_import': {
        entry: 'pages/file_import.main', 
        filename : 'pages/file_import.html',
        template : 'pages/file_import.ejs'},
    'pages/history': {
        entry: 'pages/history.main', 
        filename : 'pages/history.html',
        template : 'pages/history.ejs'},
    'pages/json_imporexport': {
        entry: 'pages/json_imporexport.main', 
        filename : 'pages/json_imporexport.html',
        template : 'pages/json_imporexport.ejs'},
    'pages/local_importer': {
        entry: 'pages/local_importer.main', 
        filename : 'pages/local_importer.html',
        template : 'pages/local_importer.ejs'},
    'pages/options': {
        entry: 'pages/options.main',
        filename : 'pages/options.html',
        template : 'pages/options.ejs'},
    'pages/topsites': {
        entry: 'pages/topsites.main', 
        filename : 'pages/topsites.html',
        template : 'pages/topsites.ejs'},
    'pages/about': {
        entry: 'pages/about.main', 
        filename : 'pages/about.html',
        template : 'pages/about.ejs'}
};

module.exports = {
    context: path.resolve(__dirname, base),
    entry: {
        'node' : Object.keys(pkg.dependencies),
        'content_scripts/ContentScipt.main': 'content_scripts/ContentScipt.main',
        
        // REST is included "dynamically" based on the object above
    },
    output: {
     //   filename: '[name].bundle.[chunkhash].js',
        filename: '[name].bundle.js',
        // chunkFilename: "[name].chunk.[chunkhash].js",
        path: dist,
        publicPath : '/'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            'humane' : 'vendor/humane/humane.min.js',            
            'naturalSort' : 'javascript-natural-sort'
            /*
             * 
            'hammerjs': 'vendor/hammerjs/hammer.min.js',
            'datepicker': 'vendor/datepicker/js/bootstrap-datepicker.min.js',
            'material' : 'vendor/node_modules/bootstrap-material-design/dist/js/material.min.js',
            'ripples' : 'vendor/node_modules/bootstrap-material-design/dist/js/ripples.min.js',
            'humane' : 'vendor/humane/humane.min.js',            
             */
        },
        modules: [
            base,
            vendor,
            nodeModules
        ]
    },
    module: {
        rules: [
            // USe Babel to Transipile JavaScript
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
            // CSS STUFF
            /*, {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {url: false} // If true => The @_MSGID_ settings in BACkground Sprite do not work
                    }
                ]
              
            } */
                      /*use: ExtractTextPlugin.extract({
                         fallback: "style-loader",
                         use: "css-loader"
                         })*/
            , {
                test: /\.html$/,
                exclude: /\.css$/,
                use: {loader: 'html-loader'}
            }
        ]
    }

};

function isExternal(module) {
  var context = module.context;

  if (typeof context !== 'string') {
    return false;
  }
  console.log("External: " + context+ " = " + (context.indexOf('node_modules') !== -1));
  return context.indexOf('node_modules') !== -1;
}
/*
    new webpack.optimize.CommonsChunkPlugin({
        name: 'pages',
        filename: '[name][hash].min.js',
        chunks : Object.keys(addonModules),
        minChunks : function(module, count) {
            return !isExternal(module) && count >= 2; // adjustable
        }
    }),


 */
module.exports.plugins = [
    
    // JQUERY Bullshit
    new webpack.ProvidePlugin({
       $: "jquery",
       jQuery: "jquery"
   }),
    
    /* new ExtractTextPlugin("[name].css"), */

    // Extract the NODE Modules / Vendor stuff to a separate JavaScript File
    new webpack.optimize.CommonsChunkPlugin({
        name: 'node',
        filename: 'vendor/[name].min.js',
        minChunks : function(module, count) {
            return isExternal(module); // adjustable
        }
    }),

    // Ninify it
    /*new webpack.optimize.UglifyJsPlugin({
        compress: {
            warnings: false //,
                    // drop_console: false
        }
    }), */
    new ManifestPlugin({
        fileName: 'gen-js-names.json'
      })
];

Object.keys(addonModules).map(function (id) {
    module.exports.entry[id] = addonModules[id].entry;
    // TODO Check if the HTML exists and only add it if the html DOES exist
    let plgn = new HtmlWebpackPlugin({
       // title: 'Code Splitting',
        chunks: [ 'node', id],
        filename: addonModules[id].filename,
        template: addonModules[id].template,
        inject: "head"
    });
    module.exports.plugins.push(plgn);
});

if (dev && false) {
    /*new webpack.SourceMapDevToolPlugin({
     filename: '[name].js.map',
     exclude: ['vendor.js']
     }) */
    module.exports.plugins.push(
            new webpack.SourceMapDevToolPlugin({
                filename: '[name].js.map',
                exclude: ['vendor.js']
            })
            );
    module.exports.devtool = 'cheap-module-source-map';
}
