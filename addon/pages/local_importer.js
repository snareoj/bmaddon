/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import v from 'lib/ModalHandler.js'
import $ from 'jquery'
import BookmarkTemplate from 'lib/Template_Bookmark.js'
import BookmarkListTemplate from 'lib/Template_BookmarkList.js'
import BMMenu from 'lib/menu.js'
import BookmarkPersistenceLocalStorage from 'background/BookmarkPersistenceLocalStorage.js';
import PageInfo from 'lib/PageInfo.js';
import ModalHandler from 'lib/ModalHandler.js'

// local_importer.js
"use strict";

/* 
 * Imports the current bookmarks into this addon
 */
var LocalImporter = function () {
    this.folderNameStack = [];
    this.newBookmarks = [];
    this.newAvailableBookmarks = [];
    this.counter = 0;
    this.persistence = new BookmarkPersistenceLocalStorage();
};


LocalImporter.prototype.checkIfAlreadyBookmark = function (bm, callback) {

    Utils.exec({cmd: "db.getBookmark", "obj": bm.info.url}, (existing) => {
        if (existing) {
            callback(true);
        } else {
            callback(false);
        }
    });
};

LocalImporter.prototype.request = function (bm, callback) {
    try {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                PageInfo.collectPageInfoData(bm.info, this.responseText);
                callback(bm, null);
            } else if (this.readyState === 4) {
                var statusText = this.statusText;
                if (!statusText) {
                    statusText = "Unknown Error";
                }
                //  callback(undefined, { status : this.status, statusText: statusText, error : null });
            }
        };
        xhttp.onerror = function (e) {
            var statusText = this.statusText;
            if (!statusText) {
                statusText = e.message;
            }
            if (!statusText) {
                statusText = "Unknown Error";
            }
            var msg = {status: this.status, statusText: statusText, error: e};
            console.log("Cant request ", msg);
            callback(undefined, msg);
        };
        xhttp.open("GET", bm.info.url, true);
        xhttp.send();
    } catch (e) {
        console.error(e);
        var msg = {status: 0, statusText: "Exception while request(" + e + ")", error: e};
        callback(null, msg);
    }
};

LocalImporter.prototype.scan = function (callback) {

    if (Utils.get().bookmarks) {
        // bookmarks available.
        Utils.get().bookmarks.getTree((bookmarkItems, error) => {
            if (error) {                
                callback(null, error);
            } else {
                this.traverse(bookmarkItems[0], 0);
                console.log(this.newBookmarks);
                callback(this.newBookmarks);
            }
        });
    } else {
        console.warn("Bookmarks not supported");
        ModalHandler.showMesg("msg.warn.bookmarksnotsupported", "Bookmarks not supported");
        callback([]);
    }

};
LocalImporter.prototype.createBM = function (bookmarkItem, folderNames) {
    // TODO remove direct dependency
    var bm = this.persistence.createBookmark({
        url: bookmarkItem.url,
        host: PageInfo.getLocation(bookmarkItem.url).hostname, // TODO Replace this with URL()
        title: bookmarkItem.title
    });
    bm.dateAdded = bookmarkItem.dateAdded;
    bm.tags = folderNames;
    return bm;
};

LocalImporter.prototype.traverse = function (bookmarkItem, indent) {
    if (bookmarkItem.url) {
        console.log(makeIndent(indent) + bookmarkItem.url);
        var bookmark = this.createBM(bookmarkItem, this.folderNameStack.slice(0));
        this.newBookmarks.push(bookmark);
        return;
    } else {
        console.log(makeIndent(indent) + "Folder");
        if (bookmarkItem.title !== "") {
            this.folderNameStack.push(bookmarkItem.title);
        }
        indent++;
    }
    if (bookmarkItem.children) {
        for (var child of bookmarkItem.children) {
            this.traverse(child, indent);
        }
    }
    indent--;
    this.folderNameStack.pop();
};

LocalImporter.prototype.import = function (bookmarkItem, callback) {
    // this.persistence.saveBookmark(bookmarkItem, callback);
    Utils.exec(
            {
                cmd: "db.updateBookmark",
                obj: bookmarkItem,
                source : "import"
            },
            callback);
};

LocalImporter.prototype._importAll = function ( currentIndex, listOfBookmarks) {
    if(currentIndex < listOfBookmarks.length) {
        this.import(listOfBookmarks[currentIndex], (res, error)=> {
            console.log("Importing #" + currentIndex+"/" +listOfBookmarks.length);
            this._importAll(currentIndex +1, listOfBookmarks );
        });
    } else {
        ModalHandler.notify("import.allimported", 'success');
    }
};
LocalImporter.prototype.importAll = function (  ) {
    this._importAll(0, this.newBookmarks );
};
    
function makeIndent(indentLength) {
    return ".".repeat(indentLength);
}

function onRejected(error) {
    console.log(`An error: ${error}`);
}

function init() {
    // Load
    var importer = new LocalImporter();

    function updateBookmark(bmTmpl) {
        importer.request(bmTmpl.bookmark, (updated, m) => {
            if (m) {
                bmTmpl.addHint('<p class="bg-danger">Error ' + m.status + ': ' + m.statusText + '</p>');
            } else if (updated) {
                bmTmpl.addHint('<p class="bg-success">Updated</p>');
            }
            importer.checkIfAlreadyBookmark(bmTmpl.bookmark, (hasBookmark) => {
                if (hasBookmark) {
                    bmTmpl.addHint('<p class="bg-danger">Already bookmarked</p>');
                }
                bmTmpl.update();
            });
        });
    }
    ;

    function createTmpl(bookmark) {
        var bmTmpl = new BookmarkTemplate(bookmark);
        var update =  $('<button class="btn btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> update</button>');
        update.on('click', ( e ) => {
              updateBookmark(bmTmpl);
        });
        bmTmpl.addAction( update );

        var importSingle = $('<button class="btn btn-default"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> import</button>');
        importSingle.on('click', (e) => {
            importer.import(bookmark, (res, error) => {
                // Imported
                if(!error) {
                    bmTmpl.addHint('<p class="bg-success">Imported</p>');
                    bmTmpl.update();
                } else {          
                    ModalHandler.showErr(error);
                }
            });
        });
        bmTmpl.addAction(importSingle);            

        return bmTmpl;
    }

    var _renderer = function (listTemplate, bookmark) {
        var tmpl = createTmpl(bookmark);
        return tmpl;
    };

    var listTemplate = new BookmarkListTemplate(function (cb) {
        cb([]);
    }, _renderer);

    listTemplate.viewClassKey = "bookmarkList.listOfImporterPage";        
    listTemplate._prepare();
    // TODO
    //var settings = new Settings();    


    var btnTest = $('<button class="btn btn-default btn-raised" id="btnImport" >Test</button>');
    //btnReload.html(get().i18n.getMessage("btn_reload_text"));
    //btnReload.html(get().i18n.getMessage("btn_reload_text"));
    btnTest.on('click', (ev) => {
        listTemplate.eachItem((item) => {
            updateBookmark(item);
        });
    });
    listTemplate.addAction(btnTest);

    // Import LOCAL Bookmarks
    var btnImport = $('<button class="btn btn-default btn-raised" id="btnImport" >Locals</button>');
    //btnReload.html(get().i18n.getMessage("btn_reload_text"));
    btnImport.on('click', (ev) => {
        // Set the function which lists the bookmarks:
        // SCAN local Bookmarks inside this browser.
        listTemplate._listBookmarksFunction = (callback) => {
            importer.scan((newBookmarks, error) => {
                callback(newBookmarks, error);
            });
        };
        listTemplate.reload();
    });
    listTemplate.addAction(btnImport);

/*
    // Import Bookmarks from NEXTCLOUD
    var btnImport2 = $('<button class="btn btn-default btn-raised" id="btnImport" >Remotes</button>');
    //btnReload.html(get().i18n.getMessage("btn_reload_text"));
    btnImport2.on('click', (ev) => {
        // Set the function which lists the bookmarks:
        // Go and get the Bookmarks from the Remote Module set by User in OPTIONS
        listTemplate._listBookmarksFunction = (callback) => {
            Utils.exec({
                cmd: "sync.fetch"
            }, (res, error) => {
                //showResult(res, error, "#downloadResult", "Downloaded bookmark.json file.");
                if (res && res.data) {
                    var list = JSON.parse(res.data);
                    callback(list);
                } else {
                    console.log("Error ", error);
                    if(error){            
                        ModalHandler.showErr(error);
                    }
                }
            });
        };
        listTemplate.reload();
    });
    listTemplate.addAction(btnImport2); */
    
    var importAll = $('<button class="btn btn-default btn-raised"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> import</button>');
    importAll.on('click', (e) => {
        importer.importAll();
    });
    
    listTemplate.addAction(importAll);

    listTemplate.appendTo($('#importExport'));

    listTemplate.reload();
}


function main() {
    /* document.querySelector("#btnOptions").addEventListener('click', ( ev) => {
     get().runtime.openOptionsPage();
     }); */

    try {
        init();

        var menu = new BMMenu();
        menu.build();
        updatePermissions();
    } catch (e) {
        console.error(e);
        ModalHandler.showEx(e);
    }
}


function updatePermissions() {
    /*Utils.get().permissions.getAll((permissions) => {
     document.getElementById('permissions').innerText = permissions.permissions.join(', ');
     }); */
}

export default main;