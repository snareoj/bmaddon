/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * Handles Commands from Backend to show notifications
 * 
 * Alarm can only be raised onvce per minute!
 * https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/alarms/get
 * 
 * Use this as less as possible!
 * 
 */
import Utils from 'lib/Utils.js'

//const  LIFE_TIME = 5000;

export default class BackendNotification {
        
        /**
         * Show a message which will be removed after 1 minute.
         */
        raiseInfo( msgID, defMsg ) {
            var b =  Utils.get();
            var title = b.i18n.getMessage("basicmessage.info.title");
            var content = b.i18n.getMessage(msgID + ".content");
            if(!content  || content == "??") {
                content = "['"+id+"']" + defMsg;
            }
            let id = "backendNotification";
            let promise = b.notifications.create(id, {
              "type": "basic",
              "title": title,
              "message": content
            });
            
            b.alarms.create({
                delayInMinutes : 1
            }).then ( ( alarm ) => {
                if(alarm) {
                    b.notifications.clear(id);
                }
            });
        }
        
        msg ( key, def, params) {
            if(key.indexOf('.')>= 0) {
                throw new Error("i18n: Key of message must not contain a dot: '"+key+"'");
            }            
            var b =  Utils.get();
            var title = b.i18n.getMessage(key, params);  
            if(this.isInvalid(title)) {
                // Dont set the key into ICON urls.
                if(key && key.indexOf('_iconUrl') >= 0 ) {
                    title = def;
                } else {
                    title = "['"+key+"'] " + def;
                }
            }
            return title;
        }
        
        
        isInvalid( val ) {
           return !(val != '' && val != '??' && typeof val !== 'undefined')
        }
        i18nParams(params) {
            if(params && params.length) {
                let p = [];
                for(let key of params){
                    if(typeof key === 'string' && key.indexOf("i18n:") == 0) {
                        let k = key.substring("i18n:".length);
                        p.push(this.msg(k, k));                        
                    } else {
                        p.push(key);
                    }
                }
                return p;
            }
            return undefined;
        }
                
        raiseBasic( msgID, params, defMsg, callback ) {
            let content = Utils.i18nMsg(msgID + "_content", defMsg, params);
            let title = Utils.i18nMsg(msgID + "_title", 'Info');
            let iconUrl = Utils.i18nMsg(msgID + "_iconUrl", '/icons/icon_info.png' );            
            let id = "backendNotification";
            let msg  = {
              type: 'basic',
              message: content,
              title: title,
              iconUrl : iconUrl
            };
            console.log(id, msg);
            Utils.get().notifications.create(id, msg , ( res, error ) => {
                console.log("Shown:", res, error);
                if(typeof callback === 'function') {
                    callback( { event : 'shown', id : res }, error );
                }
            });
            /*
            b.alarms.create({
                delayInMinutes : 1
            }).then ( ( alarm ) => {
                if(alarm) {
                    b.notifications.clear(id);
                }
            }); */
        }
        
        handle(obj, sender, callback) {
            if(obj.cmd === "backend.notify") {
                this.raiseBasic(
                    obj.msgID,
                    obj.params,
                    obj.defMsg, 
                    callback);
                return true;
            }      


            return false;
        };
}


