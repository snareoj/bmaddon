import HistoryService from 'background/HistoryService.js'
import sinonChrome from 'sinon-chrome'; // from 'sinon-chrome'
import sinon from 'sinon'; // from 'sinon-chrome'

/* 
 * Spec for Background
 */
describe("History Spec", function() {   
    
    window.chrome = sinonChrome.chrome;

    let history = window.chrome.webNavigation.onHistoryStateUpdated;
    
    // BG Script first gets Config from Storage.
    /*sinonChrome.chrome.storage.sync.get.resolves({
       settings : { 'hello' : 'world' } 
    }); */
    // NAVHandler calls update tabs
     /* sinonChrome.chrome.storage.local.get.callsArgWith(1, {
       settings : { 'hello' : 'world' } 
    }); */
    
  it("should init and register a listener ", function() {
    
        let service =  new HistoryService();
        
        service.init();
        
        expect(
            history.addListener.calledOnce
        ).toBe(true);

     //   history.addListener.resetHistory();
  });
  
  it("counts the hit of the bookmark", function() {
        // with Mock persistence
        let updatedBookmark;
        let service =  new HistoryService(undefined, {
            getBookmarkByURL : function( url , callback ) {
                callback({ url : 'URL', count : 5 }, undefined);
            },
            updateBookmark : function( bm , callback ) {
                updatedBookmark = bm;
                callback(true, undefined);
            }
        });
        
        service._countBookmark({            
            url : 'URL'            
        });
        
        expect(updatedBookmark).toBeDefined();
        expect(updatedBookmark.count).toBe(6);
  });
});
