/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global ModalHandler */
/* global Utils */

import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import BookmarkTemplate from 'lib/Template_Bookmark.js'
import BookmarkListTemplate from 'lib/Template_BookmarkList.js'
import BMMenu from 'lib/menu.js'
import $ from 'jquery'
import JSONImpExporter from 'lib/JSONImporter.js'

"use strict";

function init() {        // Load
    
    function serach(){
         Utils.get().downloads.search({ }, ( res, error) => {
            for (let d of res) {
                let li = document.createElement("li");
                li.textContent = d.filename;
                document.getElementById('filesList').appendChild(li);
            }
        });
    }
    
    $("#btnsearch").on('click', function(){
        serach();
    });    
    serach();

    var menu = new BMMenu();
    menu.build();
}

export default function main(){
        /* document.querySelector("#btnOptions").addEventListener('click', ( ev) => {
        get().runtime.openOptionsPage();
    }); */
    try {
        init();
    } catch (e) {
        console.error(e);
        ModalHandler.showEx(e);
    }
}
