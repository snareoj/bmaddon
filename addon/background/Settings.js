/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'

/* 
 * Settings Object
 */
export default function Settings(){
    this.defaultSettings = {
        'lastChangeDate' : null,
        'viewClass' : "big",
        'persistencetype' : 'none', 
        'webdav_url' : "",
        'webdav_user' : "",
        'webdav_password' : ""
    };
    this.props = Object.assign({}, this.defaultSettings);
}
/**
 * 
 * @param {type} key
 * @param {type} value
 * @returns {undefined}
 */
Settings.prototype.setProp = function(key, value){
    console.log("SetProp " + key);
    this.props[key] = value;
};

/**
 * 
 * @param {type} key
 * @param {type} defValue
 * @returns {Object}
 */
Settings.prototype.getProp = function(key, defValue){
    console.log("Get Property '"+key+"'");
    if(typeof this.props[key] !== 'undefined') {
        return this.props[key];
    } else {
        return defValue;
    }
};
/**
 * Returns the current used Storage.
 * See https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage/StorageArea/set
 * @private
 * @returns {unresolved}
 */
Settings.prototype._get = function(){
    var itsMe = Utils.get();
    return itsMe.storage.sync;
};

Settings.prototype.saveOptions = function( callback ) {
  console.log("Saving " + JSON.stringify(this.props));
  try {
        this._get().set({
              settings : this.props
        });  
        if(typeof callback === 'function') {
            callback(true);
        }
        console.log("Saved Options");    
    } catch ( e ) {
        console.error("Failed to save",  e);        
        if(callback) {
            callback(undefined, e);
        }
    }
};

Settings.prototype.restoreOptions = function( callback ) {
  this._get().get( 
        { settings : this.defaultSettings }, (res, error) => {
            if(error) {
                console.error("Error: " ,error);
            }else {
                console.log("Loading settings" );
                this.props = res.settings;
            }
            if(typeof callback === 'function'){
                callback ( this.props, error );
            }

    });
};


/**
 * Handles Settings Events
 * @param {cmd} cmdMessage 
 * @param {sender} sender
 * @param {Function} callback 
 */
Settings.prototype.handle = function (obj, sender, callback) {
    if(obj.cmd === "settings.get") {
        var setting = this.getProp(obj['key']);
        if(callback) {
            callback(setting);
        }
        return true;
    } else if(obj.cmd === "settings.load") {
        this.restoreOptions(callback);
        return true;
    } else if(obj.cmd === "settings.save") {
        var settings = obj.obj;
        if(settings) {
            for (var item in settings) {
                this.setProp(item, settings[item]);
            }
            this.saveOptions(callback);
        }
        return true;
    } else if(obj.cmd === "settings.set") {
        this.setProp(obj['key'], obj['value']);
        this.saveOptions(callback);
        return true;
    }
    return false;
};
