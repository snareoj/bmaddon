/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import EventBus from 'background/EventBus.js'
import JSONImpExporter from 'lib/JSONImporter.js';

export default function ClientListener( eventBus ){
    this.eventBus = eventBus;
}


ClientListener.prototype.attach = function(){
    this.addOnConnectListener();
    this.eventBus.addCmdHandler(this);
 };


ClientListener.prototype.addOnConnectListener = function(){
    // Listen to ContentScript Events on a port
    Utils.get().runtime.onConnect.addListener((portFromCS) => {
        this._onConnect(portFromCS);
    });
}

ClientListener.prototype._onConnect = function( portFromCS ){
//    portFromCS.postMessage({settings: this.settings});
    // Messages from ContentScript
    // https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Content_scripts
    // https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/runtime/onMessage#Parameters
    portFromCS.onMessage.addListener((cmd, resp) => {
          this._onClientMessage( portFromCS, cmd, resp);
    });
     portFromCS.onDisconnect.addListener(() => {
         console.log("Client disconnected", portFromCS);
    });
    console.log("Client connected", portFromCS);
 };


ClientListener.prototype._onClientMessage = function( client, cmd, resp ){
    var sender = client.sender;
    console.log("Message from client: " , cmd, sender, resp);
    try {
        var cmdMessage = JSON.parse(cmd);
        this.handleClientMsg(cmdMessage, sender, (result, error) => {
            client.postMessage(
                    {
                        'msgid': cmdMessage.msgid,
                        'res': result,
                        'error': error
                    });
        });
    } catch (e) {
        console.error("Exception ", e);
        let answer = 
                    {
                        'msgid': cmdMessage.msgid,
                        'res': undefined,
                        'error': e
                    };
        if(resp) {
            resp( answer );
        } else if (client.postMessage) {
            client.postMessage( answer );
        }
        throw e;
    }
    return true;
 }; 

/** 
 * This is actually called from CLIENT
 * @param {cmdMessage} cmdMessage
 * @param {object} sender
 * @param {type} callback
 * @returns {void}
 */
ClientListener.prototype._importJSON = function (cmdMessage, sender, callback) {    
    try {
        let importFunc =  (obj, callback) => {
            this.eventBus.handleCmd({
                cmd: "db.updateBookmark",
                obj: obj
            }, 
            sender,                 
            (bms, error) => {
                if (error) {
                    appendError(error);
                } else {
                    if (bms.info) {
                        console.log("Imported: " + bms.info.url);
                    } else {
                        console.log("Imported unknown: " + JSON.stringify(bms));
                    }
                }
                callback(bms, error);
            });
        };
        let j = new JSONImpExporter( importFunc );
        j.import(cmdMessage.obj, ( res, error) => {                
            if(res) {
                callback(res.length);
            } else {
                callback(null, error);
            }
        });
    }catch( e ) {
        console.error("Import failed ",e);
        callback(null, e);
    }
};


/**
 * Handles events from clients (client === injected scripts)
 * 
 * See Client.js in content_scripts
 * 
    "db.removeBookmark"
    "nav.editBookmark"
    "tags.load"
    "db.updateBookmark"
    "client.afterBookmarkUpdate"
 * 
 * @param {cmd} cmdMessage 
 * @param {object} sender
 * @param {Function} callback
 * @return {boolean} true if cmd was handled
 */
ClientListener.prototype.handleClientMsg = function (cmdMessage, sender, callback) {
    if ( cmdMessage.cmd === 'import.JSON') {
        this._importJSON(cmdMessage, sender, callback);
        return true;
    } else if ( [    
        'client.lookupBookmarkByURL',
        "db.removeBookmark",
        "nav.listBookmarks",
        "db.createAndPersistBookmark",
        "nav.editBookmark",
        "nav.openTopsites",
        "nav.home",
        "tags.load",
        "db.updateBookmark"].indexOf(cmdMessage.cmd) >= 0) {
        this.eventBus.handleCmd(cmdMessage, sender, callback);
        return true;
    } else  if ( cmdMessage.cmd=== 'client.afterBookmarkUpdate' ) {
        // User added / removed TAGS from BOokmark => Update current available tags
        this.eventBus.handleCmd({
            cmd : "tags.addTags",
            tags : cmdMessage.tags
        }, sender, (res, error) => {
            if(error) {
                callback(res, error);
                return;
            }
            // Autosync if avail
            this.eventBus.handleCmd({
                cmd : "sync.autoSyncTrigger",
                reason : 'user_edited_bookmark_infrontend'
            }, sender, callback);
        });
        return true;
    } 
    return false;
};

/**
 * Handles events from EventBus (client === injected scripts)
 * @param {cmd} cmdMessage 
 * @param {object} sender
 * @param {Function} callback
 * @return {boolean} true if cmd was handled
 */
ClientListener.prototype.handle = function (cmdMessage, sender, callback) {
    if ( cmdMessage.cmd === 'import.JSON') {
        this._importJSON(cmdMessage, sender, callback);
        return true;
    } else if ( cmdMessage.cmd === 'import.JSON') {
        this._importJSON(cmdMessage, sender, callback);
        return true;
    }
    return false;
};