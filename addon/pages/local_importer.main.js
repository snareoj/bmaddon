"use strict";
import main from 'pages/local_importer'
import Frontend from 'lib/addon/common/Frontend.js'

(function (global) {
      global.addEventListener("DOMContentLoaded", main);
      Frontend.init();
})(window.document);