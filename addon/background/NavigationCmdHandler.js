/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * Handles the navigation cmds like editBookmark, open bookmark list etc.
 */
import Utils from 'lib/Utils.js';

export default function NavigationCmdHandler(settings, listener) {

    this.settings = settings;
    this.listener = listener;

    /**
     * Handles navigation events
     * @param {cmd} cmdMessage 
     * @param {object} sender
     * @param {Function} callback 
     */
    this.handle = function (cmdMessage, sender, callback) {
        if (cmdMessage.cmd === "nav.editBookmark" && cmdMessage.id) {
            this.editBookmark(sender, cmdMessage.id , callback || DefaultCallback);
            return true;
        } else if (cmdMessage.cmd === "nav.sync") {
            this.openSync(sender);
            return true;
        } else if (cmdMessage.cmd === "nav.home") {
            this.openHome(sender);
            return true;
        }else if (cmdMessage.cmd === "nav.listBookmarks") {
            this.listBookmarks(sender, cmdMessage.hash, callback || DefaultCallback);
            return true;
        } else if (cmdMessage.cmd === "nav.openTopsites") {
            this.openTopsites(sender);
            return true;
        } else if (cmdMessage.cmd === "nav.openHistory") {
            this.openHistory(sender);
            return true;
        } else if (cmdMessage.cmd === "nav.openOptions") {
            this.openOptions(sender);
            return true;
        } else if (cmdMessage.cmd === "nav.openImporter") {
            this.openImporter(sender);
            return true;
        } else if (cmdMessage.cmd === "nav.openJSONImportAndExport") {
            this.openJSONImportAndExport(sender);
            return true;
        } else if (cmdMessage.cmd === "nav.openBookmarkURL") {
            this.openBookmarkURL(sender, cmdMessage.obj);
            return true;
        } else if (cmdMessage.cmd === "nav.about") {
            this.openAbout(sender, cmdMessage.obj);
            return true;
        }
        return false;
    };

    NavigationCmdHandler.prototype._get = function () {
        var itsMe = Utils.get();
        return itsMe;
    };

    function DefaultCallback(tab, error) {
        if (tab) {
            console.log(`Updated tab: ${tab.id}`);
        }
        if (error) {
            console.log(`Error: ${error}`);
        }
    }

    NavigationCmdHandler.prototype._openUrl = function (sender, url, callback) {
        // https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/tabs/update
        let id = 0;
        if(sender && sender.tab) {
            id = sender.tab.id;
        }
        this._get().tabs.update(
                id,
                {url: url},
                callback
                );
    };

    /*
     * Opens in current tab, and load edit bookmark page
     */
    NavigationCmdHandler.prototype.editBookmark = function (sender, id, callback) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        console.log("editBookmark");
        this._openUrl(sender, "/pages/bookmark_edit.html?id=" + id, callback);
    };

    /**
     * Opens in current tab, and load list of bookmarks 
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.listBookmarks = function (sender, hash, callback) {
        // Object { id: "bookmarks@snares.io", frameId: 0, url: "https://blog.fefe.de/", extensionId: "bookmarks@snares.io", contextId: "7-0" }
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        var url = "/pages/bookmark_list.html";
        if(hash) {
            url += "#" + hash;
        }
        this._openUrl(sender, url, callback);
    };
    /**
     * Open openAbout page
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.openAbout = function (sender) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, "/pages/about.html", DefaultCallback);
    };
    /**
     * Open Topsites page
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.openHome = function (sender) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, "/pages/home.html", DefaultCallback);
    };
    /**
     * Open Topsites page
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.openSync = function (sender) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, "/pages/sync.html", DefaultCallback);
    };
    /**
     * Open Topsites page
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.openTopsites = function (sender) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, "/pages/topsites.html", DefaultCallback);
    };
    
    /**
     * Open History page
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.openHistory = function (sender) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, "/pages/history.html", DefaultCallback);
    };
    /**
     * Open Options page
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.openOptions = function (sender) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, "/pages/options.html", DefaultCallback);
    };

    /**
     * Open openJSONImportAndExport
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.openJSONImportAndExport = function (sender) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, "/pages/json_imporexport.html", DefaultCallback);
    };
    
    /**
     * Open Importer
     * @param {tab} sender 
     */
    NavigationCmdHandler.prototype.openImporter = function (sender) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, "/pages/local_importer.html", DefaultCallback);
    };



    /**
     * Open URL of an bookmark
     * @param {tab} sender 
     * @param {bookmark} bookmark
     */
    NavigationCmdHandler.prototype.openBookmarkURL = function (sender, bookmark) {
        if (this._checkSender(sender)) {
            throw "You need to supply a sender with a tab object";
        }
        this._openUrl(sender, bookmark.info.url, (res, error) => {
            console.log("this.listener && res && error === undefined", this.listener, res, error === undefined, this);
            if (this.listener && res && error === undefined) {
                this.listener.on("res.openBookmarkURL", bookmark);
            }
            DefaultCallback(res, error);
        });
    };


    /**
     * Checks if sender is set
     * @param {object} sender
     */
    NavigationCmdHandler.prototype._checkSender = function (sender) {
        return !!!sender;
    };
}

            