/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var gulp = require('gulp');
var zip = require('gulp-zip');
var watch = require('gulp-watch');
var clean = require('gulp-clean');
var gulpCopy = require('gulp-copy');
var rename = require("gulp-rename");
const shell = require('gulp-shell');

var fs = require('fs');
var package = JSON.parse(fs.readFileSync('./package.json'));

var build_ff_destname = package.name + "_" + package.version + ".xpi";
var build_chrome_destname = package.name + "_" + package.version + ".crx";
var nodeLibCSSFiles = [    
                './node_modules/bootstrap/dist/css/bootstrap.min.css',
                './node_modules/bootstrap/dist/fonts/*.*',
                './node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css',
                './node_modules/bootstrap-material-design/dist/css/ripples.min.css'];
            
gulp.task('default', ['watchAndCopyNodeModules'], function () {

});

gulp.task('cleanLibNodeModules', function () {
   return gulp.src('./addon/vendor/node_modules')
		.pipe(clean({force: true}));    

}); 
gulp.task('copyLibToAddonVendor', [ 'cleanLibNodeModules'], function() {   
    return gulp
        .src(nodeLibCSSFiles)
        .pipe(gulpCopy("./addon/vendor", {}));                
});


function execJasmine(){    
    var Jasmine = require('jasmine');
    var jasmine = new Jasmine();

    jasmine.loadConfigFile('spec/support/jasmine.json');
    jasmine.configureDefaultReporter({
        showColors: false
    });
    jasmine.execute();
}

gulp.task('jasmine_plain', function () {
    execJasmine();
});

var jasmineBrowser = require('gulp-jasmine-browser');

gulp.task('jasmine-watch', function() {
  var filesForTest = ['src/**/*.js', 'spec/**/*spec.js'];
  return gulp.src(filesForTest)
    .pipe(watch(filesForTest))
    .pipe(jasmineBrowser.specRunner())
    .pipe(jasmineBrowser.server({port: 8888}));
});

gulp.task('jasmine-chrome', function() {
  return gulp.src(['src/**/*.js', 'spec/**/*_spec.js'])
    .pipe(jasmineBrowser.specRunner({console: true}))
    .pipe(jasmineBrowser.headless({driver: 'chrome'}));
});
// https://stackoverflow.com/questions/28048029/running-a-command-with-gulp-to-start-node-js-server
var nodemon = require('nodemon');
gulp.task('startTestServer', function () {
 // configure nodemon // https://nodemon.io/
    nodemon({
        // the script to run the app
        script: 'test/express.testserver.js',
        // this listens to changes in any of these files/routes and restarts the application
        watch: ["server.js", 'addon/*/**', 'test/spec/**'],
        ext: 'js'
        // Below i'm using es6 arrow functions but you can remove the arrow and have it a normal .on('restart', function() { // then place your stuff in here }
    }).on('restart', () => {
        gulp.src('test/express.testserver.js')
          // I've added notify, which displays a message on restart. Was more for me to test so you can remove this
          .pipe(notify('Running the start tasks and stuff'));
        }
    );
    console.log("Site available at: http://localhost:8000/jasmine.runner.html");
 });
 
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var concat = require("gulp-concat");

gulp.task('cleanBuildDir', function () {
   return gulp.src('./build')
		.pipe(clean({force: true}));    

}); 
gulp.task('copyLibToBuildDir',['cleanBuildDir'], function() {   
    return gulp
        .src(['addon/vendor/**/*'])
        .pipe(gulp.dest("./build/vendor"));                
});
gulp.task('copyNonJSBuildDir', ['copyLibToBuildDir'], function() {   
    return gulp
        .src(["!addon/**/*.js", "!addon/**/*.ejs", '!addon/vendor/**/*',  "addon/**/*"])
        .pipe(gulp.dest("./build"));                
});

gulp.task("build-babel", ['copyNonJSBuildDir'], function () {
  return gulp.src(['!addon/vendor/**/*', "addon/**/*.js"])
    .pipe(sourcemaps.init())
    .pipe(babel())
    //.pipe(concat("all.js"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("build"));
});


//chrome.exe --pack-extension=C:\myext --pack-extension-key=C:\myext.pem
gulp.task('_execChrome', shell.task([
//                    'chromium-browser --pack-extension="./build" --pack-extension-key="./private/addon.pem"'
                    '/usr/bin/google-chrome --pack-extension="./build" --pack-extension-key="./private/addon.pem"'
 ])); 
                
gulp.task('buildChrome', ['_execChrome'], function(){
    return gulp.src("./build.crx").pipe(rename(build_chrome_destname)).pipe(gulp.dest('dist/signed'));

}); 

//https://developer.mozilla.org/en-US/Add-ons/Distribution/Submitting_an_add-on
gulp.task('buildFirefox', function() {
    return gulp.src(['build/**/*'])
        .pipe(zip(build_ff_destname))
        .pipe(gulp.dest('dist/unsigned'));
}); 


var runSequence = require('run-sequence');
gulp.task('build', function() {
    runSequence('copyNonJSBuildDir', 'copyLibToAddonVendor', 'build-webpack', 'replace-jsfilenames', 'buildFirefox', 'buildChrome');
}); 

gulp.task('clean', function () {
   return gulp.src('dist/unsigned')
		.pipe(clean({force: true}));

});

var gutil = require('gulp-util'),
    webpack = require('webpack'),
    webpackConfig = require('./webpack.config.js');
    
gulp.task('build-webpack', function(callback) {
   webpack(webpackConfig, function (err, stats) {
        if (err)
            throw new gutil.PluginError('webpack:build', err);
        gutil.log('[webpack:build] Completed\n' + stats.toString({
            assets: true,
            chunks: false,
            chunkModules: false,
            colors: true,
            hash: false,
            timings: false,
            version: false
        }));
        callback();
    });
});

var extend = require('util')._extend;
gulp.task('webpack-watch', function(callback) {
    
    let cfg = extend(webpackConfig, {
       watch : true,
       watchOptions: {
            aggregateTimeout: 300 /* ,
            poll: 1000 */
        }
    });
    
   webpack(cfg, function (err, stats) {
        if (err)
            throw new gutil.PluginError('webpack:build', err);
        gutil.log('[webpack:build] Completed\n' + stats.toString({
            assets: true,
            chunks: false,
            chunkModules: false,
            colors: true,
            hash: false,
            timings: false,
            version: false
        }));       
    });
});
var replace = require('gulp-replace'); 
gulp.task('replace-jsfilenames', function () {    
    var webpackManifest = require('./build/gen-js-names.json');
    var replacements =  [];
    console.log(webpackManifest);
    gulp.src('addon/manifest.json')
    .pipe(replace('__VERSION__', package.version))
    .pipe(replace('__CLIENT_CHUNK_JAVASCRIPTFILE__', webpackManifest['node.js']))
    .pipe(replace('__CLIENT_JAVASCRIPTFILE__', webpackManifest['content_scripts/ContentScipt.main.js']))
    .pipe(gulp.dest('build'));
});