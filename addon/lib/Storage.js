/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global Promise */

import Utils from 'lib/Utils.js';

/** 
 * Index of a storage
 * 
 * Simply stores all items in storage.local under object with PREFIX
 * @param {Storage} storage 
 */
function StorageIndex( storage ){
    this.storage = storage;
    this._DEBUG = false;
}

StorageIndex.prototype.addItem = function( itemId, callback ) {
    this.storage._getItemById('index', ( index, error) => {        
        if(error) {
            throw new Error("Cant get index");
        }
        if(!index) {
            index = {
                id : 'index'
            };
        }
        if(!index.items){
            index.items = [];
        }
        if(typeof itemId === 'string') {
            if(index.items.indexOf(itemId) < 0) {
                index.items.push(itemId); // Its new
                this.storage._updateItem(index, ()=> callback());
            } else {
                callback();
            }
        } else if(Array.isArray(itemId)) {
            for (let i = 0, max = itemId.length; i < max; i++) {
                let iId = itemId[i];
                if(index.items.indexOf(iId) < 0) {
                    index.items.push(iId); // Its new
                }                
            }
            this.storage._updateItem(index, ()=> callback());
        } else {
            throw new Error("Unknown id type, cant update index.");
        }
    });
};
StorageIndex.prototype.removeItem = function( id, callback ) {
    this.storage._getItemById('index', ( res, error) => {                
        if(error) {
            throw new Error("Cant get index");
        }
        if(!res || !res.items){
            callback();
            // NO index?
            return; 
        }
        let idx = res.items.indexOf(id);
        if(idx >= 0) {
            res.items = res.items.filter(item => item !== id);
            this.storage._updateItem(res, ()=> callback());
        } else {
            callback();
        }
    });
};
StorageIndex.prototype.listItems = function( callback ) {
    this.storage._getItemById('index', ( res, error) => {                
        if(error) {
            throw new Error("Cant get index");
        }        
        if(!res) {
            res = {
                id : 'index'
            };
        }
        if(!res.items){
            res.items = [];
        }
        if(this._DEBUG) {
            console.log("Index: listItems ", res);
        }
        callback(res, error);
    });
};

StorageIndex.prototype.rebuildIndex = function( callback ) {
    this.storage._getStorage().get(null, ( list , error) => {
        if(!list || error) {
            callback(null, error);
            return;
        }
        let prefix = this.storage.prefix;
        let idx;
        for (let key in list) {
            if(key.indexOf(prefix)=== 0) {
                idx = list[key];
                break;
            }
        }
        if(idx) {
            // 
            this._checkIndexFor(0,  {
                list : idx.items,
                notFound : [],
                index : []
            }, ( result, error) => {
                
                if(error) {
                    callback(null, error);
                    return;
                } else {
                    this.storage._updateItem({
                        id : 'index',
                        items : result.index
                    }, ( res, error) => {
                        callback(res, error);
                    })
                }
                
            });
        } else {
            console.log("Index not found  for " + prefix);
        }
    });
};

StorageIndex.prototype._checkIndexFor = function( num, param, callback ) {
    if(num < param.list.length) {
        let id = param.list[num];
        this.storage.getItemById(id, (res, error) => {
            
            if(error){
                callback(null, error);
                return;                
            }
            
           if(res) {
               param.index.push(id);
           } else {
               param.notFound.push(id);
           }
            this._checkIndexFor( num + 1, param, callback);
        });
    } else {
        delete param.list;
        callback( param )
    }
};

/** 
 * Item Persistence
 * 
 * Simply stores all items in storage.local under object with PREFIX
 * @param {string} prefix 
 */
export default function Storage( prefix ) {
    if(!prefix) {
        throw new Error("Prefix needed");
    }
    this.storageType = 'local';
    this.prefix = prefix;
    this.index = new StorageIndex( this );
}

/**
 * Returns the object from the storage where the list of items is saved into
 * @private
 * @returns {object}
 */
Storage.prototype._createStorageObj = function(){
    let obj = {};
    obj[this.prefix] = {};
    return obj;
};

/**
 * Returns the object from the storage where the list of items is saved into
 * @private
 * @param {string} id
 * @returns {object}
 */
Storage.prototype._createStorageObjAndGetItem = function( id ){
    let obj = {};
    //obj[this.prefix] = {};
   // obj[this.prefix][id] = null;
    obj[this.prefix+":"+id] = null;
    return obj;
};
/**
 * Returns the object from the storage where the item is saved into
 * @private
 * @param {object|array} item item Needs an ID!
 * @returns {object}
 */
Storage.prototype._createStorageObjAndSetItem = function(item){
    let obj = {};
    //obj[this.prefix] = {};
    //obj[this.prefix][item.id] = item;
    let add = (obj, i) => {        
        if(!i.id) {
            throw Error("No ID in item, cant save");
        }  
        i['lastChange'] = new Date().toISOString();
        obj[this.prefix+":"+i.id] = i;
    }; 
    
    if(Array.isArray(item)) {
        for (let i = 0, max = item.length; i < max; i++) {
            add(obj, item[i]);
        }
    } else {
        add(obj, item);
    }

    return obj;
};

/**
 * Returns the object from the storage where the item is saved into
 * @private
 * @param {string} id Needs an ID!
 * @returns {object}
 */
Storage.prototype._createStorageObjAndDeleteItem = function(id){
    if(!id) {
        throw Error("No ID cant delete");
    }
    return this.prefix+":"+id;
};

/**
 * Returns the current used Storage.
 * See https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/storage/StorageArea/set
 * @private
 * @returns {unresolved}
 */
Storage.prototype._getStorage = function(){
    var itsMe = Utils.get();
    return itsMe.storage[this.storageType];
};

/**
 * Get the index
 * @param {type} callback
 */
Storage.prototype.getIndex = function ( callback ) {      
    this._getItemById('index', callback);
};

/**
 * Get A item from Storage by Item ID
 * @param {string} id
 * @param {type} callback
 * @returns {undefined}
 */
Storage.prototype.getItemById = function ( id , callback) {
    if(id === 'index') {
        throw new Error("ID is reserved. Use another id.");
    }    
    this._getItemById(id, callback);
};
/**
 * Get A item from Storage by Item ID
 * @param {string} id
 * @param {type} callback
 * @returns {undefined}
 */
Storage.prototype._getItemById = function ( id , callback) {
    if(!id) {
        throw new Error("No ID supplied");
    }    
    var obj = this._createStorageObjAndGetItem(id);
    this._getStorage().get(obj, (res, error) => {
        if(this._DEBUG) {
            console.log("Got from storage", res, id);
        }
        if (callback) {
            callback(res[this.prefix+":"+id], error);
        } else {
            console.warn("No callback");
        }
    });
};

/**
 *  Set a item, uses "item:URL" as KEY
 * @param {object} item
 * @param {function} callback
 * @returns {undefined}
 */
Storage.prototype.updateItem = function (item, callback) {
    if(!item.id) {
        throw new Error("No ID to update item!");
    }   
    if(item.id === 'index') {
        throw new Error("ID is reserved. Use another id.");
    }    
    this.index.addItem(item.id, () => {        
        this._updateItem(item, callback);
    });
};

/**
 *  Set a item, uses "item:URL" as KEY
 * @private
 * @param {object} item
 * @param {function} callback
 * @returns {undefined}
 */
Storage.prototype._updateItem = function (item, callback) {
    if(!item.id) {
        throw new Error("No ID to update item!");
    }   
    var obj = this._createStorageObjAndSetItem(item);
    this._getStorage().set(obj, (res, error) => {
        if(this._DEBUG) {
            console.log("Set into storage", item, res);
        }
        if (callback) {
            // Default storage implementation does not return anything if updated
            callback(res || item, error);
        } else {
            console.warn("No callback");
        }
    });
};

/**
 *  Set a item, uses "item:URL" as KEY
 * @param {array} items
 * @param {function} callback
 * @returns {undefined}
 */
Storage.prototype.bulkUpdateItems = function (items, callback) {
    if(!Array.isArray(items)) {
        throw new Error("need an array of items!");
    }   
    this.index.addItem(items, () => {   
        var obj = this._createStorageObjAndSetItem(items);
        this._getStorage().set(obj, (res, error) => {
            if(this._DEBUG) {
                console.log("Set into storage", items, res);
            }
            if (callback) {
                // Default storage implementation does not return anything if updated
                callback(res || items, error);
            } else {
                console.warn("No callback");
            }
        });
    });
};

/**
 *  Removes a item, uses "item:URL" as KEY
 * @param {String} id
 * @param {Function} callback
 * @returns {undefined}
 */
Storage.prototype.removeItem = function ( id , callback) { 
    if(!id) {
        throw new Error("No ID to removeItem!");
    }     
    if(id === 'index') {
        throw new Error("ID is reserved. Use another id.");
    }    
    this.index.removeItem(id, () => {
        this._removeItem(id, callback);
    });    
};
/**
 *  Removes a item, uses "item:URL" as KEY
 *  @private
 * @param {String} id
 * @param {Function} callback
 * @returns {undefined}
 */
Storage.prototype._removeItem = function ( id , callback) { 
    if(!id) {
        throw new Error("No ID to removeItem!");
    }     
    console.log("Removing ", id);    
    var obj = this._createStorageObjAndDeleteItem(id);
    this._getStorage().remove( obj , ( res, error ) => {
        if(this._DEBUG) {
            console.log("Removed from storage", id , res);
        }
        if (callback) {
            callback(id, error);
        } else {
            console.warn("No callback");
        }
    });
};

/** * 
 * Lists all Items
 * Gives the callback an array of all PageInfos.
 * 
 * @param {type} callback
 * @returns {undefined}
 */
Storage.prototype.listItems = function (callback) {
    // MOve this to Frontend?
    this.index.listItems(( index, error ) => {
        if(this._DEBUG) {
            console.log("List Items", index.items, error);
        }
        var items = [];
        var promises = [];
        for(let id of index.items ){ // THose are ids
            promises.push(new Promise( (resolve, reject)=> {
                this.getItemById(id, ( item , error) => {
                    if(error) {
                        reject(error);
                    } else {
                        if(item) {
                            items.push(item);
                        } else {
                            console.warn("Index Broken: No ItemFound for ", id);
                        }
                        resolve();
                    }
                })
            }));
        }
        if(this._DEBUG) {                
            console.log("Read index. total keys: " + index.items.length);
        }
        Promise.all(promises)
            .then(  ()  => callback(items))
            .catch( (e) => callback(undefined, e));
    });
};

/** * 
 * Lists all Items
 * Gives the callback an array of all PageInfos.
 * 
 * @param {type} callback
 * @returns {undefined}
 */
Storage.prototype.clearItems = function (callback) {    
     this.index.listItems(( index, error ) => {
        if(this._DEBUG) {
            console.log("List Items", index.items, error);
        }
        var items = [];
        var promises = [];
        for(let id of index.items ){ // THose are ids
            promises.push(new Promise( (resolve, reject)=> {
                this.removeItem(id, ( item , error) => {
                    if(error) {
                        reject(error);
                    } else {
                        resolve( item );
                    }
                })
            }));
        }
        if(this._DEBUG) {
            console.log("Read index. total keys: " + index.items.length);
        }
        Promise.all(promises).then(()=> {
            callback(items, error);                
        });
    });
};