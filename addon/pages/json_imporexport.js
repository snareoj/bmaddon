/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global ModalHandler */
/* global Utils */

import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import BookmarkTemplate from 'lib/Template_Bookmark.js'
import BookmarkListTemplate from 'lib/Template_BookmarkList.js'
import BMMenu from 'lib/menu.js'
import $ from 'jquery'
import JSONImpExporter from 'lib/JSONImporter.js'
        "use strict";

/* 
 * Imports and Exports from JSON textarea 
 */
function init() {        // Load

    var importer = new JSONImpExporter(
            function (obj, callback) {
                Utils.exec({
                    cmd: "db.updateBookmark",
                    obj: obj
                }, (bms, error) => {
                    if (error) {
                        appendError(error);
                    } else {
                        if (bms.info) {
                            appendSuccess("Imported: " + bms.info.url);
                        } else {
                            appendSuccess("Imported unknown: " + JSON.stringify(bms));
                        }
                    }
                    callback(bms, error);
                });
            });
            
    $("#btnimport").on('click', function () {
        try {
            importer.import($("#impExp").val());
            appendSuccess("Exported bookmarks into textarea");
        } catch (e) {
            console.error(e);
            appendError(e);
            ModalHandler.showEx(e);
        }
    });
    $("#btnexport").on('click', function () {
        importer.export((str, err) => {
            if (!err) {
                $("#impExp").val(str);
                appendSuccess("Imported bookmarks into textarea");
            } else {
                appendError(err);
            }
        });
    });


    var menu = new BMMenu();
    menu.build();
}

function  appendSuccess( txt ) {
    let c = document.createElement("p");
    c.className = "";
    c.textContent = txt;
    document.getElementById("message").appendChild(c);
}


function  appendError( txt ) {    
    let c = document.createElement("p");
    c.className = "danger";
    c.textContent = JSON.stringify(txt);
    document.getElementById("message").appendChild(c);
}

export default function main() {
    /* document.querySelector("#btnOptions").addEventListener('click', ( ev) => {
     get().runtime.openOptionsPage();
     }); */
    try {
        init();
    } catch (e) {
        console.error(e);
        ModalHandler.showEx(e);
    }
}
