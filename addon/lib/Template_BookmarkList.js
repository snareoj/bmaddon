/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global ModalHandler */
/* global Utils */
/* global Hammer */
import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import BookmarkTemplate from 'lib/Template_Bookmark.js'
import * as hammerjs from 'hammerjs'
/* 
 * ==============================
 * Template for a simple Bookmark list.
 *  
 *  Simple Example _listBookmarksFunction:
 *  _listBookmarksFunction = function( callback ){
 *      // Return 0 bookmarks, and an undefined error.
 *      callback( [], undefined );
 *  }
 * 
 * itemRenderer wants a BookmarkTemplate as Return value!
 * 
 * ==============================
 * @param {function} _listBookmarksFunction
 * @param {function} _itemRenderer
 */
export default function BookmarkListTemplate( _listBookmarksFunction, _itemRenderer, _itemListener ) {
    
    if(typeof _listBookmarksFunction !== "function") {
        throw "Please provide a bookmark list function!";
    }
    
    if(typeof _itemRenderer !== "function") {
        throw "Please provide a bookmark item renderer!";
    }
    
    this.id = Utils.numberToString(Date.now());
    this.searchVisible = false;
    this._listBookmarksFunction = _listBookmarksFunction;
    this._itemRenderer = _itemRenderer;
    this._itemListener = _itemListener;
    this.viewClassKey = "bookmarkList.default";
    this.viewClass = "big";
    this.bookmarks = [];    
    this.templates = [];
    this.searchText;
    this.scrollUp = `<div class="row  scrollUp">
        <div class="col-xs-12">
                <span class="glyphicon glyphicon-triangle-top pull-right" aria-hidden="true"></span>
        </div>
    </div> `;
    this.template = ` 
        <div id="${this.id}" ><!-- Without class="container" -->
            
            <div class="actionButtons">               
        
                <div class="btn-group btn-group-sm" id="actions">   
        
                    <!-- LIST TYPE -->
                    <button class="btn btn-default" id="btnView" >
                        <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                    </button> 
        
                    <!-- SHOW SEARCH -->
                    <button  type="button" class="btn btn-default searchVisibleBtn" data-toggle="button" aria-pressed="false" autocomplete="off">
                           <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                     </button>
                </div> 
            </div>

            <div class="panel panel-default searchPanel ">
                <div class="panel-body">      
                    <div class="input-group " >
                        <!-- ## searchAction -->
                        <input type="text" class="form-control" id="searchText" placeholder="'Search for' Regex"
                          data-toggle="popover" data-trigger="focus" title="Cant search" data-content="Something went wrong.">
                        <span class="input-group-btn">
                          <button class="btn btn-default btn-raised" type="button" id="searchBtn">
                              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                          </button>        
                        </span>
                    </div><!-- /input-group -->
                </div>
            </div><!-- /panel -->


            <div class="loading" style="display:none;">
                Loading...    
            </div>
            <div class="bookmarkList" style="display:none;">
                <div class="" id="list">
                </div>

                <div id="noItems">
                    No items!
                </div>    

                <!-- <div id="error">
                    An error occured.
                </div> -->    
            </div><!-- /bmList -->
        </div>  <!-- /container -->
    `;
};

/**
 * Handles errors.
 * @param e Ex to handle
 */
BookmarkListTemplate.prototype._handleEx = function( e ) {
    ModalHandler.showEx(e);
};

/**
 * Prepares default things which should always be available.
 * @returns {undefined}
 */
BookmarkListTemplate.prototype._prepare = function( ) {
    this.tmpl = $($.parseHTML(this.template));     
    this._prepareViews();
};
/**
 * Prepares default things which should always be available.
 * @returns {undefined}
 */
BookmarkListTemplate.prototype._prepareViews = function( ) {
    this._initSearch();
    var toggle = () => {            
        this.viewClass = this.viewClass === "small" ? "big" : "small";         
        Utils.exec({ cmd : "settings.set", 
                "key" : this.viewClassKey,
                "value" : this.viewClass}, (res, error) => {
                    this.applyView();
                    if(error) {
                        this._handleEx(error);
                    }
                });
    };
    var btnViewSmall = this.tmpl.find("#btnView");
   // btnViewSmall.html(Utils.get().i18n.getMessage("btn_reload_text"));
    btnViewSmall.on('click', toggle);
    
};

BookmarkListTemplate.prototype._initSearch= function( ) {
    var txt = this.tmpl.find("#searchText");
   //       txt.popover();
    txt.on("keyup", Utils.debounce((e) => {
        this._doSearch ( txt.val() );
    }, 100));
    this.tmpl.find("#searchBtn").on("click", ( e ) => {
        this._doSearch ( txt.val() );
    });  
        var btn = this.tmpl.find(".searchVisibleBtn");
   //       txt.popover();
    btn.on("click", () => {
        this.searchVisible = !this.searchVisible;
        this._setSearchVisibility();        
    });
    this._setSearchVisibility();
};

BookmarkListTemplate.prototype._setSearchVisibility= function( ) {
    if(this.searchVisible) {
        this.tmpl.find(".searchPanel").show();
        this.tmpl.find(".searchVisibleBtn").addClass("active").attr('aria-pressed', 'true');
    } else {
         this.tmpl.find(".searchVisibleBtn").removeClass("active").attr('aria-pressed', false);
        this.tmpl.find(".searchPanel").hide();
    }  
};

BookmarkListTemplate.prototype._doSearch= function( str ) {    
    try {
        var regex = new RegExp(str); // expression here

        this.tmpl.find(".bmItem").hide().filter( function(){
            var show = regex.test($(this).text());
            return show;
        }).show();
    } catch( e ) {
        this._handleEx(e);
    }
};
/**
 * Prepares default things which should always be available.
 * @returns {undefined}
 */
BookmarkListTemplate.prototype.applyView = function( ) {              
    Utils.exec({ cmd : "settings.get", "key" : this.viewClassKey }, ( viewClass, error ) => {
        this.viewClass = viewClass;
        if(viewClass === "small"){
            $("#"+this.id +" .bookmark-big").hide();        
            $("#"+this.id +" .bookmark-small").show();    
            $("#"+this.id +" #btnView").addClass("active");
        } else {          
            $("#"+this.id +" .bookmark-small").hide();
            $("#"+this.id +" .bookmark-big").show();       
            $("#"+this.id +" #btnView").removeClass("active");
        }  
        
        $("#"+this.id+' .loading').hide();
        $("#"+this.id+' .bookmarkList').show();
        if(error) {
            this._handleEx(error);
        }
    });
        
      
};

/**
 * Reload and Render the list
 * @returns {undefined}
 */
BookmarkListTemplate.prototype.reload = function( callback ) {
    this._listBookmarksFunction(( bookmarks, error ) => {
        try {            
            if(error){
                console.warn("TODO@Templates.js#164: Handle ERROR");
                this._handleEx(error);
                return;
            }
            this.bookmarks = bookmarks;
            if(bookmarks) {
                this.filteredBookmarks = bookmarks.slice(0);
            } else {
                this.filteredBookmarks = [];
            }
            this.renderList( callback );
        } catch ( e ) {
            console.error(e);
            this._handleEx(e);
        }
    });
};

/**
 * Sets the sort func 
 * @param {function} sortFunc
 */
BookmarkListTemplate.prototype.setSortFunc = function( sortFunc ) {
    this.listSortFunction = sortFunc;
};

/**
 * Sets the sort func and renders the list
 * @param {function} sortFunc
 */
BookmarkListTemplate.prototype.sortAndRender = function( sortFunc ) {
    this.setSortFunc(sortFunc);
    this.renderList();
};

/**
 * Goes through the filtered Bookmars, creates the item templates via the "renderer" and adds them to the list
 * @returns {undefined}
 */
BookmarkListTemplate.prototype.renderList = function( callback ) {
    let list = $("#"+this.id +" #list");
    list.empty();    
    if(!this.bookmarks){
        list.hide();
        this.tmpl.find("#noItems").show();
        return;
    }
    if(this.bookmarks.length <= 0){
        this.tmpl.find("#noItems").show();
    } else {
        this.tmpl.find("#noItems").hide();
    }    
    // Copy the list    
    let bmList = this.bookmarks.slice(0);
    
    if(typeof this.listSortFunction === 'function') {
        bmList.sort(this.listSortFunction);
    }
    
    $("#"+this.id+' .loading').show();
    $("#"+this.id+' .bookmarkList').hide();
    /*$("#"+this.id).on("swipe",function( ev ){
        console.log("Swipe event ", ev);
    }); */
    this.templates = [];
    for(var bm of bmList){  
        if(bm && bm.id) {
            var template = this._itemRenderer(this, bm);
            this.templates.push(template);  
        }
    };   
    var num = 0;
    for (var template of this.templates) {
        num ++;
        list.append(template.asHTML());     
        if(num % 5 === 0) {
            list.append(this.scrollUp);
        }   
    }    
    $("#"+this.id +" #list .scrollUp").on('click', (ev) => {
        window.scrollTo(0, 0);
    });
    
    this.applyView();
    
    if(this._itemListener && this._itemListener.onRendered) {
        this._itemListener.onRendered();
    }
    if(typeof callback === 'function') {
        callback(bmList);
    }
};

/**
 * Iterate over the jQuery Nodes
 * @param {type} callback
 * @returns {undefined}
 */
BookmarkListTemplate.prototype.eachItem = function( callback ) {
     for(var template of this.templates){  
        callback(template);
    };       
};
/**
 * Add an action
 * @param obj The HTML which is appended to Actions
 */ 
BookmarkListTemplate.prototype.addAction = function( obj ) {
   this.tmpl.find("#actions").append(obj);
};

/**
 * Appends the bookmark Template to that Document Element
 * @param {type} parentElement
 * @returns {$}
 */
BookmarkListTemplate.prototype.appendTo = function ( parentElement ) {   
    parentElement.append(this.tmpl);    
    this._initHammer(document.getElementById(this.id)); // Do this when the template is appended to document
};

/**
 * Initiates Hammer Listeners
 * @param {DOM Element] parentElement
 */
BookmarkListTemplate.prototype._initHammer = function (parentElement) {   
    // We create a manager object, which is the same as Hammer(), but without the presetted recognizers. 
    var mc = new hammerjs.default.Manager(parentElement, {
	/*recognizers: [
		// RecognizerClass, [options], [recognizeWith, ...], [requireFailure, ...]
		[Hammer.Rotate],
                [Hammer.Tap, { event: 'doubletap', taps: 2 }],
                [Hammer.Pinch, { enable: false }, ['rotate']],
		[Hammer.Swipe,{ direction: Hammer.DIRECTION_HORIZONTAL }],
	] */
    });

    // Tap recognizer with minimal 2 taps
    mc.add( new hammerjs.default.Tap({ event: 'doubletap', taps: 2 }) );
    mc.add( new hammerjs.default.Tap({ event: 'tap', taps: 1 }) );
    mc.add( new hammerjs.default.Swipe({ event: 'swipe', direction : hammerjs.default.DIRECTION_HORIZONTAL	 }) );
    
    function findItemId( element ) {
        if(element && element.getAttribute && element.getAttribute('id') && element.getAttribute('data-role') === 'bookmark'){
            return element.getAttribute('id');
        } else if(element.parentElement) {
            return findItemId(element.parentElement);
        } else {
            return undefined;
        }
    }
    
    mc.on("swipeleft", ev => {
        let id = findItemId(ev.target);  
        if(this._itemListener && id) {
            console.log("swipeleft " + id, ev);      
            this._itemListener.onItem('swipeleft', id);
        }
    });
    mc.on("swiperight", (ev) => {
        let id = findItemId(ev.target);     
        if(this._itemListener && id) {
        console.log("swiperight " + id, ev);   
            this._itemListener.onItem('swiperight', id);
        }
    });
    
    mc.on("tap", (ev) => {
        let id = findItemId(ev.target);    
        if(this._itemListener && id) {
        console.log("tap " + id, ev);    
            this._itemListener.onItem('tap', id);
        }
    });
    mc.on("doubletap", (ev) => {
        let id = findItemId(ev.target);    
        if(this._itemListener && id) {
        console.log("doubletap " + id, ev);    
            this._itemListener.onItem('doubletap', id);
        }
    });
};



function DefaultItemRenderer( listTemplate, bookmark) {
    var bmTmpl = new BookmarkTemplate(bookmark);                    
    var fab = new ActionFactory(bookmark);
    bmTmpl.addAction( fab.openURL() );
    bmTmpl.addAction( fab.edit() );
    bmTmpl.addAction( fab.delete(() => {
        // Deleted! Reload the list
        try {
            listTemplate.reload();
        } catch( e ){
            console.error(e);
            this._handleEx(e);
        }
    }));      
    return bmTmpl.asHTML();
}