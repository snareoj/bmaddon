/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */


import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import $ from 'jquery'
/* 
 * Default Menu
 * If its small device the menu is inserted at the top, else it is visible on the right side and fixed.
 */


function i( i18n, iconName, cmd ) {    
    let name  = Utils.i18nMsg("menu_" + i18n, i18n);    
    return {
        i18n : i18n,
        page : i18n+".html",
        name : name,
        icon : 'glyphicon glyphicon-' + iconName,
        cmd : cmd
    };
}
        
export default function BMMenu (){

    let items = [
        i('home', 'home', 'nav.home'),
        i('bookmark_list', 'bookmark', 'nav.listBookmarks'),
        i('topsites', 'heart-empty', 'nav.openTopsites'),
        i('history', 'time', 'nav.openHistory'),
        i('sync', 'transfer', 'nav.sync')/*,
        i('options', 'cog', 'nav.openOptions'),
        i('importExport', 'import', 'nav.openImporter'),
        i('importExportJSON', 'import', 'nav.openJSONImportAndExport') */
    ];
    let menuLis = '';
    for(let item of items) {
        menuLis += `
            <li>
                <a href="#${item.i18n}" class="item" data-page="${item.page}"  data-cmd="${item.cmd}" tooltipText="${item.name}">
                    <span class="${item.icon}" aria-hidden="true"></span>                
                    <span class="hidden-xs hidden-sm name">${item.name}</span>
                </a>
            </li>   `;
    }
    this.topMenuTemplate = `
        <nav class="c-navbar c-navbar-fixed-top" id="topMenu">
                <div class="container-fluid">
                    <ul class="c-nav c-navbar-nav">
                        ${menuLis}
                    </ul
                </div>
        </nav>
    `;
};
BMMenu.prototype.build = function(){    
        var topMenu = $(this.topMenuTemplate);
        $("body").prepend(topMenu);    
        var cb = (res, error) => {
            if(error) {        
                ModalHandler.showErr(error);
                return;
            }
        };
        topMenu.on('click', (ev) => {
           ev.preventDefault();
           let target = ev.target;
           let cmd = target.getAttribute('data-cmd');
           if(!cmd && target.parentNode) {
               cmd = target.parentNode.getAttribute('data-cmd');
           }
           if(cmd) {
               Utils.exec( {
                   cmd : cmd
               }, cb);
           }
        });
        let page = '';
        if(window.location && window.location.pathname && window.location.pathname.lastIndexOf("/") > 0) {
            let idx = window.location.pathname.lastIndexOf("/");
            page = window.location.pathname.substring(idx + 1);
        }
        $('[data-page="' +page+'"]').parent().addClass('active');
};


