/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import $ from 'jquery'
import BMMenu from 'lib/menu.js'
import * as sync from 'lib/WebDavNextCloudSync.js';
import TagsService from 'background/TagsService.js';

/**
 * SyncPage panel
 */
var SyncPage = {
    
    settings : {},
    
    toHumanDate : function( s ){
        try {
            return new Date(Date.parse(s)).toLocaleString();
        } catch ( e ) {
            return '-';
        }
    },
  
    load : function(){
        Utils.exec({ "cmd" : "sync.collectSyncInfos"}, (res, error) => {
           try {
               if(error) {
                   console.log(error);
               }
               this.syncDetails = res;      
               
               if(res['persistencetype'] !== 'webdav') {
                   $('button').prop('disabled', true);
                   $('#syncPanel').append(Utils.i18nMsg('sync_syncDisabled', "Sync is disabled. Enable it at options page."));
               }
               
               $("[data-setting='webdav_lastUploadDate']").text(this.toHumanDate(res['webdav_lastUploadDate']));    
               $("[data-setting='webdav_lastDownloadDate']").text(this.toHumanDate(res['webdav_lastDownloadDate']));
               $("[data-setting='webdav_lastSyncDate']").text(this.toHumanDate(res['webdav_lastSyncDate']));
               $("[data-setting='webdav_lastSyncCheck']").text(this.toHumanDate(res['webdav_lastSyncCheck']));
               $("[data-setting='sync_lastDBChange']").text(this.toHumanDate(res['sync_lastDBChange']));
               
               $("[data-setting='sync_Active']").text((res['sync_Active']));  
               $("[data-setting='autoSyncActive']").text((res['autoSyncActive']));  
               $("[data-setting='autoSyncTriggered']").text((res['autoSyncTriggered']));  
               $("[data-setting='autoSyncSyncOnNextAlarm']").text((res['autoSyncSyncOnNextAlarm']));
               
               let msgs = res['sync_autoSync_messages'];
               let txt = '';
               if(msgs) {
                    for(let m of msgs) {
                        txt += this.toHumanDate(new Date(m.date)) + " " + Utils.i18nMsg(m.msg, m.msg, m.params)+" <br/>";
                    }
                }
               $("[data-setting='sync_autoSync_messages']").html(txt);
               
               
           } catch (e) {
               console.error(e);
           }
       });        
    },
    
    showResult : function(res, error, htmlId, successText) {
            var msg = "";
            if (res && !error) {
                if (res.status === 200) {
                    successText += " (Http-Ok)";
                } else if (res.status === 201) {
                    successText += " (File created)";
                } else if (res.status === 204) {
                    successText += " (File updated)";
                } else if (res.status === 207) {
                    successText += " (Multi OK)";
                }
                msg = '<span class="label label-success">' + successText + '</span>';
            } else {
                let eMsg = error.message;
                if(eMsg && typeof eMsg === 'string') {
                    eMsg = JSON.parse(eMsg);
                    if(eMsg.status === 0) {
                        eMsg.statusText = "Browser denied crossside scripting.";
                    }
                    
                    msg = '<span class="label label-danger">Failed';
                    msg += ": "+ eMsg.status + "/" + eMsg.statusText;
                    msg += "</span>";  
                    ModalHandler.showErr(eMsg);
                } else {
                    msg = '<span class="label label-danger">Failed';
                    if (error.message) {
                        msg += ": " + error.message;
                    }
                    msg += "</span>";  
                    ModalHandler.showErr(error);
                }
            }
            document.querySelector(htmlId).innerHTML = msg;
        },
    
    download : function(){
        document.querySelector("#downloadResult").innerHTML = "<p><bold>Downloading and Importing .... please wait...</bold></p>";
        Utils.exec({
            cmd: "sync.fetch"
        }, (res, error) => {
            console.log("RES", res);
            console.log("error", error);
            let status = { status : 0};
            if(res) {
                status.status = 200;
                this.buildTagList( ( tags ) => {
                this.showResult(status,  error, "#downloadResult", "Downloaded bookmark.json file and imported Bookmarks.");
                    ModalHandler.showMesg('sync.complete', `
                        <p> Imported ${res.length} Bookmarks. Rebuild Tags List: <br/>
                            ${tags}
                        </p>
                    ` );
                });
                this.load();
            } else {
                this.showResult(status,  error, "#downloadResult", "Downloaded bookmark.json file.");
            }
        });
    },
    
    upload : function(){
        document.querySelector("#uploadResult").innerHTML = "<p><bold>Uploading... please wait...</bold></p>";  
        Utils.exec({
            cmd: "sync.upload"
        }, (res, error) => {
            console.log("RES", res);
            console.log("error", error);
            this.showResult(res, error, "#uploadResult", "Uploaded bookmark.json file.");
            this.load();
        });
    },
    
    buildTagList : function( cb ){
        Utils.exec({ cmd : "db.listBookmarks" }, ( bookmarks , error) => {
            if(error) {
                ModalHandler.showErr(error);
                return;
            }
            var tempTagService = new TagsService();
            tempTagService.autoPersist = false;
            for (var i = 0, max = bookmarks.length; i < max; i++) {
                let bm = bookmarks[i];
                tempTagService.addTags(bm.tags);
            }
            Utils.exec({ cmd : "tags.setAndSave", tags :  tempTagService.tags },
             (res , error) => {
                if(error) {
                    ModalHandler.showErr(error);
                    return;
                } else {
                    console.log("Persisted ", tempTagService.tags);
                    if(cb) {
                        cb ( tempTagService.tags );
                    }
                }
             });
        });
    },
    init : function(){     
        this.load();
      
        document.querySelector("#checkSync").addEventListener("click", (ev) => {
            ev.preventDefault();
            Utils.exec({ "cmd" : "sync.check" }, (res, error) => {
                // TODO
                if(error){            
                    ModalHandler.showErr(error);                
                } else {   
                    console.log(res);
                    
               
                    let html = '<p>';                    
                    if(res.conflict) {
                        html += '<span class="label label-danger">Conflict: Remote has changed and local has changed since last upload!</span>';
                    } else if(res.upload) {
                        html += '<span class="label label-warning">You have not saved changes. Upload your bookmarks now.</span>';
                    } else if(res.download) {
                        let date = res.remote.toLocaleString();
                        html += `<span class="label label-warning">Remote has changes (${date}). Download to be up to date!.</span>`;
                    } else if(res) {
                        html += '<span class="label label-success">No changes detected.</span>';
                    }
                    html += "</p>";
                    $('#syncCheckResult').html(html);
                    
                    if(res && res['remote']) {
                        $("[data-setting='remote']").text(this.toHumanDate(res['remote']));  
                    }
                }
                this.load();
            });
        });

        document.querySelector("#downloadBookmarks").addEventListener("click",  (ev) => {
            ev.preventDefault();
            this.download();
        });
        document.querySelector("#uploadBookmarks").addEventListener("click", (ev) => {
            ev.preventDefault();
            this.upload();
        });
        
        $('#clearAutoSyncMsgs').on('click', ()=>{
            Utils.pExec({
                cmd : 'sync.clearAutoSyncMessages'
            }).then(( res ) => this.load());
        });
        
        $('#reload').on('click', ()=>{
            this.load();
        });
    }
};

export default function main() {
    // Load
    try {
        SyncPage.init();
        var menu = new BMMenu();
        menu.build();
    } catch (e) {
        console.error(e);
        ModalHandler.showEx(e);
    }
};