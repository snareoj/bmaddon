/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * ==============================
 * Template for a simple Bookmark.
 * ==============================
 */
import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'

export default function BookmarkTemplate(_bm) {
    
    if(typeof _bm !== "object") {
        throw "Please provide an bookmark object!";
    }
    
    this.bookmark = _bm;
    this.actions = [];
    this.tags = [];    
    this.hints = [];
    this.viewClass = "big";
    this.mediaBody = `<h4 data-id="title" class="media-heading">Media heading</h4>   
                      <p class="description" data-id="description"></p>   
                      <p class="url" data-id="url" class="small">
                      </p>   
                      <p class="tags">
                        <span class="tagList"></span>
                      </p> 
          
                      <p class="dates">  
                        
                        <span class="label label-primary">
                            <span class="glyphicon glyphicon glyphicon-globe" aria-hidden="true"></span> 
                            <span data-id="host">Host</span>
                        </span>        
                        
                        <span class="label label-primary">
                            <span class="glyphicon glyphicon glyphicon-heart-empty" aria-hidden="true"></span> 
                            <span data-id="count">0</span>
                        </span>
                                        
                        <span class="label label-primary">
                            <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> 
                            <span data-id="dateAdded"></span>
                        </span>
                        
                        <span class="label label-primary">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> 
                            <span data-id="lastChange"></span>
                        </span>
                      </p>  
        
                      <p class="btn-group btn-group-xs pull-right actions"></p>  
                      <div class="hints"></div>`;
        
    this.template = `            
            <div class="containerNODE" id="${this.bookmark.id}"
                data-role="bookmark">
                <!-- This node is thrown away when $(node).html()) is returned -->
                <div class="bookmark-item" >
                    <div class="bookmark-small">
                        <span id="title">Media heading</span> <br/>
                        <a data-id="url">my URL</a><br/>
                        <div class="hints"></div>
                    </div>
                    <div class="media bookmark-big">
                        <div class="media-left">
                            <a href="#">
                                <img data-id="previewImg" class="media-object previewImg" alt="..." />
                            </a>                            
                        </div>
                        <div class="media-body">
                            ${this.mediaBody} 
                        </div>                              
                    </div>    
                </div>
            </div>
    `;
    
    this._render();
    
};


/**
 * Prepares default things which should always be available.
 * @returns {undefined}
 */
BookmarkTemplate.prototype._prepare = function( ) {
    this.tmpl = $(this.template);
    
    this.tmpl.find("[data-id='dateAdded']").html(Utils.dateToString(this.bookmark.dateAdded));    
    this.tmpl.find("[data-id='lastChange']").html(Utils.dateToString(this.bookmark.lastChange));    
    var pInfo = this.bookmark.info;    
    this.tmpl.find("[data-id='count']").html(Utils.numberToString(this.bookmark.count)); 
    if(pInfo) {
        this.tmpl.find("[data-id='url']").attr('href', pInfo.url);
        this.tmpl.find("[data-id='url']").html(pInfo.url);    
        this.tmpl.find("[data-id='title']").html(pInfo.title);
        this.tmpl.find("[data-id='host']").html(pInfo.host);       
    }
};

/**
 * Inserts MetaData. 
 * TODO add Tabs for each type of meta.
 * @returns {undefined}
 */
BookmarkTemplate.prototype._insertPreviewImage = function() {
    var pInfo = this.bookmark.info;
    if(!pInfo) {
        return;
    }
    var imgSrc = null;
    if (pInfo.meta) {
        var meta = pInfo.meta; 
        if (meta.ogp) {
            var ogp = meta.ogp;
            if (ogp.props["og:image"]) {
                imgSrc = ogp.props["og:image"];
            }
        }
        if(!imgSrc && meta.twitter) {
             if (meta.twitter.props["twitter:image"]) {
                imgSrc = meta.twitter.props["twitter:image"];
            }
        }
        if(!imgSrc && meta['default']) {            
             if (meta['default'].props["favicon"]) {
                imgSrc = meta['default'].props["favicon"];
            }            
        }
    } else {
        // No Meta
    }    
    if(!imgSrc || imgSrc.trim() === "") {
        // chrome-extension://__MSG_@@extension_id__
        imgSrc = "/icons/img_noimage.png";
    } else {
        var realHost = pInfo.url;
        if(!realHost.startsWith("http://") && !realHost.startsWith("https://")){
            realHost = "http://" + realHost;
        }
        var realURL = new URL(imgSrc, realHost );
        if(realURL) {                       
            imgSrc = realURL;
        }
    }        
    this.tmpl.find("[data-id='previewImg']").attr('src', imgSrc);
};

/**
 * Inserts MetaData. 
 * TODO add Tabs for each type of meta.
 * @returns {undefined}
 */
BookmarkTemplate.prototype._insertMeta = function() {
    var pInfo = this.bookmark.info;
    if (pInfo && pInfo.meta) {
        var meta = pInfo.meta; 
        var desc;
        if (meta['default'] && meta['default']["description"]) {
            desc =  meta['default']["description"];
        }
        if (!desc && meta.ogp) {
            var ogp = meta.ogp;
            if (ogp.props["og:description"] && ogp.props["og:description"].length > 0) {
                desc = ogp.props["og:description"];
            }
        }
        
        this.tmpl.find("[data-id='description']").html(desc);
    } else {
        // No Meta
    }
};

BookmarkTemplate.prototype._render = function () {   
    this._prepare(); 
    this._insertMeta();
    this._insertPreviewImage();
    this.applyView();
};

/**
 * Rerenders the template and replaces existing one with new one
 * @returns {undefined}
 */
BookmarkTemplate.prototype.update = function () {   
    var old = this.tmpl;    
    this._render();
    this.renderActions();
    this.renderTags();
    this.renderHints();
    this.applyView();
    old.replaceWith(this.tmpl);
};

/**
 * Add an action
 * Will be appended to "#action" element ( a bootstrap button grp)
 * @param {object} obj 
 */ 
BookmarkTemplate.prototype.addAction = function( obj ) {
    this.actions.push(obj);
};
BookmarkTemplate.prototype.renderActions = function() {
    for (var i = 0, max = this.actions.length; i < max; i++) {        
        this.tmpl.find(".actions").append(this.actions[i]); 
    }
};

BookmarkTemplate.prototype.renderTags = function( ) {
    if(this.bookmark.tags) {
        var tags = this.bookmark.tags;
        var tagList = this.tmpl.find(".tags .tagList");
        tagList.remove('.tag');
        for (var i = 0, max = tags.length; i < max; i++) {        
             tagList.append('<span class="tag label label-success" data-tag="'+tags[i]+'">'+tags[i]+'</span>');
        }
    }
};

/**
 * Add an hint
 * @param {String} obj 
 */ 
BookmarkTemplate.prototype.addHint = function( obj ) {
    this.hints.push(obj);
};

BookmarkTemplate.prototype.renderHints = function() {
    for (var i = 0, max = this.hints.length; i < max; i++) {        
        this.tmpl.find(".hints").append(this.hints[i]); 
    }
};

/**
 * Inserts everything into the template and returns the JQuery Node.
 * If it would be the HTML only, the eventListeners added with jquery do not work.
 * So add the JQUery Node to "DOM" as callee.
 * @returns {$|_$} tmpl The Template in a jquery object.
 */
BookmarkTemplate.prototype.asHTML = function () {  
    this.renderActions();
    this.renderTags();
    this.renderHints();
    this.applyView();
    return this.tmpl;
};

/**
 * Prepares default things which should always be available.
 * @returns {undefined}
 */
BookmarkTemplate.prototype.applyView = function( ) {          
    /*if(this.viewClass === "small"){
        $("#"+this.id +" .bmBig").hide();
        $("#"+this.id +" .bmSmall").show();        
    } else {          
        $("#"+this.id +" .bmSmall").hide();
        $("#"+this.id +" .bmBig").show();
    } */
};