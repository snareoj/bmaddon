
Bookmark Addon
==============

Bookmarkaddon for Firefox / Chrome.

See WIKI for more Detaiks: https://gitlab.com/snareoj/bmaddon/wikis/home

# How To Build #

- install NPM / Yarn
- globally install gulp-cli
- checkout project
- in project folder, open terminal and run yarn / npm to fetch node dependencies
- run "build" gulp task to build for firefox and chrome.

