/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import md5 from 'blueimp-md5';

var Utils = {
    
    get : function(){
        try {
            var itsMe;
            try {
                itsMe = browser;
            } catch (e) {};
            if(itsMe === undefined){
                itsMe = chrome;
            }
            return itsMe;
        } catch ( e ) {
            console.error("FUCK", e);
            throw e;
        }
    },
    
    /**
     * Returns t App set in the window object of the background page script
     * @param {function} callback 1st Parameter is the App (See backgroudscript.js)
     */
    getApp : function ( callback ) {
       throw "Unsupported Operation; Use exec / cmd structure";
    },
    
    /**
     * Execute Command with promise
     * @param {type} cmd
     * @returns {Promise}
     */
    pExec : function(cmd) {
    return new Promise((resolve, reject) => {
                Utils.exec(cmd, function(res, e){
                    if(e) {
                        reject(e);
                        return;
                    } else {
                        resolve(res);
                    }
                }
            );  
        });
    },
    
    /**
     * Execute a command in context of current TAB as Sender.
     * @param {object} cmd The Command 
     * @param {Function} callback Callback
     */
    exec : function(cmd, callback) {
      /*  Utils.get().tabs.getCurrent( ( tab ) => {
            Utils.getApp( ( app ) => {
                
                if(!app) {
                    console.error("No app found");
                    throw "No App Found";
                }
                
                app.handleCmd( 
                        cmd,
                        { tab : tab },
                        callback                            
                );
            });            
        }); */
        this._sendCmd(cmd, callback);
    },
    _sendCmd : function (cmd, callback) { 
        var obj = JSON.stringify(cmd);
        this.get().runtime.sendMessage(obj, function(res, error) {
            //console.log("res", res);
            if(typeof callback === "function") {
                try {
                    if(res) {
                        callback(res.res, res.error);
                    } else {
                        callback();
                    }
                } catch ( e ) {
                    console.error("Cant handle return of msg cause callback threw an exception: " , e);
                }
            } else if(res && (res.res || res.error) ) {
                console.warn("No callback for result", res);
            }
        });
    },
    
      // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    // https://davidwalsh.name/javascript-debounce-function
     debounce : function(func, wait, immediate) {
            var timeout;
            return function() {
                    var context = this, args = arguments;
                    var later = function() {
                            timeout = null;
                            if (!immediate) func.apply(context, args);
                    };
                    var callNow = immediate && !timeout;
                    clearTimeout(timeout);
                    timeout = setTimeout(later, wait);
                    if (callNow) func.apply(context, args);
            };
    },
    
    parseHash(){            
        var hash = window.location.hash.substr(1);
        var result = hash.split('&').reduce(function (result, item) {
            var parts = item.split('=');
            result[parts[0]] = parts[1];
            return result;
        }, {});
        return result;
    },
    
    dateToString : function(d) {
        var str = "-";
        if(typeof d === "date") {
            str = d.toLocaleString();
        }  else if(typeof d === "number") {
            str = new Date(d).toLocaleString();
        } else if(typeof d === "string" && d.length > 0) {
            str = new Date(d).toLocaleString();
        } 
        return str;
    },

    numberToString : function ( num ) {
        if(typeof num !== "undefined" ){
            return num.toString();
        } else {
            return "0";
        }
    },

    /**
     * Generates the md5 hash for the string.
     * @param {string} str
     * @returns {string} hash MD5 hash
     */
    genHash : function( str ) {
       if(!str || typeof str !== "string") {
           throw "No parameter for genHash!";
       }
      var hash = md5(str);
      return hash;
    },

    /**
     * Generates the id for that URL
     * @param {string} url
     * @returns {string} id
     */
    urlToBookmarkId : function( url ) {
       if(!url || typeof url !== "string") {
           throw "No parameter for urlToBookmarkId!";
       }
      return Utils.genHash( url);
    },    
    
    _i18n : function ( id ) {
        return this.i18nMsg( id, '!' + id+'!');
    },
    
    i18nMsg ( key, def, params) {
        
        if(!key) {
            throw new Error("Got no key");
        }
        
        if(key.indexOf('.')>= 0) {
            throw new Error("i18n: Key of message must not contain a dot: '"+key+"'");
        }            
        let i18nParams = this.i18nParams(params);
        let title = this.get().i18n.getMessage(key, i18nParams);  
        if(this.i18nisInvalid(title)) {
            // Dont set the key into ICON urls.
            if(key && key.indexOf('_iconUrl') >= 0 ) {
                title = def;
            } else {
                title = "['"+key+"'] " + def;
            }
        }
        return title;
    },
    
        
    i18nisInvalid( val ) {
        return !(val !== '' && val !== '??' && typeof val !== 'undefined');
    },
     
    i18nParams(msgParams) {
        if(msgParams && msgParams.length) {
            let p = [];
            for(let key of msgParams){
                if(typeof key === 'string' && key.indexOf("i18n:") === 0) {
                    let k = key.substring("i18n:".length);
                    p.push(this.i18nMsg(k, k));                        
                } else {
                    p.push(key);
                }
            }
            return p;
        }
        return undefined;
    }
      
};

export default Utils;