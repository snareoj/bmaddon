/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */


import Client from 'content_scripts/Client.js';
import BookmarkMenu from 'content_scripts/BookmarkMenu.js';
import MainMenu from 'content_scripts/MainMenu.js';
import * as Constants from 'content_scripts/Constants.js';
import TagsMenu from 'content_scripts/TagsMenu.js';

(function (global) {
    
const clsPref = 'snares-io-';
    var client = new Client();
    // var bMenu = new BookmarkMenu(c);
    var bMenu = new MainMenu();
    var prefix = Constants.prefix;
    var clIcon = Constants.clIcon;        

    function crContent(id, menuIcon, desc){
        let content =  `<span class="${clsPref}btn ${clsPref}btn-round"><i class="material-icons ${clsPref}menu-icon">${menuIcon}</i><span class="${clsPref}desc" data-i18n="${id}">${desc}</span></span>`;
        return content;  
    };
    
    function setBmed( bm ){
        
        if(bm) {
            bMenu.toggleMenuClass("bookmarked", true);
            bMenu.toggleMenuClass("notbookmarked", false);
            bMenu.disable('addBookmark', true);
            bMenu.disable('editBookmark', false);
            bMenu.disable('editTags', false);
            bMenu.disable('removeBookmark', false);
        } else {            
            bMenu.toggleMenuClass("bookmarked", false);
            bMenu.toggleMenuClass("notbookmarked", true);
            bMenu.disable('addBookmark', false);
            bMenu.disable('editBookmark', true);
            bMenu.disable('editTags', true);
            bMenu.disable('removeBookmark', true);                
        }
    };
    
    function showTagMenu (){        
        let tagMenu = new TagsMenu( client );            
        tagMenu.showTags( client.getPageBookmark().tags );
    }
    
    bMenu.addButtonObj(
    {       
        name : 'addBookmark',
        listener : ( event ) => {
            event.preventDefault();
            client.addBookmark()
             .then( (res ) => {                             
                 setBmed (res);
              //   this.tagMenu.showTags();
                showTagMenu();
             }).catch( error => {
                alert("Error see console."); 
                console.error("Error Bookmark", error);
                setBmed ( null );
             });  
        },
        build : ( parent ) => {
            let content =  crContent('addBookmark', 'star', 'Add bookmark' );
            parent.innerHTML = content;
        }
    }      
    );

    bMenu.addButtonObj(
    {
        name : 'editBookmark',
        listener : ( event ) => {
            event.preventDefault();
            client.editBookmark();
        },
        build : ( parent ) => {              
            let content =  crContent('editBookmark', 'edit', 'Edit bookmark' );
            parent.innerHTML = content;
        }
    });
    bMenu.addButtonObj(
    {
        name : 'editTags',
        listener : ( event ) => {
            event.preventDefault();
            showTagMenu();
        },
        build : ( parent ) => {
            let content =  crContent('editTags', 'label', 'Edit <br/> tags' );
            parent.innerHTML = content;
        }
    });           
    bMenu.addButtonObj(
    {
        name : 'removeBookmark',
        listener : ( event ) => {
            event.preventDefault();
            client.deleteBookmark()
               .then( (res ) => {                             
                  setBmed (res);
               }).catch( error => {
                  alert("Error see console."); 
                   console.error("Error Bookmark", error);
                   setBmed ( false );
               });                  
        },
        build : ( parent ) => {
            let content =  crContent('removeBookmark', 'delete_forever', 'Remove bookmark');
            parent.innerHTML = content;
        }
    });             
    bMenu.addButtonObj(
    {
        name : 'gotoBookmarks',          
        listener : ( event ) => {
            event.preventDefault();
            client.listBookmarks();                
        },
        build : ( parent ) => {                
            let content =  crContent('gotoBookmarks', 'book', 'Open bookmarks' );
            parent.innerHTML = content;
        }
    });      
/*
    bMenu.addButtonObj(
    {
        name : 'openTopsites',         
        listener : ( event ) => {
            event.preventDefault();
            client.openTopsites();                
        },
        build : ( parent ) => {                
            let content =  crContent('openTopsites', 'favorite_border', 'Open topsites' );
            parent.innerHTML = content;
        }
    });   */
      bMenu.addButtonObj(
    {
        name : 'openHome',         
        listener : ( event ) => {
            event.preventDefault();
            client.openHome();                
        },
        build : ( parent ) => {                
            let content =  crContent('openHome', 'home', 'Open Home' );
            parent.innerHTML = content;
        }
    });

    bMenu.addButtonObj(
    {
        name : 'historyBack',         
        listener : ( event ) => {
            event.preventDefault();
            window.history.back();
        },
        build : ( parent ) => {                
            let content =  crContent('historyBack', 'chevron_left', 'History  <br/>back' );
            parent.innerHTML = content;
        }
    });            

    bMenu.addButtonObj(
    {
        name : 'historyForward',         
        listener : ( event ) => {
            event.preventDefault();
            window.history.forward();           
        },
        build : ( parent ) => {                
            let content =  crContent('historyForward', 'chevron_right', 'History  <br/>forward' );
            parent.innerHTML = content;
        }
    });    
    bMenu.addButtonObj(
    {
        name : 'newTab',         
        listener : ( event ) => {
            event.preventDefault();
            client.listBookmarks();                
        },
        build : ( parent ) => {                
            let content =  crContent('newTab', 'open_in_new', 'Open <br/>new Tab' );
            parent.innerHTML = content;
        }
    });  
      
    bMenu.addButtonObj(
    {
        name : 'closeTab',         
        listener : ( event ) => {
            event.preventDefault();
            client.listBookmarks();                
        },
        build : ( parent ) => {                
            let content =  crContent('closeTab', 'close', 'Close<br/>Tab' );
            parent.innerHTML = content;
        }
    });  
    
    bMenu.addButtonObj(
    {
        name : 'prevPage',         
        listener : ( event ) => {
            event.preventDefault();
            client.listBookmarks();                
        },
        build : ( parent ) => {                
            let content =  crContent('prevPage', 'skip_previous', 'Previous<br/>Page' );
            parent.innerHTML = content;
        }
    });  
      
    bMenu.addButtonObj(
    {
        name : 'nextPage',         
        listener : ( event ) => {
            event.preventDefault();
            client.listBookmarks();                
        },
        build : ( parent ) => {                
            let content =  crContent('nextPage', 'skip_next', 'Next<br/>Page' );
            parent.innerHTML = content;
        }
    });  
            
    bMenu.init();
    bMenu.disable('newTab', true);
    bMenu.disable('closeTab', true);
    bMenu.disable('prevPage', true);
    bMenu.disable('nextPage', true);

    // Initial State of menu
    client.getBookmark()
          .then(bm => {
              client.setBookmarked(bm);
              setBmed(bm);
          }).catch(e => console.error("Error while getting Bookmark", e));
      
})(window.document);