/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import EventBus from 'background/EventBus.js'

export default function BackgroundListener(eventBus) {
    this.eventBus = eventBus;
    this.sessions  =  {};
}

BackgroundListener.prototype.attach = function(){
    // Listen to other addon parts
    this.attachOnMessageListener();
    this.attachTabListener();
    this.sessions = {};
    this.eventBus.addCmdHandler(this);
};
    
BackgroundListener.prototype.attachOnMessageListener = function(){
    // Listen to other addon parts
    Utils.get().runtime.onMessage.addListener((cmd, sender, sendResponse)=> {
        console.log("Message from other parts: " , cmd, sender);
        try {
            var cmdMessage = JSON.parse(cmd);
            this.eventBus.handleCmd(cmdMessage, sender, (result, error) => {
                sendResponse(
                        {
                            'msgid': cmdMessage.msgid,
                            'res': result,
                            'error': this.toResponseError(error)
                        });
            });
        } catch (e) {
            console.error("Exception ", e);
            sendResponse(
                        {
                            'msgid': cmdMessage.msgid,
                            'res': undefined,
                            'error': e
                        });
            throw e;
        }
        return true;
    });
};
BackgroundListener.prototype.attachTabListener = function(){
    // Listen to other addon parts
    Utils.get().tabs.onCreated.addListener( ( tab ) => this.onTabCreated ( tab ));
    Utils.get().tabs.onRemoved.addListener( ( tab ) => this.onTabRemoved ( tab ));

};

BackgroundListener.prototype.onTabCreated = function( tab ){
    console.log("Tab Created", tab);
}

BackgroundListener.prototype.onTabRemoved = function( tab ){
    console.log("Tab Removed", tab);
    if(this.sessions[tab.id]) {
        delete this.sessions[tab.id];
    }
}

BackgroundListener.prototype.toResponseError = function(error) {
    if(!error) {
        return undefined;
    } else if(error instanceof TypeError) {
        return {
            message : error.message,
            stack : error.stack
        };
    } else if(error instanceof Error) {
        return {
            message : error.message,
            stack : error.stack
        };
    } else {
        return {
            message : JSON.stringify(error)
        };
    }
};
 
/**
 * Handles navigation events
 * @param {cmd} cmdMessage 
 * @param {object} sender
 * @param {Function} callback
 * @return {boolean} true if cmd was handled
 */
BackgroundListener.prototype.handle = function (cmdMessage, sender, callback) {
    if ( cmdMessage.cmd === 'background.session.set') {
        this._clientSessionSET(cmdMessage.sessionId, cmdMessage.key, cmdMessage.value, callback);
        return true;
    } else if ( cmdMessage.cmd === 'background.session.get') {
        this._clientSessionGET(cmdMessage.sessionId, cmdMessage.key, callback);
        return true;
    }
    return false;
};

/**
 * This is actually called from CLIENT
 * @param {cmdMessage} cmdMessage
 * @param {object} sender
 * @param {type} callback
 * @returns {void}
 */
BackgroundListener.prototype._clientSessionGET = function (id, key, callback) {    
    if(typeof this.sessions[id] === 'undefined')  {
        callback(undefined, undefined);
        return;
    }
    callback(this.sessions[id][key]);
}

/**
 * This is actually called from CLIENT
 * @param {cmdMessage} cmdMessage
 * @param {object} sender
 * @param {type} callback
 * @returns {void}
 */
BackgroundListener.prototype._clientSessionSET = function (id, key, value, callback) {    
    if(!this.sessions[id])  {
        this.sessions[id] = {};
    }
    if(!key) {
        throw new Error("No Key supplied");
    }
    this.sessions[id][key] = value;
    callback( this.sessions[id][key] );
}