import BookmarkPersistenceLocalStorage from 'background/BookmarkPersistenceLocalStorage.js'
import sinonChrome from 'sinon-chrome'; // from 'sinon-chrome'
import sinon from 'sinon'; // from 'sinon-chrome'

import mock from '/spec/store.mock.js'; 

/* 
 * Spec for Background
 */
describe("Bookmark Persistence Spec", function() {   
    
    window.chrome = sinonChrome.chrome;
        
    function stubBMPers( bmStore ){
        sinon.stub(bmStore.store, "_getStorage", function () { return mock; });   
    }
    
  it("saves objects in a prefix at storage", function( done ) {
    
        let storage =  new BookmarkPersistenceLocalStorage();
        stubBMPers(storage);
        storage.createAndPersistBookmark({
            url : 'test'
        }, (res, error) => {
            
            expect(
               res
            ).toBeDefined();

           done();
        });
  });
});
