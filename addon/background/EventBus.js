/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global Utils */

// background-script.js
"use strict";

export default function EventBus() {
    this.cmdHandler = [];
};

/**
 * Add a cmd handler
 * @param {type} handler
 * @returns {undefined}
 */
EventBus.prototype.addCmdHandler = function (handler) {
    this.cmdHandler.push(handler);
};

/**
 * Handle the cmd 
 * @param {type} cmd
 * @param {sender} sender
 * @param {type} callback
 * @returns {undefined}
 */
EventBus.prototype.handleCmd = function (cmd, sender, callback, supressError) {
    try {
        if (!cmd) {
            console.warn("Cant parse, missing smth: ", cmd);
            return;
        }
        var handled = false;

        for (var i = 0, max = this.cmdHandler.length; i < max; i++) {
            try {
                handled |= this.cmdHandler[i].handle(cmd, sender, callback);
            } catch (e) {
                console.error(e);
                if (callback) {
                    callback(undefined, e);
                }
                throw e;
            }
        }

        if (!handled && (!supressError || typeof supressError === 'undefined' )) {
            if (callback) {
                callback(undefined, new Error("Unknown Message: " + JSON.stringify(cmd)));
            }
            console.warn("Unknown Message: ", cmd);
        }
    } catch ( e ) {
        console.error(e);
        throw e;
    }
};