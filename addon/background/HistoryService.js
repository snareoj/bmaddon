/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * Listens to 
 * https://developer.mozilla.org/en-US/Add-ons/WebExtensions/API/webNavigation/onHistoryStateUpdated
 * 
 * Saves history items
 * 
 */
import Utils from 'lib/Utils.js';
import Storage from 'lib/Storage.js';

const MAX_TOP_SITES_LENGTH = 20;

/**
 * History Service
 * - Rises the count of bookmarks if a history item is updated
 * - Saves its own history (History on Andorid not available)
 * - if default history available can count bookmarks
 * @param {Settings} settings 
 * @param {BookmarkPersistenceCmdHandler} bmPersitence
 */
export default function HistoryService ( settings, bmPersitence ) {
    this.settings = settings;
    this.bmPersitence = bmPersitence;
    this.historyBucketStore =  new Storage("historyItems");    
    this.pageHitsStore =  new Storage("pageHits");
    this.topSitesStore =  new Storage("topSites");
};

/**
 * Init History Service
 * @returns {void}
 */
HistoryService.prototype.init = function(){   
    // We dont want to filter the events
    //let filter = undefined;
    Utils.get().webNavigation.onHistoryStateUpdated.addListener(( e )=> {
        console.log("HistoryStateUpdated ", e);
        this.onPageHit(e.url);
    }); //, filter);
};

HistoryService.prototype.onPageHit  = function( url ) {    
    this._logPageHit(url, 1, (res, error) => {
        if(error) {
            console.error("Cant log page hit ", error);
        }
    });
    this._logHistory(url);
    this._countBookmark( url );
};

HistoryService.prototype._logPageHit  = function( url, count, callback ) {    
    let id = Utils.urlToBookmarkId(url);
    this.pageHitsStore.getItemById( id, ( counter, error ) => {
        if(error) {
            console.log("Cant count pagehit ", error);
            return;
        }
        counter = this._doCount(id, counter, url, count);
        this.renewTopSites(url, counter.hits, ( res , error ) => {
            this.pageHitsStore.updateItem( counter, callback );
        });
    });    
};

HistoryService.prototype._doCount  = function( id, counter, url, count ) {    
    if(!counter) {
        counter =  { 
            id: id,
            hits : count 
        };
    } else {
        counter.hits = counter.hits + count;
    } 
    return counter;
};
/**
 * Get the topSites sorted by Hits
 * @param {type} callback
 * @returns {undefined}
 */
HistoryService.prototype.getTopSites = function( callback ) {
    this.topSitesStore.getItemById('topsites', ( topsites, error) => {
        if(error) {
            callback(undefined, error);
        } else {
            if(topsites && topsites.sites) {
                this._getBookmarkRecurse(0, topsites, callback);
            } else {
                // NO topsites
                callback({
                    sites: []
                });
            }
        }
    });
};


/**
 * 
 * @param {type} index
 * @param {type} topsites
 * @param {type} callback
 * @returns {undefined}
 */
HistoryService.prototype._getBookmarkRecurse = function( index, topsites, callback ) {
   if(index < topsites.sites.length){
        let curSite = topsites.sites[index];
        this.bmPersitence.getBookmarkByURL(
                curSite.url,
                ( bookmark , error  ) => {
                    if(error) {
                        console.warn("Error on history state update", error);
                    } else if(bookmark){
                       curSite.bookmark = bookmark; 
                    }
                    this._getBookmarkRecurse(index + 1, topsites, callback);
                });
   } else {
       callback(topsites);
   }
};

/**
 * Reset the topSites
 * @param {type} callback
 * @returns {undefined}
 */
HistoryService.prototype.resetTopSites = function( callback ) {
    this.topSitesStore.updateItem({ id : 'topsites', sites: [] }, callback);
};

function topsitesSort(a, b) {
    return b.hits - a.hits;
}

/**
 * Update Topsites with that url
 * @param {type} url
 * @param {type} hits
 * @param {function} callback
 * @returns {undefined}
 */
HistoryService.prototype.renewTopSites  = function( url, hits, callback ) {    
    this.topSitesStore.getItemById('topsites', ( topsites, error ) => {
        if( error ) {
            callback(null, error);
            return;
        }
        if(!topsites) {
            topsites = { id : 'topsites', sites: [] };
        }
        this._updateTopsites(topsites, url, hits);
        this.topSitesStore.updateItem(topsites, ( res, error) => {
            if(!error) {
                callback(topsites, error);
                console.log("Updated Topsites List:", topsites);
            } else {                
                callback(res, error);
            }
        });
    });
}; 

HistoryService.prototype._updateTopsites  = function( topsites, url, hits ) {    
    let has = topsites.sites.find( item => {
        return item.url === url;
    });
    if(has) {
        has.hits = hits;
    } else {
        topsites.sites.push( {
            url : url,
            hits : hits
        });        
    }
    topsites.sites.sort( topsitesSort );
    if(topsites.sites.length > MAX_TOP_SITES_LENGTH) {
        topsites.sites = topsites.sites.slice(0, MAX_TOP_SITES_LENGTH);
    }
}; 

/**
 * Update the history
 * @param {string} url
 */
HistoryService.prototype._logHistory  = function( url ) {          
    let historyItem = {
        url : url,
        dateAdded : new Date().toISOString()
    };        
    this._insertHistoryItem(historyItem);    
};

/**
 * Inserts an history item into the day bucket
 * @param {type} item
 * @returns {undefined}
 */
HistoryService.prototype._insertHistoryItem  = function( item ) {      
    
    this.getHistoryItemBucketForDate( item.dateAdded, ( bucket , error) => {        
        if(error) {
            console.log("Cant insertHistoryItem" , error);
            return;
        }
        bucket.items.push( item );        
        this.historyBucketStore.updateItem( bucket, (res, error) => {
            if(res) {
                console.log('Added history item', item);
            } else if(error) {
                console.log("Cant insertHistoryItem" , error);
            }
        });        
    });    
};

/**
 * Get the bucket for that date
 * @param {string} dateISOString in ISO STRING FORM
 * @param {type} callback
 * @returns {undefined}
 */
HistoryService.prototype.getHistoryItemBucketForDate  = function( dateISOString , callback ) {      
    if(!dateISOString) {
        throw new Error("You have to supply a date ISO string!");
    }
    let num = Date.parse(dateISOString);
    let date = new Date(num);        
    // Store it per Day
    let bucketID = date.getYear()+":" + date.getMonth()+":"+date.getDate();    
    this.historyBucketStore.getItemById(bucketID, ( res, error) => {
        if(!res) {
            callback( {
                id : bucketID,
                items : []
            }, error);
        } else {
            callback(res, error);
        }
    });    
};

/**
 * Clears history
 * @param {type} callback
 * @returns {undefined}
 */
HistoryService.prototype.deleteHistory = function( callback )  { 
    this.historyBucketStore.clearItems(( res, error )=> {
        if(error) {
            callback(null, error);
        } else {
            // TODO Should the page hit log cleared too?
            callback(res, error);
        }
    });
};


/**
 * Raises the count in an bookmark
 * @param {type} url
 * @returns {undefined}
 */
HistoryService.prototype._countBookmark = function( url )  {    
    if(!this.bmPersitence) {
        console.warn("No Bookmark persistence in history, cant lookup");
        return;
    }  
    this.bmPersitence.getBookmarkByURL(
            url,
            ( bookmark , error  ) => {
                if(error) {
                    console.warn("Error on history state update", error);
                    return;
                } else if(bookmark){
                    bookmark.count ++;
                    this.bmPersitence.updateBookmark( bookmark, (res, error) => {       
                        if(error) {
                            console.warn("Error on bookmark update", error);
                            return;
                        }        
                    });
                }
            }
    );
};

/**
 * Clears pageHitStore, Goes through bookmarks and rebuilds topsites
 * @param {type} callback
 * @returns {undefined}
 */
HistoryService.prototype._rebuildTopsites =  function( callback ) {    
    this.pageHitsStore.clearItems((res, error ) => {   
        if(error) {callback(null, error); return;}    
        this.resetTopSites( ( reset, error ) => {     
            if(error) {callback(null, error); return;}
            this.bmPersitence.listBookmarks( ( list, error )=> {
                if(error) {callback(null, error); return;}
                let topsites = { id : 'topsites', sites: [] };
                let pageHits = [];
                this._insertBookmarkCount(0, list, topsites, pageHits, ( res )=> {
                    if(res) {
                        this.topSitesStore.updateItem(topsites, (res, error) => {
                            if(error) {callback(null, error); return;}                                
                            this.pageHitsStore.bulkUpdateItems(pageHits, callback);
                         });
                     } else {
                         callback(undefined, undefined);
                     };
                });
            });
        });
    }) ;
};

/**
 * Recusrive Function to add bookmark count to topsites 
 * @param {type} index
 * @param {type} list
 * @param {topsites} topsites
 * @param {topsites} pageHits
 * @param {type} callback
 * @returns {undefined}
 */
HistoryService.prototype._insertBookmarkCount =  function( index, list, topsites, pageHits, callback ) {
    if(index < list.length) {
        let bm = list[index];
        if(bm && bm.info && bm.count) {
            let id = Utils.urlToBookmarkId(bm.info.url);
            let c = this._doCount(id, undefined, bm.info.url, bm.count);
            pageHits.push(c);
            this._updateTopsites(topsites, bm.info.url, bm.count);        
        }
        this._insertBookmarkCount(index + 1, list, topsites, pageHits, callback);            
    } else {
        callback( true );
    }
};

/**
 * Handles fopsites and client get bookmarks
 * @param {type} cmd
 * @param {type} sender
 * @param {type} callback
 * @returns {Boolean}
 */
HistoryService.prototype.handle =  function ( cmd, sender, callback) {
    if(cmd.cmd === 'fetch.topsites') {
        this.getTopSites(  callback );
        return true;
    } else if(cmd.cmd === 'fetch.history') {
        this.getHistoryItemBucketForDate( cmd.date, callback);
        return true;
    } else if(cmd.cmd === 'history.delete') {
        this.deleteHistory( callback );
        return true;
    }else if(cmd.cmd === 'client.lookupBookmarkByURL') {
        this.bmPersitence.getBookmarkByURL( cmd.url, callback);
        this.onPageHit( cmd.url );
        return true;
    }else if(cmd.cmd === 'fetch.historyBuckets') {
        return true;
    } else if(cmd.cmd === 'history.rebuildHistoryIndex'){
        //this.
        return true;
    } else if(cmd.cmd === 'history.rebuildTopsitesIndex'){
        this._rebuildTopsites( callback );
        return true;
    } else if ( cmd.cmd === 'main.clearStorage') {        
        this.historyBucketStore.clearItems((res, error ) => {
           callback ( 'cleared history', error);  
        }) ;
        this.pageHitsStore.clearItems((res, error ) => {
           callback ( 'cleared pagehits', error);  
        }) ;
        this.topSitesStore.clearItems((res, error ) => {
            callback ( 'cleared topsites', error);  
        }) ;
        return true;
    }
    return false;
};