/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* global Utils */

// background-script.js
"use strict";

import Settings from 'background/Settings.js';
import Utils from 'lib/Utils.js';
import EventBus from 'background/EventBus.js';
import ClientListener from 'background/ClientListener.js';
import BackgroundListener from 'background/BackgroundListener.js';
import BookmarkPersistenceLocalStorage from 'background/BookmarkPersistenceLocalStorage.js';
import BookmarkPersistenceCmdHandler from 'background/BookmarkPersistenceCmdHandler.js';
import NavigationCmdHandler from 'background/NavigationCmdHandler.js';
import SyncService from 'background/SyncService.js';
import TagsService from 'background/TagsService.js';
import HistoryService from 'background/HistoryService.js';
import BookmarkAPIService from 'background/BookmarkAPIService.js';
import BackendNotification from 'background/BackendNotification.js';

export default function BackgroundScript() {
    try { 
        this.cmdHandler = [];
        this.eventBus = new EventBus();

        this.settings = new Settings();
        this.eventBus.addCmdHandler(this.settings);

        this.eventBus.addCmdHandler(new NavigationCmdHandler(this.settings));
        var tagsService = new TagsService();
        this.eventBus.addCmdHandler(tagsService);

        var persistence = new BookmarkPersistenceLocalStorage();

        this.persCmdHandler = new BookmarkPersistenceCmdHandler(this.settings, persistence, this.eventBus);
        this.eventBus.addCmdHandler(this.persCmdHandler);

        this.syncService = new SyncService(this.settings, this.eventBus);
        this.syncService.init();

        this.clientListener = new ClientListener(this.eventBus);
        this.clientListener.attach();

        this.backgroundListener = new BackgroundListener(this.eventBus);    
        this.backgroundListener.attach();

        this.historyService = new HistoryService(this.settings, this.persCmdHandler);
        this.historyService.init();
        this.eventBus.addCmdHandler(this.historyService);
        
        
        this.eventBus.addCmdHandler(new BackendNotification());

        let bmService = new BookmarkAPIService(this.settings, this.eventBus);
        bmService.attach();

        // Listen to Toolbar Button  
        // https://developer.mozilla.org/en-US/Add-ons/WebExtensions/user_interface/Browser_action
        Utils.get().browserAction.onClicked.addListener((event) => {
            Utils.get().tabs.create({
                url: "/pages/bookmark_list.html"
            });
        });
        
        
    } catch ( e ) {
        console.error("Failed to init Background script", e);
    }

};

BackgroundScript.prototype.start = function(startCb) {
    // Init Settings
    let SUPRESS_HANDLED_ERROR = true;
    this.settings.restoreOptions((props) => {
        
        // Notify that we started!
        this.eventBus.handleCmd({
           'cmd' : 'background.startup'
        }, null, ( res, error) => { 
            console.log("Backgroundscript initialized.");
            if (typeof startCb === 'function') {
                startCb(this);
            }
        }, SUPRESS_HANDLED_ERROR);
    });
};