/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'
import ModalHandler from 'lib/ModalHandler.js'
import Topsites from 'lib/topsites.js'
import BMMenu from 'lib/menu.js'

function listTopsites() {    
    // Topsites stuff
    var topsites = new Topsites();
    if(topsites.checkIsDefaultTopSites()) {
        topsites.fetchDefaultTopsites( ( sites, error)=> {
            render('defaultTopsites', {
                sites : sites
            }, error,
            function ( site) {            
                let tmpl = `
                    <span id="title" class="media-heading">${site.title}</span> <br/>
                    <a data-id="url" href="${site.url}" target="_blank">${site.url}</a><br/>
                `;            
                return tmpl;
            });
        });
    } else {
       // document.getElementById('defaultTopsitesH1').remove();
    }
    
    topsites.fetchTopsites( ( topsites, error ) => {
        render('customTopsites', topsites, error,
        function (site) {            
            let tmpl = `<span class="labels"><span class="label label-info">${site.hits}</span>`;
            if(site.bookmark) {
                tmpl += `<span class="label label-success bookmarkhint" data-bookmarkid="${site.bookmark.id}"><span class=" glyphicon glyphicon-bookmark" aria-hidden="true"></span></span>`;
            }
            tmpl += `</span><a data-id="url" href="${site.url}" target="_blank">${site.url}</a>`;            
            return tmpl;
        });
    });    
    
    $('#customTopsites').on('click', ( e )=> {
        if(e.target.getAttribute('data-bookmarkid')) {
            let bmId = e.target.getAttribute('data-bookmarkid');
            Utils.exec( {
                "cmd" : "nav.editBookmark",
                "id" : bmId
            }, (res, error) => {                                        
                if(error) {
                    ModalHandler.showErr(error);
                    return;
                }
            });
        }
    });
    
    // And the menu
    var menu = new BMMenu();
    menu.build();
    
}

function render(parentId, listOf, error, itemR) {
    var div = document.getElementById(parentId);
    if (!listOf.sites.length) {
        div.innerText = 'No sites returned from the topSites API.';
        return;
    } else if(error) {        
        ModalHandler.showErr(error);
        return;
    } else {
        renderList(div, listOf.sites, itemR);
    }
}

function renderList(div, sites, childR) {
    let panel = document.createElement('div');
    panel.className="panel panel-default";
    let panelBody = document.createElement('div');
    panelBody.className="panel-body";
    panel.appendChild(panelBody);
    let ul = document.createElement('ul');
    ul.className = 'list-group hitListCounter';
    for (let site of sites) {
        let li = document.createElement('li');
        li.className = 'list-group-item topsiteItem';
        li.innerHTML = childR(site);
        ul.appendChild(li);
    }
    panelBody.appendChild(ul);
    div.appendChild(panel);
}

export default function main(){
    /* document.querySelector("#btnOptions").addEventListener('click', ( ev) => {
        get().runtime.openOptionsPage();
    }); */
    try {
        listTopsites();
    } catch ( e ) {
        console.error("Error " , e);        
        ModalHandler.showEx(e);
    }
}; 

console.log("TEST");