import sinonChrome from 'sinon-chrome'; // from 'sinon-chrome'
import * as bg from '/background/BackgroundScript.js';

/* 
 * Spec for Background
 */
describe("Background TestSuite", function () {

    window.chrome = sinonChrome.chrome;
    
    // BG Script first gets Config from Storage.
    /*sinonChrome.chrome.storage.sync.get.resolves({
       settings : { 'hello' : 'world' } 
    }); */
    // NAVHandler calls update tabs
     sinonChrome.chrome.storage.sync.get.callsArgWith(1, {
       settings : { 'hello' : 'world' } 
    });
        
    // NAVHandler calls update tabs
    sinonChrome.chrome.tabs.update.callsArgWith(2, true);
    
    afterEach(function(){        
        // Reset after each test
       sinonChrome.chrome.storage.sync.get.resetHistory();
     //  sinonChrome.chrome.tabs.update.resetHistory();
    });
    
    it("Instantiates and loads Settings ", function ( done ) {                
        // Parameter is the startup caklkback
        var bgScript = new bg.BackgroundScript();
        bgScript.start(( script) => {            
            expect(script).toBeDefined();            
            expect(script.eventBus).toBeDefined();
            expect(sinonChrome.chrome.storage.sync.get.calledOnce).toBe(true);
            expect(script.eventBus.cmdHandler).toBeDefined();
            expect(script.eventBus.cmdHandler.length).toBeGreaterThan(0);
            expect(script.settings.props['hello']).toBe('world');  
            done();
        });
        
    });
    /*
    it("It handles nav cmd with tab ID ZERO", function ( done ) {                
        // Parameter is the startup caklkback
        var TABID = 0;
        var bgScript = new bg.BackgroundScript();
        bgScript.start(( script  ) => {   
            script.handleCmd(
            // CMD
            {
                cmd : 'nav.listBookmarks'
            }, 
            // sender.tab
            { 
                tab :  { id : TABID }
            }, 
            // CALLBACK
            function ( res , error) {            
                expect(error).not.toBeDefined();     
                expect(res).toBe(true);
                var calls = sinonChrome.chrome.tabs.update.getCalls(0);
                console.log(calls[0].args);
                expect(calls[0].args[0]).toBe(TABID); // The TAB ID
                expect(calls[0].args[1].url).toBe('/pages/bookmark_list.html'); // The url
                expect(calls[0].args[2]).toBeDefined(); // The callback
                done();
            });
        });
        // TEst
    }); */
});
