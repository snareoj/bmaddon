/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * Tool to read page information like metas.
 */

// embedded_menu.js
"use strict";
    
var PageInfo = {   
    

    readDefaultMeta : function(func, pageInfo) {
        var metaData = {
            name  : 'default',
            starts : '',
            count : 0,
            props : {}
        };
          // DefaultMeta
      /*  <!--

            <meta name="description" content="News und Foren zu Computer, IT, Wissenschaft, Medien und Politik. Preisvergleich von Hardware und Software sowie Downloads bei Heise Medien.">
                <meta name="keywords" content="heise online, c't, iX, Technology Review, Newsticker, Telepolis, Security, Netze">

        <meta name="publisher" content="Heise Medien" />
        <meta name="viewport" content="width=1175" />
        <link rel="author" title="Kontakt" href="mailto:kontakt%40heise.de?subject=heise%20online">
        <link rel="search" title="Suche" href="https://www.heise.de/suche/">
        <link rel="alternate" type="application/atom+xml" title="Aktuelle News von heise online" href="https://www.heise.de/newsticker/heise-atom.xml">
        <link rel="alternate" type="application/rss+xml" title="Aktuelle News von heise online (für ältere RSS-Reader)" href="https://www.heise.de/newsticker/heise.rdf">

        --> */
        this.defMetaContent(func("meta[name='description']"), metaData, 'description');
        this.defMetaContent(func("meta[name='publisher']"), metaData, 'publisher');
        this.defMetaContent(func("meta[name='keywords']"), metaData, 'keywords');
        this.defMetaContent(func("title"), metaData, 'title');

        //<link rel="shortcut icon" type="image/x-icon" href="/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico" id="favicon">
        var icon = func("link[rel='shortcut icon']");
        if(icon && icon.length){
             var content =  icon[0].getAttribute("href");
            if(content) {
                metaData.props['favicon'] = content;
                metaData.count++;
            }
        }
        if(metaData.count > 0){
            pageInfo.meta["default"] = metaData;
        }
    },

    defMeta : function(func, metaData, name){    
       var text = func("meta[name='"+name+"']");
       this.defMetaContent(text, metaData, name);
    },

    defMetaContent : function(text, metaData, name){    
        if(text && text.length > 0) {
            var content =  text[0].getAttribute("content");
            if(content) {
                metaData.props[name] = content;
                metaData.count++;
            }
        }
    },
    
// Twitter Card https://dev.twitter.com/cards/types/summary
    enrichMeta : function ( func, pageInfo, name, propName, startsWith, realName ){
        var openGraphNodes = func("meta["+propName+"^='"+startsWith+"']");
        if(openGraphNodes) {
            var metaData = {
                name  : realName,
                starts : startsWith,
                count : 0,
                props : {}
            };
            for (var meta of openGraphNodes) {
                var prop = meta.getAttribute(propName);
                var content =  meta.getAttribute("content");
                metaData.props[prop] = content;
                metaData.count ++;
            }
            if(metaData.count > 0) {
                pageInfo.meta[name] = metaData;
            }
        }
    },


    doAllMeta : function(func, pageInfo){

        // DefaultMeta
        this.readDefaultMeta(func, pageInfo);

        // Twitter Card https://dev.twitter.com/cards/types/summary
        this.enrichMeta(func, pageInfo, "twitter", "name", "twitter", "twitter:", "Twitter Card");
    // https://developers.facebook.com/docs/applinks
        this.enrichMeta(func, pageInfo, "applinks", "property", "al:", "Facebook ");
        // http://ogp.me/
       this.enrichMeta(func, pageInfo, "ogp", "property", "og:", "Open Graph Protocol");
        // http://www.metatags.org/dublin_core_metadata_element_set
        this.enrichMeta(func, pageInfo, "dublincore","name", "dc.", "Dublin Core Metadata");
        // http://www.metatags.org/dublin_core_metadata_element_set
        this.enrichMeta(func, pageInfo, "dublincore_caps","name", "DC.", "Dublin Core Metadata");

        // https://msdn.microsoft.com/de-de/library/gg491732(v=vs.85).aspx
        this.enrichMeta(func, pageInfo, "ms","name", "msapplication-", "Microsoft Pinned Site Metadata");

    },


// Coolects  page info like URL and Title, some META objects are read ( twitter, open graph, applinks )
    collectPageInfo : function ( win ) {

        if(!win) {
            throw "Please provide a window object!";
        }

        var pageInfo = {
            url : win.document.URL,
            host : win.location.host,
            title : win.document.title,
            meta : {}
        };    
       this.doAllMeta(function( query ) {
            return win.document.querySelectorAll(query);
        }, pageInfo);
        return pageInfo;
    },

    getLocation : function(href) {
        var l = document.createElement("a");
        l.href = href;
        return l;
    },

    // Coolects  page info like URL and Title, some META objects are read ( twitter, open graph, applinks )
     collectPageInfoData : function ( pageInfo, data ) {

        if(!data) {
            throw "Please provide a window object!";
        }

        var parser = new DOMParser();
        var doc = parser.parseFromString(data, "text/html");
        var jq = $(doc);
        var l = this.getLocation( pageInfo.url );
        pageInfo.host = l.hostname; //win.location.host,
        pageInfo.title = jq.find("title").text();
        pageInfo.meta = {};

        this.doAllMeta(function( query ) {
            return jq.find(query);
        }, pageInfo);
        return pageInfo;
    }
}; 

export default PageInfo;