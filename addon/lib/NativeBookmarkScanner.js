/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Utils from 'lib/Utils.js'

export default function NativeBookmarkScanner(){
    this.folderNameStack = [];
    this.newBookmarks = [];
    this.newAvailableBookmarks = [];
    this.counter = 0;
}

NativeBookmarkScanner.prototype.scan = function (callback) {

    if (Utils.get().bookmarks) {
        // bookmarks available.
        Utils.get().bookmarks.getTree((bookmarkItems, error) => {
            if (error) {                
                callback(null, error);
            } else {
                this.traverse(bookmarkItems[0], 0);
                console.log(this.newBookmarks);
                callback(this.newBookmarks);
            }
        });
    } else {
        console.warn("Bookmarks not supported");
        callback([]);
    }

};
NativeBookmarkScanner.prototype.createBookmark = function (bookmarkItem, folderNames) {
    if(!bookmarkItem.url) {
        // NO URL
        return undefined;
    }
    
    // TODO remove direct dependency
    var bm = {
        url: bookmarkItem.url,
        host: new URL(bookmarkItem.url).hostname, // TODO Replace this with URL()
        title: bookmarkItem.title
    };
    bm.dateAdded = bookmarkItem.dateAdded;
    bm.tags = folderNames;
    return bm;
};

NativeBookmarkScanner.prototype.traverse = function (bookmarkItem, indent) {
    if (bookmarkItem.url) {
        console.log(makeIndent(indent) + bookmarkItem.url);
        var bookmark = this.createBookmark(bookmarkItem, this.folderNameStack.slice(0));
        this.newBookmarks.push(bookmark);
        return;
    } else {
        console.log(makeIndent(indent) + "Folder");
        if (bookmarkItem.title !== "") {
            this.folderNameStack.push(bookmarkItem.title);
        }
        indent++;
    }
    if (bookmarkItem.children) {
        for (var child of bookmarkItem.children) {
            this.traverse(child, indent);
        }
    }
    indent--;
    this.folderNameStack.pop();
};


function makeIndent(indentLength) {
    return ".".repeat(indentLength);
}

function onRejected(error) {
    console.log(`An error: ${error}`);
}