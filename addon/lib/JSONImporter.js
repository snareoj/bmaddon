/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'

export default function JSONImpExporter( importFunc ) {
    this.folderNameStack = [];
    this.newBookmarks = [];
    this.newAvailableBookmarks = [];
    this.counter = 0;
    this.importFunc = importFunc;
};

JSONImpExporter.prototype.importSingle = function (obj, callback) {
    try {
        this.importFunc(obj, callback);
    } catch (e) {
        console.error(e);
        callback(null, e);
    }
};
JSONImpExporter.prototype.importAll = function (currentIndex, listOfBookmarks, callback) {
    
    if(!listOfBookmarks) {
        return;
    }
    
    if (currentIndex < listOfBookmarks.length) {
        this.importSingle(listOfBookmarks[currentIndex], (res, error) => {
            if(error) {
                callback(error);
            } else {
                this.importAll(currentIndex + 1, listOfBookmarks, callback);            
            }
        });
    } else if (callback) {
        callback(listOfBookmarks);
    } else {
        console.log("Import all finished: ", listOfBookmarks.length);
    }
};
JSONImpExporter.prototype.import = function (text, callback) {
    var listOfBookmarks = JSON.parse(text);
    this.importAll(0, listOfBookmarks, callback);
};

JSONImpExporter.prototype.export = function (callback) {
    Utils.exec({cmd: "db.listBookmarks"}, (bms, error) => {
        callback(JSON.stringify(bms), error);
    });
};