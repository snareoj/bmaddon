/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 NextCloud Backend
 */

function NclFile( path ) {
    this._path = path;
    this._isDirectory = false;
    /**
     * File: D:href: /.bookmarks/sample.json
background.bundle.9460253….js:1565 File: D:creationdate: 2017-07-23T11:10:03Z
background.bundle.9460253….js:1565 File: D:getcontentlanguage: de
background.bundle.9460253….js:1565 File: D:getetag: CVCdzJUIacw9Kwb67Kcw8FmdUGNusLZla98Kb4EDkIs
background.bundle.9460253….js:1565 File: D:getlastmodified: Sun, 23 Jul 2017 11:29:16 GMT
background.bundle.9460253….js:1565 File: D:iscollection: false
background.bundle.9460253….js:1565 File: ns666:Win32LastModifiedTime: Sun, 23 Jul 2017 11:29:16 GMT
background.bundle.9460253….js:1565 File: ns666:Win32LastAccessTime: Sun, 23 Jul 2017 11:29:16 GMT
background.bundle.9460253….js:1565 File: D:getcontentlength: 759
background.bundle.9460253….js:1565 File: D:resourcetype: 
background.bundle.9460253….js:1565 File: ns666:Win32FileAttributes: 0
background.bundle.9460253….js:1565 File: D:displayname: sample.json
background.bundle.9460253….js:1565 File: D:getcontenttype: text/plain
background.bundle.9460253….js:1565 File: ns666:Win32CreationTime: Sun, 23 Jul 2017 11:10:03 GMT
background.bundle.9460253….js:1565 File: D:status: HTTP/1.1 200 OK
     */
    this._properties = {};
        
}

NclFile.prototype.collectProperties = function( xmlResp ) { 
        let props = {};
        try {
            this.collectProperties_(xmlResp, props);
            this._properties = props;
        } catch(  e ) {
            console.log(e);
        }
};

NclFile.prototype.lastModified = function() { 
        let s = this._properties['D:getlastmodified'];
        if(s) {
            return new Date(Date.parse(s));
        } else {
            return undefined;
        }
};

NclFile.prototype.collectProperties_ = function( parent, props ) { 
    if(parent && parent.children && parent.children.length > 0) {
        for(let child of parent.children) {
            this.collectProperties_(child, props);
        }
    } else if(parent) {
       props[parent.nodeName] = parent.textContent;
    }
};

NclFile.prototype.propFind = function( client, callback ) {
        var req = new NclBaseRequest(
            client._toURL( this ),
            'PROPFIND',
            client._auth
            );
    req.onData = ( status, text, xml, type ) => {
        this.collectProperties(xml);
        callback ( { status : status, text : text, xml : xml, type : type } , undefined ); 
    }; 
    req.onError = ( status, statusText, e ) => {
        callback ( undefined , {
             status : status, 
             statusText : statusText,
             e : e
        } ); 
    }; 
    req.exec();
};

function NclBaseRequest(url, method, auth, reqBody ) {

    this._method = method;
    this._url = url;
    this._auth = auth;

    this.onData = (status, data) => {
        console.log("Got Data. You need to override this function", status, data);
    };

    this.onError = (status, statusText, e) => {
        console.error("Error in request \n( " + status +","+ statusText+", \n"+ this._method+ ", \n"+this._url+" \n)",  status, statusText, e);
    };

    /**
     * Execute Request
     * @returns {undefined}
     */
    this.exec = () => {
        
        console.log("Cookies = " + document.cookie);
        /*
    207 Multi-Status (WebDAV)
    A Multi-Status response conveys information about multiple resources in situations where multiple status codes might be appropriate.
    208 Multi-Status (WebDAV)
    Used inside a DAV: propstat response element to avoid enumerating the internal members of multiple bindings to the same collection repeatedly.
         */
       // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/207
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = () => {
            if (xhttp.readyState === 4) {
                if (xhttp.status === 200 // Normal HTTP                
                        || xhttp.status === 201 // Webdav: File CREATED
                        || xhttp.status === 204 // Webdav: File UPDATED
                        || xhttp.status === 207) { // Webdav: Multi OK
                    this.onData(xhttp.status, xhttp.responseText, xhttp.responseXML, xhttp.responseType);
                } else {
                    this.onError(xhttp.status, xhttp.statusText, null);
                }
            } else {
                 if (xhttp.status !== 0) {
              //      this.onError(xhttp.status, xhttp.statusText, null);                     
                 }                
            }
        };
        xhttp.onerror = (e) => {
            this.onError(xhttp.status, xhttp.statusText,  e);
        };
        
        xhttp.withCredentials = true;
        xhttp.crossDomain = false;
        
        xhttp.open(this._method, this._url, true);
        
        if(!this._auth) {
            throw new Error("No authentication data!");
        }
        
        var auth = btoa(this._auth._userName +":"+ this._auth._password);
       // xhttp.setRequestHeader("User-Agent", "Bookmark Addon");       
        xhttp.setRequestHeader("Authorization", "Basic " + auth);                     
        //xhttp.setRequestHeader("Cookie", "");      
        
        if(reqBody !== undefined) {
            xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhttp.send(reqBody);
        } else {
            xhttp.send();
        }
    };

}

/**

The base url for all WebDAV operations for a Nextcloud instance is /remote.php/dav.

All requests need to provide authentication information, either as a Basic Auth header or by passing a set of valid session cookies.

https://docs.nextcloud.com/server/12/developer_manual/client_apis/WebDAV/index.html

 * @param {type} baseURL
 * @param {type} userName
 * @param {type} password
 * @returns {WebDavNextCloudSync}
*
*/
function WebDavNextCloudSync(baseURL, userName, password ) {

    //this._settings = settings;
    //E.g. : remote.php/dav/files/user
    this._baseURL = baseURL;        
    this._auth = {
        _userName : userName,
        _password : password
    };    
    
    this._toURL = function( file ) {
        //return this._davURL + this._auth._userName + file._path;
        let url =  this._baseURL + file._path;
        console.log("NextCloud URL: " + url);
        return url;
    };

    this.getRootFolder = function( callback ){
        var rootFolder = new NclFile();
        rootFolder._path = "/";
        rootFolder._isDirectory = true;
        this.listFolders(rootFolder, callback);
    };


    this.getFile = function ( nclFile, callback ) {
        //GET remote.php/dav/files/user/path/to/file
         var req = new NclBaseRequest(
                this._toURL( nclFile ),
                'GET',
                this._auth
                );
        req.onData = ( status, data ) => {
            callback ( {
                status  : status,
                data : data
            }, undefined ); 
        }; 
        req.onError = ( status, statusText, e ) => {
            callback ( undefined , {
                 status : status, 
                 statusText : statusText,
                 e : e
            } ); 
        }; 
        req.exec();
    };
    
    this.putFile = function ( nclFile, data, callback ) {
        //GET remote.php/dav/files/user/path/to/file
         var req = new NclBaseRequest(
                this._toURL( nclFile ),
                'PUT',
                this._auth,
                data
                );
        req.onData = ( status, data ) => {
            callback ( {
                status  : status,
                data : data
            } , undefined ); 
        }; 
        req.onError = ( status, statusText, e ) => {
            callback ( undefined , {
                 status : status, 
                 statusText : statusText,
                 e : e
            } ); 
        }; 
        req.exec();
    };
    
    /**
     * List Folders
     * @returns {String}
     */
    this._listFolders = function ( nclFile, callback ) {
        var tmpl = `<?xml version="1.0"?>
                    <d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns">
                      <d:prop>
                            <d:getlastmodified />
                            <d:getetag />
                            <d:getcontenttype />
                            <d:resourcetype />
                            <oc:fileid />
                            <oc:permissions />
                            <oc:size />
                            <d:getcontentlength />
                            <nc:has-preview />
                            <oc:favorite />
                            <oc:comments-unread />
                            <oc:owner-display-name />
                            <oc:share-types />
                      </d:prop>
                    </d:propfind>`;

        var req = new NclBaseRequest(
                this._toURL( nclFile ),
                'PROPFIND',
                this._auth,                
                tmpl
                );
        req.onData = ( data ) => {
            callback ( data , undefined ); 
        }; 
        req.onError = ( status, statusText, e ) => {
            callback ( undefined , {
                 status : status, 
                 statusText : statusText,
                 e : e
            } ); 
        }; 
        req.exec();

    };
    
    this.listRootFolder = function ( callback ) {
        let file = new NclFile('/');
        file.propFind(this, callback);
    }
}

export {
    NclFile,
    NclBaseRequest,
    WebDavNextCloudSync
}
