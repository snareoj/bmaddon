/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */


import Client from 'content_scripts/Client.js';
import TagsMenu from 'content_scripts/TagsMenu.js';
import * as Constants from 'content_scripts/Constants.js';

export default class BookmarkMenu {

    /**
     * @param {Client} client
     */
    constructor(client) {        
        this.client = client;
        this.inited = false;

        this.initCounter = 0;
        
        var prefix = Constants.prefix;
        var idMenu = Constants.IdMenu;
        var clIcon = Constants.clIcon;
      
            this.menuTemplate = `    
         <div id="${idMenu}">
             <div id="${prefix}tap">
                 <span id="${prefix}tap_ttl"></span>
                 <span id="${prefix}tap_aBm" class="${prefix}btn" style="display:none;"><span class="${clIcon} ${prefix}addBookmark"></span>Add Bookmark</span>
                 <span id="${prefix}tap_lBm" class="${prefix}btn" style="display:none;"><span class="${clIcon} ${prefix}listBookmarks"></span>List Bookmarks</span>    
                 <span id="${prefix}tap_eBm" class="${prefix}btn" ><span class="${clIcon} ${prefix}editBookmark"></span>Edit Bookmark</span>
                 <span id="${prefix}tap_tBm" class="${prefix}btn" ><span class="${clIcon} ${prefix}editTags"></span>Edit Tags</span>
                 <span id="${prefix}tap_dBm" class="${prefix}btn" ><span class="${clIcon} ${prefix}removeBookmark"></span>Remove Bookmark</span>
             </div>
             <div id="${prefix}menuBtns">
                 <div id="${prefix}bookmarkBtn" class="${clIcon} ${prefix}bookmarked"></div>     
                 <div id="${prefix}listBookmarksBtn" class="${clIcon} ${prefix}listBookmarks"></div>   
                 <div id="${prefix}importJSONFile" style="display:none;" class="${clIcon} ${prefix}importJSONFile"></div>     
             </div>
         </div>`;
    }

    init() {
        if (this.inited)
            return;
        this.inited = true;
        
        this.tagMenu = new TagsMenu(this.client);
        // Finmally create Menu
        if(!this.createMenu()) {
            this.inited = false;
            this.initCounter ++;
            if(this.initCounter < 3){
                setTimeout(()=> this.init(), 500);
            } else {
                console.log("Failed to init bookmarks! No Body detected.");
            }
            return;
        }

        // Initial State of menu
        this.client.getBookmark()
                .then(bm => {
                    this.client.setBookmarked(bm);
                    this.setBmed(bm);
                }).catch(e => console.error("Error while getting Bookmark", e));
    }

    setBmed(bm) {
        if (bm) {
            this.toggleBookmarkButton.tooltip = "Click to remove bookmark";
            this.toggleBookmarkButton.className = Constants.clIcon + " " + Constants.prefix + "bookmarked";
        } else {
            this.toggleBookmarkButton.tooltip = "Click to add bookmark";
            this.toggleBookmarkButton.className = Constants.clIcon + " " + Constants.prefix + "nobookmark";
        }
    }


    createMenu() {
     // can access and modify the DOM
        var bodyNode = document.body;
        var menuRoot = document.getElementById(Constants.IdMenu);
        if (!menuRoot && bodyNode) {
            menuRoot = document.createElement("div");
            bodyNode.appendChild(menuRoot);
            menuRoot.innerHTML = this.menuTemplate;
            menuRoot.data = {};
            this.menuRoot = menuRoot;
            this.toggleBookmarkButtonInot();            
            this.allBtns();
            this.setSprite();
            this.initImportBtn();
            return true;
        }
        return false;
    }
    showTapMenu( action ){
             if(action === true) {
                 this.visible = true;
                 document.getElementById(Constants.prefix + 'tap').className = Constants.prefix+'visible';  
             } else {
                 this.visible = false;
                 document.getElementById(Constants.prefix + 'tap').className = '';                  
             }
    }        
    
    addBookmark(){
        this.client.addBookmark()
         .then( (res ) => {                             
             this.setBmed (res);
             this.tagMenu.showTags();
         }).catch( error => {
            alert("Error see console."); 
             console.error("Error Bookmark", error);
             this.setBmed ( null );
         });  
    }
         
    toggleBookmarkButtonInot() {        
         // Icon Menu
         // Hinzufügen zu Bookmarks
         this.toggleBookmarkButton = document.getElementById(Constants.prefix + 'bookmarkBtn');          
        // toggleBookmarkButton.onclick= (ev) => {
         this.toggleBookmarkButton.onmousedown = (ev) => {
             console.log("EVENT " ,'onmousedown');
            if(this.client.isPageBookmarked()) {
                this.tagMenu.hideTags();   
                if(!this.visible) {
                   this.showTapMenu(true);
                } else {
                   this.showTapMenu(false);
                }
             } else {
                this.addBookmark();
             }
             ev.preventDefault();
         };
    }
    
    allBtns() {


         var mOuver = function( ev ){
           //  console.log("ev")
         };

        const prefix = Constants.prefix;
         var btn = document.getElementById(prefix + 'tap_aBm');          
         //btn.onclick= (ev) => {
         btn.onmouseover = mOuver;
         btn.onmouseup = (ev) => {
             console.log("EVENT " ,'tap_aBm');
             ev.preventDefault();
             this.showTapMenu(false);
             this.addBookmark();
         };
         btn = document.getElementById(prefix + 'tap_tBm');          
         //btn.onclick= (ev) => {
         btn.onmouseover = mOuver;
         btn.onmouseup = (ev) => {
             console.log("EVENT " ,'tap_tBm');
             ev.preventDefault();
             this.tagMenu.showTags( this.client.getPageBookmark().tags );
         };
         btn = document.getElementById(prefix + 'tap_lBm');          
         //btn.onclick= (ev) => {
         btn.onmouseover = mOuver;
         btn.onmouseup = (ev) => {
             console.log("EVENT " ,'tap_lBm');
             ev.preventDefault();
             this.showTapMenu(false);
             this.client.listBookmarks();
         };
         btn = document.getElementById(prefix + 'tap_dBm');          
         //btn.onclick= (ev) => {
         btn.onmouseover = mOuver;
         btn.onmouseup = (ev) => {
             console.log("EVENT " ,'tap_dBm');
             ev.preventDefault();
             this.showTapMenu(false);
             this.client.deleteBookmark()
                .then( (res ) => {                             
                   this.setBmed (res);
                }).catch( error => {
                   alert("Error see console."); 
                    console.error("Error Bookmark", error);
                    this.setBmed ( null );
                });  
         };
         btn = document.getElementById(prefix + 'tap_eBm');          
         //btn.onclick= (ev) => {
         btn.onmouseover = mOuver;
         btn.onmouseup = (ev) => {
             console.log("EVENT " ,'tap_eBm');
             ev.preventDefault();
             this.showTapMenu(false);
             this.client.editBookmark();
         };
         /*
         document.getElementById(prefix + 'tap').onmouseout = (ev) => {
             console.log("EVENT " ,' bmRoot.onmouseout');
             ev.preventDefault();
             stopTapMenu();
             hideTags();                   
         }; */

         btn = document.getElementById(prefix + 'listBookmarksBtn');          
         //btn.onclick= (ev) => {
         btn.onmouseup = (ev) => {
             ev.preventDefault();                
             this.client.listBookmarks();
         };

    }
    
    setSprite() {
       try {
           var bUndef = true;
           try {
               bUndef = browser === undefined;
           } catch (e) {};
           //Code for displaying <extensionDir>/images/myimage.png: 
           var itsMe = chrome && bUndef ? "chrome" : "browser";
           this.menuRoot.className += Constants.prefix + itsMe;
       } catch (e) {
           console.log("ERROR", e);
       }
       /// background-image:url('chrome-extension://__MSG_@@extension_id__/icons/icon_sprite.png');
   }
   
   initImportBtn(){
        var btn;
        if(window.location.pathname.indexOf(".bookmarks.json") > 0) {
             btn = document.getElementById(Constants.prefix + 'importJSONFile'); 
             btn.style.display = 'inherit';
             //btn.onclick= (ev) => {
             btn.onmouseup = (ev) => {
                 ev.preventDefault();                
                 //  Firefox => Rawdata
                 let el = document.querySelector('div.textPanelBox div.panelContent pre.data');
                 if(!el) {
                     // Chroime
                     el = document.querySelector('body > pre');
                 }
                 if(el && el.textContent.length) {
                     console.log("found content " + el.textContent.length);

                 console.log("createBookmark");
                 this.client.importJSON(el.textContent)
                 .then( (res ) => {                             
                        alert(res + " Imported ") ;
                 }).catch( error => {
                         alert("Error see console."); 
                         console.error("Error while creating Bookmark", error);
                 });                 
             };
            }
            
        }
   }

}