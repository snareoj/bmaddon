
import EventBus from '/background/EventBus.js';

/* 
 * Spec for Background
 */
describe("EventBus TestSuite", function () {
    
    it("Does not handle unkown Messages and returns an error", function ( done ) {       
        
        // Parameter is the startup caklkback
        var bgScript = new EventBus();
        bgScript.handleCmd({
            cmd : 'unknown'
        }, null, function ( res , error) {            
            expect(error).toBeDefined();        
            expect(res).not.toBeDefined();
            expect(error.message).toBe('Unknown Message: {"cmd":"unknown"}');
            done();
        });
        
    });
});
