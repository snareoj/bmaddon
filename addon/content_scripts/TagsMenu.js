/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Client from 'content_scripts/Client.js'
import * as Constants from 'content_scripts/Constants.js'
import naturalSort from 'naturalSort'

function _sortedArray( tags ){
    naturalSort.insensitive = true;
    let keys = Object.keys(tags).sort(naturalSort);    
    return keys;
}

export default class TagsMenu {
        
    /**
     * @param {Client} client
     */
    constructor( client ) {
        this.showTagsTimerHandle = undefined;
        this.client = client;
        var prefix = Constants.prefix;
        this.tagMenu = `
         <div id="${prefix}close"></div>
         <div id="${prefix}tagList"  class="${prefix}tagList">
         </div>`;
    }

    hideTags() {
        clearTimeout(this.showTagsTimerHandle);
        if (document.getElementById(Constants.id('tagMenu'))) {
            document.getElementById(Constants.id('tagMenu')).remove();
        }
    }

    showTags(highlightedTags) {
        if (document.getElementById(Constants.id('tagMenu'))) {
            return;
        }
        this.client.loadTags()
            .then(( answer ) => {
                if (answer) {
                       this.renderMenu(answer, highlightedTags);
                }}
            ).catch((error) =>  {
                console.error("Error while edited Bookmark", error);
            });
    }

                
     renderMenu(completeTagList, highlightedTags){
        var tRoot = document.createElement("div");
        tRoot.setAttribute('id', Constants.id('tagMenu'));
        tRoot.innerHTML = this.tagMenu;
        var bodyNode = document.body;
        bodyNode.appendChild(tRoot);
        // Auto Closing / Closing By User  var timer =  0, timerDelta = 10;
        var closer = document.getElementById(Constants.id('close'));
        var timer = 0, timerDelta = 10, maxTime = 2000;
        var timerFunc = () => {
            timer += timerDelta;
            closer.textContent = "Closing taglist and updateing bookmark in " + (maxTime - timer);
            // Time is out, set the bookmarks!
            if (timer >= maxTime) {
                clearTimeout(this.showTagsTimerHandle);
                this.client.setTags(tagList).then( ( ) => {
                    tRoot.remove();
                }).catch( ( e ) => {                            
                    console.log("No bookmark" , e);
                    this.hideTags();
                });                        
            } else {
                this.showTagsTimerHandle = setTimeout(timerFunc, timerDelta);
            }
        };
        timerFunc();
        var tagList = [];
        // Render and Handle the tags
        var list = document.getElementById(Constants.prefix + 'tagList');
        
        for (let key of _sortedArray(completeTagList)) {
            let span = document.createElement('span');
            span.setAttribute('data-tag', key);
            span.className = Constants.id('tag');
            span.innerText = key;
            if (highlightedTags && highlightedTags.indexOf(key) >= 0) {
                span.className += "  "+ Constants.id('selected');
                tagList.push(key);
            }
            list.appendChild(span);
        }
        list.onclick = (ev) => {
            // ADD and REMOVE clicked tag to TagList
            var tagName;
            if (ev.target && ev.target.getAttribute('data-tag')) {
                tagName = ev.target.getAttribute('data-tag');
            }
            let idx = tagList.indexOf(tagName);
            if (idx >= 0) {
                // already marked, toggle it
                ev.target.className = Constants.prefix +'tag';
                tagList.splice(idx, 1);
            } else if (tagName) {
                tagList.push(tagName);
                ev.target.className = Constants.prefix +'tag'+"  "+ Constants.prefix +'selected';
            }
            if (tagName) {
                // User clicked on a tag, reset the timer
                timer = 0;
            }
        };
        console.log("Tags: ", completeTagList);
     }

}

