/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

import Utils from 'lib/Utils.js'

/**
 * Simulates kind of Background Session where a script can save and load variables;
 * Will be deleteted if the tab is closed!
 */
export default function BackgroundSessionClient(){        
}

BackgroundSessionClient.prototype.get = function( key , callback ){
        Utils.get().tabs.getCurrent( ( tab, error ) => {                
            if(error){
                console.log("Error!",error);
                return;
            }          
            console.log("background.session.get", key, tab);
            Utils.exec( {
                cmd : 'background.session.get',
                key : key,
                sessionId : tab.id
            }, callback);
                
        });
};

BackgroundSessionClient.prototype.set = function( key, value, callback ){
        Utils.get().tabs.getCurrent( ( tab, error ) => {                
            if(error){
                console.log("Error!",error);
                return;
            }                
            console.log("background.session.set", key, value, tab);
            Utils.exec( {
                cmd : 'background.session.set',
                key : key,
                value : value,
                sessionId : tab.id
            }, callback);
                
        });
};



