/*
   Copyright [2017] [Jonas Tränkner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. 
 
 */

/* 
 *  Client JavaScript
 */
import PageInfo from 'lib/PageInfo'

export default class Client {        

    constructor () {
        this.bookmarked = false;    
        this.msgCallbacks = {};
    
        this.myPort = this.get().runtime.connect({name: "port-from-cs"});
        this.myPort.onDisconnect.addListener(function (m) {
            console.log("Backend disconnected.");
        });
        this.myPort.onMessage.addListener((m) => {
            if (m && typeof this.msgCallbacks[m.msgid] === "function") {
                try {
                    this.msgCallbacks[m.msgid](m);
                } catch ( e ) {
                    console.log("Error handling answer from bg script ", e);
                } finally {
                    delete this.msgCallbacks[ m.msgid ];
                }
            } else {
                // Got unknown message
                console.log("No Callback registered for ", m);
            }
        });
    }

    get() {
        var itsMe = chrome ? chrome : browser;
        return itsMe;
    }
    
    isPageBookmarked(){
        return this.bookmarked;
    }
    setPageBookmarked( bookmarkAvail ){
        this.bookmarked = bookmarkAvail;
    }
    getPageBookmark(){
        return this.bookmark;
    }
    setPageBookmark( bm ){
        this.bookmark= bm;
    }
    // Adds a bookmark
    addBookmark() {
        if (this.isPageBookmarked() === false) {            
            var pInfo = PageInfo.collectPageInfo(window);
            return this.createBookmark(pInfo);
        }else {
            return Promise.resolve(this.getPageBookmark());
        }
    }
    
    // Removes the bookmark if existent, else adds bookmark
    deleteBookmark() {
        if (this.isPageBookmarked() === true) {
            return this.removeBookmark(this.getPageBookmark().id);
        } else {
            return Promise.resolve(false);
        }
    }
    
    // SET the page bookmarked, saves at key "bookmark:URL"
    setBookmarked(res) {
        if (res !== undefined && res !== null) {
            this.setPageBookmarked(true);
            this.setPageBookmark(res);
        } else {
            this.setPageBookmarked(false);
            this.setPageBookmark(null);
        }
    }   


    sendCmd(cmd, callback, msgd) {
        /*myPort.postMessage(null,
         obj, null, function (result, e) { */
        var msgid = new Date();
        cmd.msgid = msgd + msgid.toString();
        this.msgCallbacks[ cmd.msgid ] = (result) => {
            if (result && result.error) {
                console.error("Error in handling runtime message", result.error);
                callback(undefined, result.error);
            } else if (callback && result) {
                console.info("Handling runtime answer", result);
                callback(result.res, result.error);
            } else if (callback) {
                console.info("Handling runtime answer callback", result);
                callback(result);
            } else {
                throw "Cant handle Result";
            }
        };    
        var obj = JSON.stringify(cmd);
         this.myPort.postMessage(obj, ( answer ) => {
            console.log("Answeer", answer);
        });
    }

    createBookmark(pInfo) {
        return new Promise( (resolve, reject ) => {
            console.log("createBookmark");
             this.sendCmd({
                "cmd": "db.createAndPersistBookmark",
                "obj": pInfo
            }, (answer, error) => {
                if (answer || (!answer && !error)) {
                    console.log("Bookmark created.", answer);
                     this.setBookmarked(answer);    
                     resolve(answer);
                     return;
                } 
                if (error) {
                    console.error("Error while creating Bookmark", error);
                }
                reject(error);
            });
        });
    }

    removeBookmark(bmId) {
        return new Promise( (resolve, reject ) => {
            console.log("removeBookmark");
            this. sendCmd({
                "cmd": "db.removeBookmark",
                "obj": bmId
            }, (answer, error)  => {
                if (answer || (!answer && !error)) {
                    console.log("Bookmark removed.", answer);
                     this.setBookmarked(null);
                     resolve(null);
                } else {
                    console.error("Error while saving BOokmark", error);
                    reject(error);
                }
            });
        });
    }

    editBookmark() {        
        if(!this.isPageBookmarked()) {
            return;
        }        
        console.log("Editing Bookmark");
         this.sendCmd({
            "cmd" : "nav.editBookmark",
            "id" : this.getPageBookmark().id
         }, (answer, error)  => {
            if (answer || (!answer && !error)) {
                console.log("Bookmark edited.", answer);
              //  this.setBookmarked(answer);
            }
            if (error) {
                console.error("Error while edited Bookmark", error);
            }
        });  
    }
    
    loadTags() {        
        return new Promise( (resolve, reject ) => {
            this.sendCmd({
                   "cmd": "tags.load"
               }, function (answer, error) {
                   if (answer || (!answer && !error)) {  
                      resolve(answer); 
                   }  else {
                       reject(error); 
                   }
               });
        });
    }
    
    setTags( tagList ){
        return new Promise( (resolve, reject ) => {
            if(this.isPageBookmarked() && this.getPageBookmark()) {
                this.getPageBookmark().tags = tagList;
                this.sendCmd({
                    "cmd": "db.updateBookmark",
                    "obj": this.getPageBookmark()
                }, (answer, error) => {
                    if (answer || (!answer && !error)) {
                        console.log("Bookmark updated.", answer);
                        this.setBookmarked(answer);
                    }
                    if (error) {
                        reject(error);
                        console.error("Error while updateing Bookmark", error);
                        return;
                    }              
                    this.sendCmd({
                        "cmd": "client.afterBookmarkUpdate",
                        "tags": tagList
                    }, function (answer, error) {
                        if (answer || (!answer && !error)) {
                            console.log("Tags updated.", answer);
                            resolve(answer);
                        }
                        if (error) {
                            reject(error);
                            console.error("Error while updateing Tags", error);
                        }    
                    });
                });
            } 
            resolve();
        });
    }
    
    importJSON( txtContent ) {
        return new Promise( (resolve, reject ) => {            
            sendCmd({
                "cmd": "import.JSON",
                "obj": txtContent
            }, (answer, error) => {
                if (answer || (!answer && !error)) {
                   resolve(answer) ;
                }
                if (error) {
                    reject("Error see console."); 
                    console.error("Error while creating Bookmark", error);
                }
            });
        });        
    }
    
     getBookmark() {
        return new Promise( (resolve, reject ) => {     
            var pInfo = PageInfo.collectPageInfo(window);
            console.log("get Bookmark", pInfo.url);
            this.sendCmd({
                "cmd": "client.lookupBookmarkByURL",
                "url": pInfo.url,
                'msgid': "client.lookupBookmarkByURL"
            }, (res, e) => {
                if(e) {
                    reject(e);
                } else {
                    resolve(res);
                }
            });
        });        
    }

    listBookmarks() {
        console.log("nav.List Bookmark");
        this.sendCmd({
            "cmd": "nav.listBookmarks"
        });
    }
       

    openTopsites() {
        console.log("nav.List Topsites");
        this.sendCmd({
            "cmd": "nav.openTopsites"
        });
    }
 

    openHome() {
        console.log("nav.List Jome");
        this.sendCmd({
            "cmd": "nav.home"
        });
    }
}
